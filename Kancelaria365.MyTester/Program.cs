﻿using Microsoft.Data.Edm.Library;
using Microsoft.OData.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office365.OutlookServices;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Kancelaria365.MyTester
{
    class Program
    {
        static string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlJyUXF1OXJ5ZEJWUldtY29jdVhVYjIwSEdSTSIsImtpZCI6IlJyUXF1OXJ5ZEJWUldtY29jdVhVYjIwSEdSTSJ9.eyJhdWQiOiJodHRwczovL291dGxvb2sub2ZmaWNlLmNvbSIsImlzcyI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzY3MDk1MmFjLWQ4YTAtNDg4Zi1hNjQyLTZmYWViY2I3OTU0ZC8iLCJpYXQiOjE0ODI1Mjk2NTUsIm5iZiI6MTQ4MjUyOTY1NSwiZXhwIjoxNDgyNTMzNTU1LCJhY3IiOiIxIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6IjQzMWJkNDc2LWU2OWEtNDk1NS1iMTY4LWZkNDIwMWM3YmFkYyIsImFwcGlkYWNyIjoiMSIsImVfZXhwIjoxODAwMCwiZmFtaWx5X25hbWUiOiJNYWNobmlrIiwiZ2l2ZW5fbmFtZSI6IkFydHVyIiwiaXBhZGRyIjoiODcuMTAxLjc3LjEzNSIsIm5hbWUiOiJBcnR1ciBNYWNobmlrIiwib2lkIjoiMmM1N2E3MTctNmIzZi00ZjliLWIxYzQtYWEzNDE4MTM0ODAzIiwicGxhdGYiOiIzIiwicHVpZCI6IjEwMDM3RkZFOUE3NjBCQUUiLCJzY3AiOiJDYWxlbmRhcnMuUmVhZFdyaXRlIENvbnRhY3RzLlJlYWQgTWFpbC5SZWFkIFRhc2tzLlJlYWRXcml0ZSIsInN1YiI6IlpzWlpibHhoVlJFU3lFYUJTb2hsQUZUdGs3ck1fOFBuT3hPMUF0aWhmZ1EiLCJ0aWQiOiI2NzA5NTJhYy1kOGEwLTQ4OGYtYTY0Mi02ZmFlYmNiNzk1NGQiLCJ1bmlxdWVfbmFtZSI6ImFtYWNobmlrQG1hY2hzb2Z0Mi5vbm1pY3Jvc29mdC5jb20iLCJ1cG4iOiJhbWFjaG5pa0BtYWNoc29mdDIub25taWNyb3NvZnQuY29tIiwidmVyIjoiMS4wIn0.WTQAq9VFgGayE-HsvbVRYG3apzTgqI-MhRxb6SJ06xidLQIAX2jRpY91ngQWu5KR1W2hRIttM-8pEleb1zFWE2yfpOWkYpQyQQXAoM5z2XDghfzQo3mdF4sHrk1DiaK3TOtWMQ0JzqozUWIaB6DIG0wkaBp-zmuiwphMBme3gjXij_hEY3sarmdkpYSwmDVxbCsAi65ycl36-iA8taHjLYTPFMVVJDcGRwFb6oL66lEcaBh-HCpv3HTmKFzeSksozsQFPkA0oh-kLuRKJgXme-Wy5vV7BfJyhiOl4nN918By8NtJrFY2uT9hsXsRlv_qq1KGI6DuNN4Nx9B97X-54A";
        public static async Task test()
        {
            try
            {
                var httpClient = new HttpClient();


                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await httpClient.GetAsync("https://outlook.office.com/api/v2.0/$metadata");

                response.EnsureSuccessStatusCode();

                string content = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
            }
        }
        static void Main(string[] args)
        {
            try
            {
              
                test().Wait();
                //OutlookServicesClient s;
                //ICalendar s=    s.Me.Calendar.ExecuteAsync();

                //Uri uri = new Uri("https://outlook.office.com/api/v2.0/me");
                //DataServiceContext ctx = new DataServiceContext(uri, ODataProtocolVersion.V4);

                //ctx.IgnoreMissingProperties = true;

                //ctx.ReceivingResponse += (object sender, ReceivingResponseEventArgs e) =>
                //{
                //};
                //ctx.BuildingRequest += (object sender, BuildingRequestEventArgs e) =>
                //{
                //    e.Headers.Add("Authorization", "Bearer " + token);
                //};
                //ctx.SendingRequest2 += (object sender, SendingRequest2EventArgs e) =>
                //{
                //};

                //var query = ctx.CreateQuery<Calendar>("Calendars");
                //var obj = query.ToList();
            }
            catch (DataServiceQueryException ex)
                {
            }
            catch (DataServiceClientException ex)
            {
            }
            catch (Exception ex)
            {
            }
        }
    }
    public class CustomContext : DataServiceContext
    {
        public class Calendar
        { }
    }

}

