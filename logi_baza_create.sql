﻿CREATE TABLE [dbo].[NLog] (
   [ID] [int] IDENTITY(1,1) NOT NULL,
   [time_stamp] [varchar](max),
   [level] [varchar](max),
   [logger] [varchar](max),
   [message] [varchar](max),
   [machineName] [varchar](max),
   [userName] [varchar](max),
   [callSite] [varchar](max),
   [threadId] [varchar](max),
   [logException] [varchar](max),
   [stackTrace] [varchar](max),
 CONSTRAINT [PK_dbo.Log] PRIMARY KEY CLUSTERED ([ID] ASC) 
   WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

GO

CREATE PROCEDURE [dbo].[NLog_AddEntry_p] (
   @time_stamp varchar(max),
   @level varchar(max),
   @logger varchar(max),
   @message varchar(max),
   @machineName varchar(max),
   @userName varchar(max),
   @callSite varchar(max),
   @threadId varchar(max),
   @logException varchar(max),
   @stackTrace varchar(max)
) AS
BEGIN
  INSERT INTO [dbo].[NLog] (
   [time_stamp],
   [level],
   [logger],
   [message],
   [machineName],
   [userName],
   [callSite],
   [threadId],
   [logException],
   [stackTrace]
  ) VALUES (
   @time_stamp,
   @level,
   @logger,
   @message,
   @machineName,
   @userName,
   @callSite,
   @threadId,
   @logException,
   @stackTrace
  );
END