﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Data
{
    public class CzynnoscRepository :
        GenericRepositoryBase<Czynnosc>, IRepositoryBase<Czynnosc>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override string KeyId
        {
            get
            {
                //Należy recznie utworzy alias otakiej naziwie w management propeties
                return "CzynnoscIdMP";
            }
        }

        public override string ListName
        {
            get
            {
                return GlobalNames.Lists.Czynnosci;
            }
        }

        public override SP.CamlQuery CreateCustomQuery(SearchParameters searchParameters)
        {
            if (searchParameters.UseCustomDataSource)
            {
                SP.CamlQuery query = new SP.CamlQuery();
                string customQuery = string.Empty;

                switch (searchParameters.CustomDataSourceName)
                {
                    case "Czynnosc":
                        SimpleSearch sprawa = searchParameters.SimpleSearch.FirstOrDefault(x => x.PropertyName == "SprawaId");
                        SimpleSearch kontakt = searchParameters.SimpleSearch.FirstOrDefault(x => x.PropertyName == "KontaktId");

                        if (kontakt != null && kontakt.Value != null)
                        {
                            customQuery = "<Eq><FieldRef Name='Firma' LookupId='TRUE'/><Value Type='Lookup'>" + kontakt.Value + "</Value></Eq>";
                            if (sprawa != null && sprawa.Value != null)
                            {
                                string sprawaWhere = "<Eq><FieldRef Name='Sprawa' LookupId='TRUE'/><Value Type='Lookup'>" + sprawa.Value + "</Value></Eq>";
                                customQuery = string.Format("<And>{0}{1}</And>", customQuery, sprawaWhere);
                            }

                            customQuery = string.Format(@"<View><Query><Where><And>{1}{0}</And></Where></Query></View>", DeleteQuery, customQuery);
                            query.ViewXml = customQuery; ;
                        }
                        else
                        {
                            query = null;
                        }
                        break;
                    case "Czynnosc-Moje":
                        UserRepository userRepo = new UserRepository(Context);
                        customQuery = "<Eq><FieldRef Name='Wykonawca' LookupId='TRUE'/><Value Type='Integer'>" + userRepo.CurrentUser.Id + "</Value></Eq>";
                        customQuery = string.Format(@"<View><Query><Where><And>{1}{0}</And></Where></Query></View>", DeleteQuery, customQuery);
                        query.ViewXml = customQuery; ;

                        break;
                    default:
                        throw new NotImplementedException();
                }
                return query;
            }
            else
            {
                return null;
            }
        }

        public CzynnoscRepository()
        : base()
        {
        }

        public CzynnoscRepository(SP.ClientContext context)
                : base(context)
        {
        }

        public override Czynnosc ConvertToEntity(SP.ListItem item)
        {
            Czynnosc entity = new Czynnosc();
            try
            {
                entity.Id = item.Id;
                entity.Title = item.GetValue<string>("Title");
                entity.Kategoria = item.GetDictionaryItemValue<SP.FieldLookupValue>("Kategoria");
                entity.Firma = item.GetTypedDictionaryItemValue<Kontakt, SP.FieldLookupValue>("Firma");
                entity.Sprawa = item.GetTypedDictionaryItemValue<Sprawa, SP.FieldLookupValue>("Sprawa");
                entity.OsobaFakturowana = item.GetDictionaryItemValue<SP.FieldLookupValue>("OsobaFakturowana");
                entity.Wprowadzajacy = new UserDictionaryItem(item.GetDictionaryItemValue<SP.FieldUserValue>("Wprowadzajacy"));
                entity.Wykonawca = new UserDictionaryItem(item.GetDictionaryItemValue<SP.FieldUserValue>("Wykonawca"));

                entity.NumerFaktury = item.GetValue<string>("NumerFaktury");
                entity.NumerKorekty = item.GetValue<string>("NumerKorekty");
                entity.PublikujWPKWeb = item.GetValue<bool>("PublikujWPKWeb");
                entity.ObciazycKlienta = item.GetValue<bool>("ObciazycKlienta");
                entity.Zatwierdzona = item.GetValue<bool>("Zatwierdzona");
                entity.Zafakturowana = item.GetValue<bool>("Zafakturowana");
                entity.DataWykonania = item.GetValue<DateTime?>("DataWykonania");
                entity.CzasZarejestrowany = item.GetValue<Double>("CzasZarejestrowany");
                entity.CzasFakturowany = item.GetValue<int>("CzasFakturowany");
                entity.DataWprowadzenia = item.GetValue<DateTime>("DataWprowadzenia");
                entity.Uwagi = item.GetValue<string>("Uwagi");
                entity.OpisWykonania = item.GetValue<string>("OpisWykonania");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return entity;
        }

        public override SP.ListItem ConvertToItem(Czynnosc entity, SP.ListItem item)
        {
            try
            {
                item.SetValue<string>("Title", entity.Title);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("Firma", entity.Firma);
                item.SetValue<string>("SprawaId", entity.Sprawa.Id.ToString());
                item.SetValue<string>("KontaktId", entity.Firma.Id.ToString());
                item.SetDictionaryItemValue<SP.FieldLookupValue>("Sprawa", entity.Sprawa);
                //item.SetValue<SP.FieldLookupValue>("Kategoria", new SP.FieldLookupValue() { LookupId = entity.Kategoria.Id });
                item.SetDictionaryItemValue<SP.FieldUserValue>("Wykonawca", entity.Wykonawca);
                //item.SetValue<SP.FieldUserValue>("OsobaFakturowana", new SP.FieldUserValue() { LookupId = entity.OsobaFakturowana.Id });
                item.SetDictionaryItemValue<SP.FieldUserValue>("Wprowadzajacy", entity.Wprowadzajacy);
                //item.SetValue<string>("NumerFaktury", entity.NumerFaktury);
                //item.SetValue<string>("NumerKorekty", entity.NumerKorekty);
                //item.SetValue<DateTime>("CzasTrwania", entity.CzasTrwania);
                item.SetValue<bool>("PublikujWPKWeb", entity.PublikujWPKWeb);
                item.SetValue<bool>("ObciazycKlienta", entity.ObciazycKlienta);
                item.SetValue<bool>("Zatwierdzona", entity.Zatwierdzona);
                item.SetValue<bool>("Zafakturowana", entity.Zafakturowana);
                item.SetValue<DateTime?>("DataWykonania", entity.DataWykonania);
                item.SetValue<Double>("CzasZarejestrowany", entity.CzasZarejestrowany);
                item.SetValue<int>("CzasFakturowany", entity.CzasFakturowany);
                item.SetValue<DateTime>("DataWprowadzenia", entity.DataWprowadzenia);
                item.SetValue<string>("Uwagi", entity.Uwagi);
                item.SetValue<string>("OpisWykonania", entity.OpisWykonania);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }
    }
}

