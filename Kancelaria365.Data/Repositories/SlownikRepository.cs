﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Data
{
    public class SlownikRepository :
        GenericRepositoryBase<DictionaryItem>, IRepositoryBase<DictionaryItem>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        #region base methods
        public override string ListName
        {
            get
            {
                return "";
            }
        }

        public override string BaseView
        {
            get
            {
                return @"<View>{0}</View>";
            }
        }

        public SlownikRepository(SP.ClientContext context)
                   : base(context)
        {
            Context = context;
        }

        public DictionaryItem GetEntityById(int id, string listName)
        {
            SP.List spList = Context.Web.Lists.GetByTitle(listName);
            SP.ListItem item = spList.GetItemById(id);

            Context.Load(item);
            Context.ExecuteQuery();

            return ConvertToEntity(item);
        }

        public List<SlownikItem> GetDictionaryItems()
        {
            List<SlownikItem> slownikiList = new List<SlownikItem>();
            try
            {
                Web web = Context.Web;

                IEnumerable<List> result = Context.LoadQuery(web.Lists.Include(
                                                                                   list => list.Title,
                                                                                   list => list.Id));
                Context.ExecuteQuery();

                foreach (List list in result)
                {
                    try
                    {
                        if (list.Title != GlobalNames.Lists.KancelariaPliki &&
                            list.Title != GlobalNames.Lists.KancelariaStrony
                            )
                        {
                            List oList = web.Lists.GetByTitle(list.Title);
                            string isDictionary = oList.GetProperty("IsDictionary");
                            if (isDictionary == "1")
                            {
                                var view = oList.DefaultView;
                                Context.Load(view);
                                Context.ExecuteQuery();

                                SlownikItem newSlownik = new SlownikItem();
                                newSlownik.Nazwa = list.Title;
                                newSlownik.ServerRelativeUrl = view.ServerRelativeUrl;
                                newSlownik.ServerUrl = new Uri(Context.Url).GetLeftPart(UriPartial.Authority);
                                slownikiList.Add(newSlownik);
                            }

                        }
                    }
                    catch (Exception ex) { }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return slownikiList;
        }

        public override DictionaryItem ConvertToEntity(SP.ListItem item)
        {
            DictionaryItem dictionaryItem = new DictionaryItem();
            try
            {
                dictionaryItem.Id = item.Id;
                dictionaryItem.Value = item.GetValue<string>("Title");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return dictionaryItem;
        }

        public override SP.ListItem ConvertToItem(DictionaryItem entity, SP.ListItem item)
        {
            try
            {
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }

        public DictionaryItem<T> GetEntityById<T>(int id, string listName)
            where T : ICustomEntity
        {
            SP.List spList = Context.Web.Lists.GetByTitle(listName);
            SP.ListItem item = spList.GetItemById(id);

            Context.Load(item);
            Context.ExecuteQuery();

            return ConvertToEntity<T>(item);
        }

        public DictionaryItem<T> ConvertToEntity<T>(SP.ListItem item)
                      where T : ICustomEntity
        {
            DictionaryItem<T> dictionaryItem = new DictionaryItem<T>();
            try
            {
                dictionaryItem.Id = item.Id;
                dictionaryItem.Value = item.GetValue<string>("Title");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return dictionaryItem;
        }

        public SP.ListItem ConvertToItem<T>(DictionaryItem<T> entity, SP.ListItem item)
                      where T : ICustomEntity
        {
            try
            {
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }

        #endregion

        #region custom methods

        public List<DictionaryItem> GetSprawyForKontaktId(int kontaktId, string listName)
        {
            string whereQuery = string.Format("<Query><Where><And>{0}<Eq><FieldRef Name='Kontakt' LookupId='TRUE' /><Value Type='Lookup'>{1}</Value></Eq></And></Where></Query>", DeleteQuery, kontaktId);
            SP.CamlQuery query = new SP.CamlQuery();
            query.ViewXml = string.Format(BaseView, whereQuery);

            return this.GetEntityCustomList(query,listName).ToList();
        }

        #endregion
    }
}