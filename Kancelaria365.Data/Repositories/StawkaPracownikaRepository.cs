﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Data
{
    public class StawkaPracownikaRepository
        : GenericRepositoryBase<StawkaPracownika>, IRepositoryBase<StawkaPracownika>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public override string KeyId
        {
            get
            {
                //Należy recznie utworzy alias otakiej naziwie w management propeties
                throw new NotImplementedException();
            }
        }

        public override string ListName
        {
            get
            {
                return GlobalNames.Lists.StawkiPracownikow;
            }
        }

        public override string BaseView
        {
            get
            {
                return @"<View>{0}</View>";
            }
        }

        public StawkaPracownikaRepository()
        : base()
        {
        }

        public StawkaPracownikaRepository(SP.ClientContext context)
                : base(context)
        {
        }

        public override StawkaPracownika ConvertToEntity(SP.ListItem item)
        {
            StawkaPracownika entity = new StawkaPracownika();
            try
            {
                entity.Id = item.Id;
                entity.Title = item.GetValue<string>("Title");
                entity.Kontakt = item.GetDictionaryItemValue<SP.FieldLookupValue>("Kontakt");
                entity.StawkiData = item.GetValue<DateTime>("StawkiData");
                entity.Stawka = item.GetValue<double>("Stawka");
                entity.Opis = item.GetValue<string>("Opis");
                entity.KategoriaPracownika = item.GetDictionaryItemValue<SP.FieldLookupValue>("KategoriaPracownika");
                entity.KontaktId = item.GetValue<string>("KontaktId");                
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return entity;
        }

        public override SP.ListItem ConvertToItem(StawkaPracownika entity, SP.ListItem item)
        {
            try
            {
                item.SetValue<string>("Title", entity.Title);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("Kontakt", entity.Kontakt);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("KategoriaPracownika", entity.KategoriaPracownika);
                item.SetValue<DateTime?>("StawkiData", entity.StawkiData);
                item.SetValue<double>("Stawka", entity.Stawka);
                item.SetValue<string>("Opis", entity.Opis);
                item.SetValue<string>("KontaktId", entity.KontaktId);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }
    }
}