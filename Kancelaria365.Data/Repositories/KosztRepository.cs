﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Data
{
    public class KosztRepository :
                GenericRepositoryBase<Koszt>, IRepositoryBase<Koszt>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override string KeyId
        {
            get
            {
                //Należy recznie utworzy alias otakiej naziwie w management propeties
                throw new NotImplementedException();
            }
        }

        public override SP.CamlQuery CreateCustomQuery(SearchParameters searchParameters)
        {
            if (searchParameters.UseCustomDataSource)
            {
                SP.CamlQuery query = new SP.CamlQuery();
                string customQuery = string.Empty;

                switch (searchParameters.CustomDataSourceName)
                {
                    case "Koszt":
                        SimpleSearch sprawa = searchParameters.SimpleSearch.FirstOrDefault(x => x.PropertyName == "SprawaId");
                        SimpleSearch kontakt = searchParameters.SimpleSearch.FirstOrDefault(x => x.PropertyName == "KontaktId");

                        if (kontakt != null && kontakt.Value != null)
                        {
                            customQuery = "<Eq><FieldRef Name='Firma' LookupId='TRUE'/><Value Type='Lookup'>" + kontakt.Value + "</Value></Eq>";
                            if (sprawa != null && sprawa.Value != null)
                            {
                                string sprawaWhere = "<Eq><FieldRef Name='Sprawa' LookupId='TRUE'/><Value Type='Lookup'>" + sprawa.Value + "</Value></Eq>";
                                customQuery = string.Format("<And>{0}{1}</And>", customQuery, sprawaWhere);
                            }

                            customQuery = string.Format(@"<View><Query><Where><And>{1}{0}</And></Where></Query></View>", DeleteQuery, customQuery);
                            query.ViewXml = customQuery; ;
                        }
                        else
                        {
                            query = null;
                        }
                        break;
                    case "Koszt-Moje":
                        UserRepository userRepo = new UserRepository(Context);
                        customQuery = "<Eq><FieldRef Name='Wprowadzajacy' LookupId='TRUE'/><Value Type='Integer'>" + userRepo.CurrentUser.Id + "</Value></Eq>";
                        customQuery = string.Format(@"<View><Query><Where><And>{1}{0}</And></Where></Query></View>", DeleteQuery, customQuery);
                        query.ViewXml = customQuery; ;

                        break;
                    default:
                        throw new NotImplementedException();
                }
                return query;
            }
            else
            {
                return null;
            }
        }

        public override string ListName
        {
            get
            {
                return GlobalNames.Lists.Koszty;
            }
        }

        public override string BaseView
        {
            get
            {
                return @"<View>{0}</View>";
            }
        }

        public KosztRepository()
            : base()
        {
        }

        public KosztRepository(SP.ClientContext context)
            : base(context)
        {
            Context = context;
        }

        public override Koszt ConvertToEntity(SP.ListItem item)
        {
            Koszt entity = new Koszt();
            try
            {
                entity.Id = item.Id;
                entity.Title = item.GetValue<string>("Title");
                entity.SprawaId = item.GetValue<string>("SprawaId");
                entity.KontaktId = item.GetValue<string>("KontaktId");
                entity.Sprawa = item.GetTypedDictionaryItemValue<Sprawa, SP.FieldLookupValue>("Sprawa");
                entity.Firma = item.GetTypedDictionaryItemValue<Kontakt, SP.FieldLookupValue>("Firma");

                entity.Kategoria = item.GetDictionaryItemValue<SP.FieldLookupValue>("Kategoria");
                entity.Wprowadzajacy = item.GetTypedDictionaryItemValue<CustomUser, SP.FieldUserValue>("Wprowadzajacy");
                entity.Waluta = item.GetDictionaryItemValue<SP.FieldLookupValue>("Waluta");
                entity.StawkaVat = item.GetDictionaryItemValue<SP.FieldLookupValue>("StawkaVat");

                entity.NumerFaktury = item.GetValue<string>("NumerFaktury");
                entity.PublikujWPKWeb = item.GetValue<bool>("PublikujWPKWeb");
                entity.ObciazycKlienta = item.GetValue<bool>("ObciazycKlienta");
                entity.Data = item.GetValue<DateTime>("Data");
                entity.Uwagi = item.GetValue<string>("Uwagi");
                entity.Opis = item.GetValue<string>("Opis");

                entity.KwotaNetto = item.GetValue<decimal>("KwotaNetto");
                entity.KwotaBrutto = item.GetValue<decimal>("KwotaBrutto");
                entity.Kurs = item.GetValue<decimal>("Kurs");
                entity.PKWiU = item.GetValue<string>("PKWiU");

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return entity;
        }

        public override SP.ListItem ConvertToItem(Koszt entity, SP.ListItem item)
        {
            try
            {
                item.SetValue<string>("Title", entity.Title);
                item.SetValue<string>("SprawaId", entity.Sprawa.Id.ToString());
                item.SetValue<string>("KontaktId", entity.Firma.Id.ToString());

                item.SetDictionaryItemValue<SP.FieldLookupValue>("Firma", entity.Firma);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("Sprawa", entity.Sprawa);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("Kategoria", entity.Kategoria);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("StawkaVat", entity.StawkaVat);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("Waluta", entity.Waluta);
                item.SetDictionaryItemValue<SP.FieldUserValue>("Wprowadzajacy", entity.Wprowadzajacy);
                item.SetValue<string>("NumerFaktury", entity.NumerFaktury);
                item.SetValue<bool>("PublikujWPKWeb", entity.PublikujWPKWeb);
                item.SetValue<bool>("ObciazycKlienta", entity.ObciazycKlienta);
                item.SetValue<DateTime>("Data", entity.Data);

                item.SetValue<string>("Uwagi", entity.Uwagi);
                item.SetValue<string>("Opis", entity.Opis);
                item.SetValue<decimal>("KwotaBrutto", entity.KwotaBrutto);
                item.SetValue<decimal>("KwotaNetto", entity.KwotaNetto);
                item.SetValue<decimal>("Kurs", entity.Kurs);
                item.SetValue<string>("PKWiU", entity.PKWiU);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }
    }
}