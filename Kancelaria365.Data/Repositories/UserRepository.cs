﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Data
{
    public class UserRepository
            : GenericRepositoryBase<CustomUser>, IRepositoryBase<CustomUser>
    {
        public override string BaseView
        {
            get
            {
                return @"<View>{0}</View>";
            }
        }

        public string UserqQuery
        {
            get
            {
                return "<Query><Where>" +
                           "<Or>" +
                               "<BeginsWith>" +
                                   "<FieldRef Name='Title'/><Value Type='Text'>{0}</Value>" +
                               "</BeginsWith>" +
                               "<BeginsWith>" +
                                   "<FieldRef Name='ImnName'/><Value Type='Text'>{0}</Value>" +
                               "</BeginsWith>" +
                            "</Or>" +
                       "</Where></Query>";
            }
        }

        public override string ListName
        {
            get
            {
                return "Lista informacji o użytkowniku";
            }
        }

        private SP.User currentUser;
        public SP.User CurrentUser
        {
            get
            {
                if (currentUser == null)
                {
                    currentUser = Context.Web.CurrentUser;
                    Context.Load(currentUser, user => user.Title);
                    Context.Load(currentUser, user => user.Id);
                    Context.Load(currentUser, user => user.LoginName);
                    Context.ExecuteQuery();
                    return currentUser;
                }
                else
                {
                    return currentUser;
                }
            }
        }

        public List<CustomUser> GetUserStartsWith(string value)
        {
            string whereQuery = string.Format(BaseView, string.Format(UserqQuery, value));
            SP.CamlQuery query = new SP.CamlQuery();
            query.ViewXml = whereQuery;
            return GetEntityCustomList(query);
        }

        public override CustomUser ConvertToEntity(SP.ListItem item)
        {
            CustomUser entity = new CustomUser();
            entity.Id = item.Id;
            entity.Title = item.GetValue<string>("Title");
            entity.ImnName = item.GetValue<string>("Name");
            entity.Department = item.GetValue<string>("Department");
            entity.JobTitle = item.GetValue<string>("JobTitle");
            return entity;
        }

        public override SP.ListItem ConvertToItem(CustomUser entity, SP.ListItem item)
        {
            return item;
        }


        public UserRepository(SP.ClientContext context)
            : base(context)
        {
            Context = context;
        }
    }
}