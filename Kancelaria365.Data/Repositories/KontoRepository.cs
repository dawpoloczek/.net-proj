﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Data
{
    public class KontoRepository
        : GenericRepositoryBase<Konto>, IRepositoryBase<Konto>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override string KeyId
        {
            get
            {
                //Należy recznie utworzy alias otakiej naziwie w management propeties
                throw new NotImplementedException();
            }
        }

        public override string ListName
        {
            get
            {
                return GlobalNames.Lists.Konta;
            }
        }

        public override string BaseView
        {
            get
            {
                return @"<View>{0}</View>";
            }
        }

        public KontoRepository()
            : base()
        { }

        public KontoRepository(SP.ClientContext context)
                : base(context)
        {
        }

        public List<Konto> GetEntityListForKancelariaId(int id)
        {
            string whereQuery = string.Format("<Query><Where><And>{0}<Eq><FieldRef Name='Kancelaria' LookupId='TRUE' /><Value Type='Lookup'>{1}</Value></Eq></And></Where></Query>", DeleteQuery, id);
            SP.CamlQuery query = new SP.CamlQuery();
            query.ViewXml = string.Format(BaseView, whereQuery);

            return this.GetEntityCustomList(query).ToList();
        }

        public override Konto ConvertToEntity(SP.ListItem item)
        {
            Konto entity = new Konto();
            try
            {
                entity.Id = item.Id;
                entity.Title = item.GetValue<string>("Title");
                entity.IBAN = item.GetValue<string>("IBAN");
                entity.Komentarz = item.GetValue<string>("Komentarz");
                entity.Swift = item.GetValue<string>("Swift");
                //entity.KontaktId = item.GetValue<string>("KontaktId");
                entity.Kontakt = item.GetDictionaryItemValue<SP.FieldLookupValue>("Kontakt");

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return entity;
        }

        public override SP.ListItem ConvertToItem(Konto entity, SP.ListItem item)
        {
            try
            {
                item.SetValue<string>("Title", entity.Title);
                item.SetValue<string>("IBAN", entity.IBAN);
                item.SetValue<string>("Swift", entity.Swift);
                item.SetValue<string>("Komentarz", entity.Komentarz);
                item.SetValue<string>("KontaktId", entity.KontaktId);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("Kontakt", entity.Kontakt);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }
    }
}