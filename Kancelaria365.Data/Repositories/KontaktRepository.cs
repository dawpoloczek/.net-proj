﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Data
{
    public class KontaktRepository
        : GenericRepositoryBase<Kontakt>, IRepositoryBase<Kontakt>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override string EntityListCacheKey { get { return "KontaktEntityListCacheKey"; } }
        public override string KeyId
        {
            get
            {
                //Należy recznie utworzy alias otakiej naziwie w management propeties
                return "KontaktIdMP";
            }
        }
        public override string ListName
        {
            get
            {
                return GlobalNames.Lists.Kontakty;
            }
        }

        public override string BaseView
        {
            get
            {
                return @"<View>{0}</View>";
            }
        }

        public KontaktRepository()
            : base()
        { }

        public KontaktRepository(SP.ClientContext context)
                : base(context)
        {
        }

        public override Kontakt ConvertToEntity(SP.ListItem item)
        {    
            Kontakt entity = new Kontakt();
     
            try
            {
                entity.Id = item.Id;
                entity.Title = item.GetValue<string>("Title");
                entity.OutlookContact.Value = item.GetValue<string>("OutlookContactDisplayName");
                entity.OutlookContact.Id = item.GetValue<string>("OutlookContactID");
                entity.NazwaPelna = item.GetValue<string>("NazwaPelna");
                entity.Kod = item.GetValue<string>("Kod");
                entity.Ulica = item.GetValue<string>("Ulica");
                entity.Miasto = item.GetValue<string>("Miasto");
                entity.Kraj = item.GetValue<string>("Kraj");
                entity.Wojewodztwo = item.GetValue<string>("Wojewodztwo");
                entity.Terytorium = item.GetValue<string>("Terytorium");
                entity.KorespondencjaKod = item.GetValue<string>("KorespondencjaKod");
                entity.KorespondencjaUlica = item.GetValue<string>("KorespondencjaUlica");
                entity.KorespondencjMiasto = item.GetValue<string>("KorespondencjaMiasto");
                entity.KorespondencjKraj = item.GetValue<string>("KorespondencjaKraj");
                entity.KorespondencjWojewodztwo = item.GetValue<string>("KorespondencjaWojewodztwo");
                entity.KorespondencjTerytorium = item.GetValue<string>("KorespondencjaTerytorium");
                entity.InnyAdresKorespondencyjny = item.GetValue<bool>("InnyAdresKorespondencyjny");
                entity.TelefonFirma = item.GetValue<string>("TelefonFirma");
                entity.TelefonKomorka = item.GetValue<string>("TelefonKomorka");
                entity.TelefonPraca = item.GetValue<string>("TelefonPraca");
                entity.TelefonAsystent = item.GetValue<string>("TelefonAsystent");
                entity.Email = item.GetValue<string>("Email");
                entity.Skype = item.GetValue<string>("Skype");
                entity.WWWUrl = item.GetValue<string>("WWWUrl");
                entity.OsobaNazwiskoRodowe = item.GetValue<string>("NazwiskoRodowe");
                entity.OsobaImionaRodzicow = item.GetValue<string>("ImionaRodzicow");
                entity.FirmaSpolkaMatka = item.GetDictionaryItemValue<SP.FieldUserValue>("SpolkaMatka");
                //entity.FirmaFormaPrawna = item.GetValue<string>("FormaPrawna");
                entity.Kategoria = item.GetDictionaryItemValue<SP.FieldLookupValue>("Kategoria");
                entity.Kancelaria = item.GetDictionaryItemValue<SP.FieldLookupValue>("Kancelaria");

                entity.Prowadzacy = new UserDictionaryItem(item.GetDictionaryItemValue<SP.FieldUserValue>("Prowadzacy"));
                entity.Wprowadzajacy = new UserDictionaryItem(item.GetDictionaryItemValue<SP.FieldUserValue>("Wprowadzajacy"));

                entity.DataWprowadzenia = item.GetValue<DateTime>("DataWprowadzenia");
                entity.Status = item.GetValue<string>("Status");
                entity.TypKontaktu = item.GetValue<string>("TypKontaktu");
                entity.NIP = item.GetValue<string>("NIP");
                entity.KRS = item.GetValue<string>("KRS");
                entity.NIPUE = item.GetValue<string>("NIPUE");
                entity.Regon = item.GetValue<string>("Regon");
                entity.NumerWPK = item.GetValue<string>("NumerWPK");
                entity.DodatkowyIdentyfikator = item.GetValue<string>("DodatkowyIdentyfikator");
                entity.EKD = item.GetValue<string>("EKD");

                entity.GodzinyPracy = item.GetValue<string>("GodzinyPracy");
                entity.DataRozpoczeciaDzialalnosci = item.GetValue<DateTime?>("DataRozpoczeciaDzialalnosci");
                entity.WpisDoRejestru = item.GetValue<string>("WpisDoRejestru");
                entity.RodzajDzialalnosci = item.GetValue<string>("RodzajDzialalnosci");
                entity.Zarzad = item.GetValue<string>("Zarzad");
                entity.Uwagi = item.GetValue<string>("Uwagi");
                entity.ZrodloDanych = item.GetValue<string>("ZrodloDanych");

                entity.FormaRozliczenia = item.GetValue<string>("FormaRozliczenia");
                entity.OpisNaFakture = item.GetValue<string>("OpisNaFakture");
                entity.DomyslneKontoKancelarii = item.GetValue<string>("DomyslneKontoKancelarii");
                entity.WalutaFaktury = item.GetDictionaryItemValue<SP.FieldLookupValue>("WalutaFaktury");
                entity.DomyslnaStawkaVat = item.GetDictionaryItemValue<SP.FieldLookupValue>("DomyslnaStawkaVat");
                entity.OswiadczenieOWystawieniuBezPodpisu = item.GetValue<bool>("OswiadczenieOWystawieniuBezPodpisu");
                entity.RyczaltAdministracyjny = item.GetValue<double>("RyczaltAdministracyjny");
                entity.DomyslnyRabat = item.GetValue<double>("DomyslnyRabat");
                entity.PokazujZestawienieGodzinoweDlaRyczaltow = item.GetValue<bool>("PokazujZestawienieGodzinoweDlaRyczaltow");
                entity.FormaPlatnosci = entity.FormaPlatnosci = item.GetValue<string>("FormaPlatnosci");
                entity.DomyslnySzablonFaktury = item.GetValue<string>("DomyslnySzablonFaktury");
                entity.DomyslneOpisyPozycjiNaFakturzePolskie = item.GetValue<bool>("DomyslneOpisyPozycjiNaFakturzePolskie");
                entity.PKWiU = item.GetValue<string>("PKWiU");
                entity.PremiaZaSukces = item.GetValue<double>("PremiaZaSukces");
                entity.OdKwoty = item.GetValue<double>("OdKwoty");
                entity.OdKwoty = item.GetValue<double>("LimitGodzinNiezafakturowanychDoAlarmu");
                entity.BudzetLimitKosztowNiezafakturowanychDoAlarmu = item.GetValue<string>("BudzetLimitKosztowNiezafakturowanychDoAlarmu");
                entity.WalutaStawekGodzinowych = item.GetDictionaryItemValue<SP.FieldLookupValue>("WalutaStawekGodzinowych");
                entity.Notatki = item.GetValue<string>("Notatki");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return entity;
        }

        public override SP.ListItem ConvertToItem(Kontakt entity, SP.ListItem item)
        {
            try
            { 
                item.SetValue<string>("Title", entity.Title);
                item.SetValue<string>("OutlookContactDisplayName", entity.OutlookContact.Value);
                item.SetValue<string>("OutlookContactID", entity.OutlookContact.Id);
                item.SetValue<string>("NazwaPelna", entity.NazwaPelna);
                item.SetValue<string>("Kod", entity.Kod);
                item.SetValue<string>("Ulica", entity.Ulica);
                item.SetValue<string>("Miasto", entity.Miasto);
                item.SetValue<string>("Kraj", entity.Kraj);
                item.SetValue<string>("Wojewodztwo", entity.Wojewodztwo);
                item.SetValue<string>("Terytorium", entity.Terytorium);
                item.SetValue<string>("KorespondencjaKod", entity.KorespondencjaKod);
                item.SetValue<string>("KorespondencjaUlica", entity.KorespondencjaUlica);
                item.SetValue<string>("KorespondencjaMiasto", entity.KorespondencjMiasto);
                item.SetValue<string>("KorespondencjaKraj", entity.KorespondencjKraj);
                item.SetValue<string>("KorespondencjaWojewodztwo", entity.KorespondencjWojewodztwo);
                item.SetValue<string>("KorespondencjaTerytorium", entity.KorespondencjTerytorium);
                item.SetValue<bool>("InnyAdresKorespondencyjny", entity.InnyAdresKorespondencyjny);
                item.SetValue<string>("TelefonFirma", entity.TelefonFirma);
                item.SetValue<string>("TelefonKomorka", entity.TelefonKomorka);
                item.SetValue<string>("TelefonPraca", entity.TelefonPraca);
                item.SetValue<string>("TelefonAsystent", entity.TelefonAsystent);
                item.SetValue<string>("Email", entity.Email);
                item.SetValue<string>("Skype", entity.Skype);
                item.SetValue<string>("WWWUrl", entity.WWWUrl);
                item.SetValue<string>("NazwiskoRodowe", entity.OsobaNazwiskoRodowe);
                item.SetValue<string>("ImionaRodzicow", entity.OsobaImionaRodzicow);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("SpolkaMatka", entity.FirmaSpolkaMatka);
                // item.SetValue<string>("FormaPrawna", entity.FirmaFormaPrawna);

                item.SetDictionaryItemValue<SP.FieldLookupValue>("Kategoria", entity.Kategoria);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("Kancelaria", entity.Kancelaria);
                item.SetDictionaryItemValue<SP.FieldUserValue>("Prowadzacy", entity.Prowadzacy);
                item.SetDictionaryItemValue<SP.FieldUserValue>("Wprowadzajacy", entity.Wprowadzajacy);

                item.SetValue<string>("TypKontaktu", entity.TypKontaktu);
                item.SetValue<string>("NIP", entity.NIP);
                item.SetValue<string>("KRS", entity.KRS);
                item.SetValue<string>("NIPUE", entity.NIPUE);
                item.SetValue<string>("Regon", entity.Regon);
                item.SetValue<string>("NumerWPK", entity.NumerWPK);
                item.SetValue<string>("DodatkowyIdentyfikator", entity.DodatkowyIdentyfikator);
                item.SetValue<string>("EKD", entity.EKD);

                item.SetValue<DateTime?>("DataWprowadzenia", entity.DataWprowadzenia);
                item.SetValue<string>("Status", entity.Status);

                item.SetValue<string>("GodzinyPracy", entity.GodzinyPracy);
                item.SetValue<DateTime?>("DataRozpoczeciaDzialalnosci", entity.DataRozpoczeciaDzialalnosci);
                item.SetValue<string>("WpisDoRejestru", entity.WpisDoRejestru);
                item.SetValue<string>("RodzajDzialalnosci", entity.RodzajDzialalnosci);
                item.SetValue<string>("Zarzad", entity.Zarzad);
                item.SetValue<string>("Uwagi", entity.Uwagi);
                item.SetValue<string>("ZrodloDanych", entity.ZrodloDanych);

                item.SetValue<string>("FormaRozliczenia", entity.FormaRozliczenia);
                item.SetValue<string>("OpisNaFakture", entity.OpisNaFakture);
                item.SetValue<string>("DomyslneKontoKancelarii", entity.DomyslneKontoKancelarii);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("WalutaFaktury", entity.WalutaFaktury);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("DomyslnaStawkaVat", entity.DomyslnaStawkaVat);
                item.SetValue<bool>("OswiadczenieOWystawieniuBezPodpisu", entity.OswiadczenieOWystawieniuBezPodpisu);
                item.SetValue<double>("RyczaltAdministracyjny", entity.RyczaltAdministracyjny);
                item.SetValue<double>("DomyslnyRabat", entity.DomyslnyRabat);
                item.SetValue<bool>("PokazujZestawienieGodzinoweDlaRyczaltow", entity.PokazujZestawienieGodzinoweDlaRyczaltow);
                item.SetValue<string>("FormaPlatnosci", entity.FormaPlatnosci);
                item.SetValue<string>("DomyslnySzablonFaktury", entity.DomyslnySzablonFaktury);
                item.SetValue<bool>("DomyslneOpisyPozycjiNaFakturzePolskie", entity.DomyslneOpisyPozycjiNaFakturzePolskie);
                item.SetValue<string>("PKWiU", entity.PKWiU);
                item.SetValue<double>("PremiaZaSukces", entity.PremiaZaSukces);
                item.SetValue<double>("OdKwoty", entity.OdKwoty);
                item.SetValue<double>("LimitGodzinNiezafakturowanychDoAlarmu", entity.OdKwoty);
                item.SetValue<string>("BudzetLimitKosztowNiezafakturowanychDoAlarmu", entity.BudzetLimitKosztowNiezafakturowanychDoAlarmu);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("WalutaStawekGodzinowych", entity.WalutaStawekGodzinowych);
                item.SetValue<string>("Notatki", entity.Notatki);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }
    }
}