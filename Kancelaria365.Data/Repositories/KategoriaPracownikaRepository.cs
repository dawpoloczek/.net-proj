﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Data
{
    public class KategoriaPracownikaRepository
        : GenericRepositoryBase<KategoriaPracownika>, IRepositoryBase<KategoriaPracownika>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override string KeyId
        {
            get
            {
                //Należy recznie utworzy alias otakiej naziwie w management propeties
                throw new NotImplementedException();
            }
        }

        public override string ListName
        {
            get
            {
                return GlobalNames.Lists.KategoriePracownikow;
            }
        }

        public override string BaseView
        {
            get
            {
                return @"<View>{0}</View>";
            }
        }

        public KategoriaPracownikaRepository(SP.ClientContext context)
                : base(context)
        {
        }

        public override KategoriaPracownika ConvertToEntity(SP.ListItem item)
        {
            KategoriaPracownika entity = new KategoriaPracownika();
            try
            {
                entity.Id = item.Id;
                entity.Title = item.GetValue<string>("Title");
                entity.Priorytet = item.GetValue<double>("Priorytet");
                entity.Stawka = item.GetValue<double>("Stawka");
                entity.Opis = item.GetValue<string>("Opis");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return entity;
        }

        public override SP.ListItem ConvertToItem(KategoriaPracownika entity, SP.ListItem item)
        {
            try
            {
                item.SetValue<string>("Title", entity.Title);
                item.SetValue<double>("Priorytet", entity.Priorytet);
                item.SetValue<double>("Stawka", entity.Stawka);
                item.SetValue<string>("Opis", entity.Opis);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }
    }
}