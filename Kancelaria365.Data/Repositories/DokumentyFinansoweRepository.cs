﻿using Kancelaria365.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;
using NLog;

namespace Kancelaria365.Data
{
    public class DokumentyFinansoweRepository
        : GenericRepositoryBase<DokumentFinansowy>, IRepositoryBase<DokumentFinansowy>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override string KeyId
        {
            get
            {
                //Należy recznie utworzy alias otakiej naziwie w management propeties
                throw new NotImplementedException();
            }
        }
        public override string ListName
        {
            get
            {
                return GlobalNames.Lists.KontaktyDokumentyFinansowe;
            }
        }


        public DokumentyFinansoweRepository()
                : base()
        {
            DeleteQuery = "";
        }

        public DokumentyFinansoweRepository(SP.ClientContext context)
                : base(context)
        {
            DeleteQuery = "";
        }

        public override DokumentFinansowy ConvertToEntity(SP.ListItem item)
        {
            DokumentFinansowy entity = new DokumentFinansowy();
            try
            {
                Context.Load(item.File);
                Context.ExecuteQuery();
                entity.Id = item.Id;
                entity.Title = item.GetValue<string>("Title");
                entity.FileRef = item.GetValue<string>("FileRef");
                //entity.KontaktId = item.GetValue<string>("KontaktId");
                entity.Kontakt = item.GetDictionaryItemValue<SP.FieldLookupValue>("Kontakt");
                entity.LinkFilenameNoMenu = item.File.Name;
                entity.SiteUrl = new Uri(Context.Url).GetLeftPart(UriPartial.Authority);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return entity;
        }

        public override SP.ListItem ConvertToItem(DokumentFinansowy entity, SP.ListItem item)
        {
            try
            {
                item.SetValue<string>("Title", entity.Title);
                //item.SetValue<string>("KontaktId", entity.KontaktId);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("Kontakt", entity.Kontakt);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }
    }
}