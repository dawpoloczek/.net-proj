﻿using System;
using System.Collections.Generic;
using System.Linq;
using SP = Microsoft.SharePoint.Client;

namespace Kancelaria365.Data
{
    public interface IRepositoryBase<T>
    {
        string ListName { get; }

        string BaseView { get; }

        T GetEntityById(int id);

        List<T> GetEntityList(string listName = "", bool dictionaryDefaultValue = false, SearchParameters searchParameters = null);

        void AddUpdateItem(T entity);

        T ConvertToEntity(SP.ListItem item);

        SP.ListItem ConvertToItem(T entity, SP.ListItem item);
    }
}
