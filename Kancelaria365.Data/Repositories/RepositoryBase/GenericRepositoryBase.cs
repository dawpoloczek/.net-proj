﻿using Kancelaria365.Common;
using Kancelaria365.Data.Repositories.RepositoryBase;
using Microsoft.SharePoint.Client.Search.Query;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using SP = Microsoft.SharePoint.Client;

namespace Kancelaria365.Data
{
    public abstract class GenericRepositoryBase<T>: RepositoryBase
        where T : ICustomEntity, new()
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public GenericRepositoryBase()
        {
        }

        public GenericRepositoryBase(SP.ClientContext context)
        {
            Context = context;
        }

        public void DeleteItem(int id)
        {
            try
            {
                SP.List spList = Context.Web.Lists.GetByTitle(ListName);
                SP.ListItem item = spList.GetItemById(id);

                Context.Load(item);
                try
                {
                    Context.ExecuteQuery();
                    item.SetValue<bool>("Usuniety", true);
                    item.BreakRoleInheritance(false, true);
                    item.Update();
                    Context.ExecuteQuery();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
        }

        public void AddUpdateItem(T entity)
        {
            try
            {
                SP.List spList = Context.Web.Lists.GetByTitle(ListName);
                SP.ListItem item = spList.GetItemById(entity.Id);

                Context.Load(item);
                try
                {
                    Context.ExecuteQuery();
                }
                catch
                {
                    SP.ListItemCreationInformation itemCreateInfo = new SP.ListItemCreationInformation();
                    item = spList.AddItem(itemCreateInfo);
                    Context.ExecuteQuery();
                }
                item = ConvertToItem(entity, item);
                item.Update();
                Context.ExecuteQuery();
                entity.Id = item.Id;
            }
            catch (Exception ex)
            {
                logger.CustomError(ex);
                throw ex;
            }
        }

        public T GetEntityById(int id)
        {
            SP.List spList = Context.Web.Lists.GetByTitle(ListName);
            SP.ListItem item = spList.GetItemById(id);

            Context.Load(item);
            Context.ExecuteQuery();

            return ConvertToEntity(item);
        }

        public virtual List<T> GetEntityList(SearchParameters searchParameters)
        {
            return GetEntityList("", false, searchParameters);
        }

        public virtual List<T> GetEntityList(string listName = "", bool dictionaryDefaultValue = false, SearchParameters searchParameters = null)
        {
            List<T> retrunValue = new List<T>();
            try
            {
                retrunValue = GetEntityList(ConvertToEntity, listName, searchParameters);
                if (dictionaryDefaultValue)
                {
                    ICustomEntity item = new DictionaryItem(0, GlobalNames.Constants.DefaultListValue) as ICustomEntity;
                    retrunValue.Insert(0, (T)item);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return retrunValue;
        }

        public List<T> GetEntityCustomList(SP.CamlQuery query, string listName = "")
        {
            List<T> retrunValue = new List<T>();
            try
            {
                string listTitle = ListName;
                if (!string.IsNullOrEmpty(listName))
                {
                    listTitle = listName;
                }
                if (!string.IsNullOrEmpty(listTitle))
                {
                    SP.List spList = Context.Web.Lists.GetByTitle(listTitle);

                    SP.ListItemCollection listItems = spList.GetItems(query);
                    Context.Load(spList);
                    Context.Load(listItems);
                    Context.ExecuteQuery();

                    if (listItems != null)
                    {
                        retrunValue = listItems.Cast<SP.ListItem>().Select(x => ConvertToEntity(x)).ToList();
                    }
                    else
                    {
                        retrunValue = new List<T>();
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return retrunValue;
        }

        public abstract T ConvertToEntity(SP.ListItem item);

        public abstract SP.ListItem ConvertToItem(T entity, SP.ListItem item);

        private List<T> GetEntityList(Func<SP.ListItem, T> convertFunction, string listName = "", SearchParameters searchParameters = null)
        {
            List<T> retrunValue = new List<T>();
            try
            {
                string listTitle = ListName;
                if (!string.IsNullOrEmpty(listName))
                {
                    listTitle = listName;
                }
                if (!string.IsNullOrEmpty(listTitle))
                {
                    SP.List spList = Context.Web.Lists.GetByTitle(listTitle);

                    SP.CamlQuery query = CreateCustomQuery(searchParameters);
                    if (query == null)
                    {
                        query = AllCamlQuery;
                    }

                    SP.ListItemCollection listItems = spList.GetItems(query);

                    Context.Load(spList);
                    Context.Load(listItems);
                    Context.ExecuteQuery();

                    if (listItems != null)
                    {
                        retrunValue = listItems.Cast<SP.ListItem>().Select(x => convertFunction(x)).ToList();
                    }
                    else
                    {
                        retrunValue = new List<T>();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return retrunValue;
        }

        public List<int> SearchEngine(string searchtText, AdvancedSearch advancedSearch)
        {
            List<int> retrunValue = new List<int>();

            try
            {
                Uri baseUri = new Uri(Context.Url);

                using (Context)
                {
                    string strQuery = searchtText;

                    List<string> allLists = new List<string>();
                    //if (advancedSearch.Sprawy)
                    //    allLists.Add(SPUrlUtility.CombineUrl(Context.Url, "/sprawy"));
                    ////if (advancedSearch.Konta)
                    ////    allLists.Add(SPUrlUtility.CombineUrl(Context.Url, "/konta"));
                    //if (advancedSearch.Zadania)
                    //    allLists.Add(SPUrlUtility.CombineUrl(Context.Url, "/zadania"));
                    //if (advancedSearch.Czynnosci)
                    //    allLists.Add(SPUrlUtility.CombineUrl(Context.Url, "/czynnosci"));
                    //if (advancedSearch.Koszty)
                    //    allLists.Add(SPUrlUtility.CombineUrl(Context.Url, "/koszty"));
                    //if (advancedSearch.Kontakty)
                    //    allLists.Add(SPUrlUtility.CombineUrl(Context.Url, "/kontakty"));

                    foreach (string strPath in allLists)
                    {
                        strQuery += String.Format(" Path:{0}", strPath);
                    }

                    KeywordQuery keywordQuery = new KeywordQuery(Context);
                    var properties = keywordQuery.SelectProperties;
                    properties.Add("ListID");
                    properties.Add("ListItemID");
                    properties.Add(KeyId);

                    keywordQuery.QueryText = strQuery + " (contentclass:STS_ListItem)";

                    SearchExecutor searchExecutor = new SearchExecutor(Context);
                    SP.ClientResult<ResultTableCollection> results = searchExecutor.ExecuteQuery(keywordQuery);
                    Context.ExecuteQuery();

                    List<string> ids = results.Value[0].ResultRows
                        .GroupBy(x => x[KeyId]).Where(x => x.Key != null).Select(x => x.Key.ToString()).ToList();


                    ids.ForEach(x => retrunValue.Add(Convert.ToInt32(x)));
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return retrunValue;
        }
    }
}