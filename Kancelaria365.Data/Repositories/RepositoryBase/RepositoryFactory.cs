﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint.Client;

namespace Kancelaria365.Data
{
    public class RepositoryFactory
    {
        public static IRepositoryBase<T> Create<T>(ClientContext context)
        {
            Type type = typeof(T);
            if (type == typeof(CzynnoscRepository))
                return (IRepositoryBase<T>)new CzynnoscRepository(context);
            else if (type == typeof(KontaktRepository))
                return (IRepositoryBase<T>)new KontaktRepository(context);
            else if (type == typeof(KontoRepository))
                return (IRepositoryBase<T>)new KontoRepository(context);
            else if (type == typeof(KosztRepository))
                return (IRepositoryBase<T>)new KosztRepository(context);
            else if (type == typeof(SlownikRepository))
                return (IRepositoryBase<T>)new SlownikRepository(context);
            else if (type == typeof(SprawaRepository))
                return (IRepositoryBase<T>)new SprawaRepository(context);
            else
                return null;
        }
    }
}
