﻿using Kancelaria365.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using SP = Microsoft.SharePoint.Client;

namespace Kancelaria365.Data.Repositories.RepositoryBase
{
    public abstract class RepositoryBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public string DeleteQuery = "<Neq><FieldRef Name='Usuniety'/><Value Type='Boolean'>1</Value></Neq>";

        public abstract string ListName { get; }

        public virtual string BaseView
        {
            get
            {
                return @"<View>{0}</View>";
            }
        }

        public virtual string KeyId { get; }

        public virtual string EntityListCacheKey { get; }

        public SP.ClientContext Context { get; set; }

        public virtual SP.CamlQuery AllCamlQuery
        {
            get
            {
                string whereQuery = string.Format(@"<Query><Where>{0}</Where></Query>", DeleteQuery);

                SP.CamlQuery query = new SP.CamlQuery();
                query.ViewXml = string.Format(BaseView, whereQuery);
                return query;
            }
        }

        public virtual SP.CamlQuery ExposedColumnCamlQuery(string columnName)
        {
            string whereQuery = string.Format(@"<ViewFields><FieldRef Name='{0}' /></ViewFields><Query><Where>{1}</Where></Query>", columnName, DeleteQuery);

            SP.CamlQuery query = new SP.CamlQuery();
            query.ViewXml = string.Format(BaseView, whereQuery);
            return query;
        }

        public virtual SP.CamlQuery CreateCustomQuery(SearchParameters searchParameters)
        {
            return null;
        }

        public List<string> GetChoices(string fieldName, bool dictionaryDefaultValue = false)
        {
            List<string> returnList = null;
            try
            {
                SP.List spList = Context.Web.Lists.GetByTitle(ListName);
                SP.FieldChoice choiceField = Context.CastTo<SP.FieldChoice>(Context.Web.Lists.GetByTitle(ListName).Fields.GetByInternalNameOrTitle(fieldName));
                Context.Load(choiceField);
                Context.ExecuteQuery();

                returnList = new List<string>(choiceField.Choices);

                if (dictionaryDefaultValue)
                {
                    returnList.Insert(0, GlobalNames.Constants.DefaultListValue);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnList;
        }

        public int GetEntitiesCount(string listName = "")
        {
            int retrunValue = 0;
            string listTitle = ListName;
            if (!string.IsNullOrEmpty(listName))
            {
                listTitle = listName;
            }
            if (!string.IsNullOrEmpty(listTitle))
            {
                SP.List spList = Context.Web.Lists.GetByTitle(listTitle);

                SP.ListItemCollection listItems = spList.GetItems(AllCamlQuery);
                Context.Load(spList);
                Context.Load(listItems);
                Context.ExecuteQuery();

                if (listItems != null)
                {
                    retrunValue = listItems.Count();
                }
                else
                {
                    retrunValue = 0;
                }
            }
            return retrunValue;
        }

        public double GetColumnSum(string columnName, string listName = "")
        {
            double retrunValue = 0;
            try
            {
                string listTitle = ListName;
                if (!string.IsNullOrEmpty(listName))
                {
                    listTitle = listName;
                }
                if (!string.IsNullOrEmpty(listTitle))
                {
                    SP.List spList = Context.Web.Lists.GetByTitle(listTitle);
                    SP.CamlQuery query = ExposedColumnCamlQuery(columnName);
                    SP.ListItemCollection listItems = spList.GetItems(query);
                    Context.Load(spList);
                    Context.Load(listItems);
                    Context.ExecuteQuery();

                    if (listItems != null)
                    {
                        retrunValue = listItems.Cast<SP.ListItem>().Select(x => Convert.ToDouble(x[columnName].ToString())).Sum();
                    }
                    else
                    {
                        retrunValue = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return retrunValue;
        }
    }
}
