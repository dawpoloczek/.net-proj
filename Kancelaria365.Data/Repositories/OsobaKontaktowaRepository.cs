﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Data
{
    public class OsobaKontaktowaRepository
        : GenericRepositoryBase<OsobaKontaktowa>, IRepositoryBase<OsobaKontaktowa>
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override string KeyId
        {
            get
            {
                //Należy recznie utworzy alias otakiej naziwie w management propeties
                throw new NotImplementedException();
            }
        }

        public override string ListName
        {
            get
            {
                return GlobalNames.Lists.OsobyKontaktowe;
            }
        }

        public override string BaseView
        {
            get
            {
                return @"<View>{0}</View>";
            }
        }

        public OsobaKontaktowaRepository()
            : base()
        { }

        public OsobaKontaktowaRepository(SP.ClientContext context)
                : base(context)
        {
        }


        public override OsobaKontaktowa ConvertToEntity(SP.ListItem item)
        {
            OsobaKontaktowa entity = new OsobaKontaktowa();
            try
            {
                entity.Id = item.Id;
                entity.Title = item.GetValue<string>("Title");
                entity.Adres = item.GetValue<string>("Adres");
                entity.Telefon = item.GetValue<string>("Telefon");
                entity.Mail = item.GetValue<string>("Mail");
                //entity.KontaktId = item.GetValue<string>("KontaktId");
                entity.Kontakt = item.GetDictionaryItemValue<SP.FieldLookupValue>("Kontakt");
                //if (kanselaria != null)
                //{
                //    entity.KancelariaId = kanselaria.LookupId;
                //}
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return entity;
        }

        public override SP.ListItem ConvertToItem(OsobaKontaktowa entity, SP.ListItem item)
        {
            try
            {
                item.SetValue<string>("Title", entity.Title);
                item.SetValue<string>("Adres", entity.Adres);
                item.SetValue<string>("Telefon", entity.Telefon);
                item.SetValue<string>("Mail", entity.Mail);
                //item.SetValue<string>("KontaktId", entity.KontaktId);
                item.SetDictionaryItemValue<SP.FieldLookupValue>("Kontakt", entity.Kontakt);
                //item.SetValue<SP.FieldLookupValue>("Kancelaria", new SP.FieldLookupValue() { LookupId = entity.KancelariaId });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }

    }
}