﻿using System;
using System.Collections.Generic;
using System.Linq;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Data
{
    public class SprawaRepository :
        GenericRepositoryBase<Sprawa>, IRepositoryBase<Sprawa>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override string EntityListCacheKey { get { return "SprawaEntityListCacheKey"; } }

        public override string KeyId
        {
            get
            {
                //Należy recznie utworzy alias otakiej naziwie w management propeties
                throw new NotImplementedException();
            }
        }

        public override string ListName
        {
            get
            {
                return GlobalNames.Lists.Sprawy;
            }
        }

        public override string BaseView
        {
            get
            {
                return @"<View>{0}</View>";
            }
        }

        public SprawaRepository()
            : base()
        { }

        public SprawaRepository(SP.ClientContext context)
                : base(context)
        {
        }

        public override Sprawa ConvertToEntity(SP.ListItem item)
        {
            Sprawa entity = new Sprawa();
            try
            {
                entity.Id = item.Id;
                entity.Kontakt = item.GetTypedDictionaryItemValue<Kontakt, SP.FieldLookupValue>("Kontakt");
                entity.Title = item.GetValue<string>("Title");
                entity.Kategoria = item.GetDictionaryItemValue<SP.FieldLookupValue>("Kategoria");
                entity.Etap = item.GetValue<string>("Etap");
                entity.NrPorzadkowy = item.GetValue<string>("NrPorzadkowy");
                entity.NrWewnetrzny = item.GetValue<string>("NrWewnetrzny");

                entity.Prowadzacy = new UserDictionaryItem(item.GetDictionaryItemValue<SP.FieldUserValue>("Prowadzacy"));
                entity.Zlecajacy = new UserDictionaryItem(item.GetDictionaryItemValue<SP.FieldUserValue>("Zlecajacy"));
                entity.Sygnatura = item.GetValue<string>("Sygnatura");
                entity.WartUniwer = item.GetValue<string>("WartUniwer");
                entity.Wprowadzajacy = new UserDictionaryItem(item.GetDictionaryItemValue<SP.FieldUserValue>("Wprowadzajacy"));
                entity.DataWprowadzenia = item.GetValue<DateTime>("DataWprowadzenia");
                entity.DataPrzyjecia = item.GetValue<DateTime>("DataPrzyjecia");
                entity.DataZakończenia = item.GetValue<DateTime?>("DataZakonczenia");
                entity.Uwagi = item.GetValue<string>("Uwagi");
                entity.KontaktId = item.GetValue<string>("KontaktId");
               // entity.PlanId = item.GetValue<string>("PlanId");

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return entity;
        }

        public override SP.ListItem ConvertToItem(Sprawa entity, SP.ListItem item)
        {
            try
            {
                item.SetValue<string>("Title", entity.Title);
                item.SetValue<SP.FieldLookupValue>("Kontakt", new SP.FieldLookupValue() { LookupId = entity.Kontakt.Id });
                item.SetValue<string>("KontaktId", entity.Kontakt.Id.ToString());

                item.SetValue<SP.FieldLookupValue>("Kategoria", new SP.FieldLookupValue() { LookupId = entity.Kategoria.Id });
                item.SetDictionaryItemValue<SP.FieldUserValue>("Prowadzacy", entity.Prowadzacy);
                item.SetDictionaryItemValue<SP.FieldUserValue>("Zlecajacy", entity.Zlecajacy);
                item.SetDictionaryItemValue<SP.FieldUserValue>("Wprowadzajacy", entity.Wprowadzajacy);
                item.SetValue<string>("Etap", entity.Etap);
                item.SetValue<string>("NrPorzadkowy", entity.NrPorzadkowy);
                item.SetValue<string>("NrWewnetrzny", entity.NrWewnetrzny);
                item.SetValue<string>("Sygnatura", entity.Sygnatura);
                item.SetValue<string>("WartUniwer", entity.WartUniwer);
                item.SetValue<string>("Uwagi", entity.Uwagi);
                item.SetValue<DateTime>("DataWprowadzenia", entity.DataWprowadzenia);
                item.SetValue<DateTime>("DataPrzyjecia", entity.DataPrzyjecia);
                item.SetValue<DateTime?>("DataZakonczenia", entity.DataZakończenia);
                item.SetValue<string>("KontaktId", entity.KontaktId);
                //item.SetValue<string>("PlanId", entity.PlanId);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return item;
        }
    }
}