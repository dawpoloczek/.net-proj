﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SP = Microsoft.SharePoint.Client;

namespace Kancelaria365.Data
{
    public static class KancelariaLog
    {
        public static void CustomError(this NLog.Logger logger, Exception ex)
        {
            //Type type = ex.GetType();
            if(ex is SP.ServerException)
            {
                var exception = ex as SP.ServerException;
                if (!string.IsNullOrWhiteSpace(exception.ServerErrorValue))
                {
                    logger.Error(exception, exception.ServerErrorValue);
                }
                else
                {
                    logger.Error(exception);

                }
            }
            else
            {
                logger.Error(ex);
            }
        }
    }
}
