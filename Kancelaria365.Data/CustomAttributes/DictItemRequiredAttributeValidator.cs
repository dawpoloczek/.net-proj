﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data.CustomAttributes
{
    public class DictItemRequiredAttributeValidator : RequiredAttribute
    {
        public DictItemRequiredAttributeValidator():
            base()
        {
            ErrorMessage = "Pole jest wymagane.";
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (((DictionaryItem)value).Id == 0)
            {
                return new ValidationResult(base.ErrorMessage);
            }
            return ValidationResult.Success;
        }
    }
}