﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data.CustomAttributes
{
    public class Kancelaria365DateFormat: DisplayFormatAttribute
    {
        public Kancelaria365DateFormat()
        {
            DataFormatString = "{0:dd MMMM yyyy}";
        }
    }
}