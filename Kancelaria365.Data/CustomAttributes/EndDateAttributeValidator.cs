﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data.CustomAttributes
{
    public class EndDateAttributeValidator : ValidationAttribute
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private string controlDatePropName;
        private string controlDatePropDispName;
        public EndDateAttributeValidator(string controlDatePropName, string controlDatePropDispName)
        {
            this.controlDatePropName = controlDatePropName;
            this.controlDatePropDispName = controlDatePropDispName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                if (value != null)
                {
                    object obj = validationContext.ObjectInstance;
                    if (obj != null)
                    {
                        var testProp = obj.GetType().GetProperty(controlDatePropName).GetValue(obj, null);
                        if(testProp is DateTime &&
                           value is DateTime)
                        {
                            DateTime valueDate = (DateTime)value;
                            DateTime testPropDate = (DateTime)testProp;

                            if(valueDate.Date < testPropDate.Date)
                            {
                                return new ValidationResult("Data musi być większa lub równa wartości w: " + controlDatePropDispName);
                            }
                        }
                    }          
                    
                }
                return ValidationResult.Success;
            }
            catch (Exception ex)
            {
                logger.CustomError(ex);
                throw ex;
            }
        }
    }
}