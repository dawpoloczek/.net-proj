﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data.CustomAttributes
{
    public class DictItemConditionalRequiredAttributeValidator : DictItemRequiredAttributeValidator
    {
        private String PropertyName { get; set; }
        private Object DesiredValue { get; set; }
        public DictItemConditionalRequiredAttributeValidator():
            base()
        {
            ErrorMessage = "Pole jest wymagane.";
        }
        public DictItemConditionalRequiredAttributeValidator(String propertyName, Object desiredvalue)
        {
            PropertyName = propertyName;
            DesiredValue = desiredvalue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Object instance = validationContext.ObjectInstance;
            Type type = instance.GetType();
            Object proprtyValue = type.GetProperty(PropertyName).GetValue(instance, null);
            if (proprtyValue.ToString().ToLower() == DesiredValue.ToString().ToLower())
            {
                return base.IsValid(value, validationContext);
            }
            return ValidationResult.Success;
        }
    }
}