﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data.CustomAttributes
{
    public class NIPAttributeValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                string nip = value.ToString();

                int[] weights = { 6, 5, 7, 2, 3, 4, 5, 6, 7 };
                bool result = false;
                if (nip.Length == 10)
                {
                    int controlSum = CalculateControlSum(nip, weights);
                    int controlNum = controlSum % 11;
                    if (controlNum == 10)
                    {
                        controlNum = 0;
                    }
                    int lastDigit = int.Parse(nip[nip.Length - 1].ToString());
                    result = controlNum == lastDigit;
                    if (result)
                    {
                        return ValidationResult.Success;
                    }
                    else
                    {
                        return new ValidationResult("Błędna wartośći");
                    }
                }
                else {
                    return new ValidationResult("Błędna wartośći");
                }
            }
            else
            {
                return new ValidationResult("" + validationContext.DisplayName + " pole jest wymagane");
            }
        }

        private static int CalculateControlSum(string input, int[] weights, int offset = 0)
        {
            int controlSum = 0;
            for (int i = 0; i < input.Length - 1; i++)
            {
                controlSum += weights[i + offset] * int.Parse(input[i].ToString());
            }
            return controlSum;
        }
    }
}