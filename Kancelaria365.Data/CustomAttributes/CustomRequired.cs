﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kancelaria365.Data.CustomAttributes
{
    public class CustomRequired : RequiredAttribute
    {
        public CustomRequired():
            base()
        {
            ErrorMessage = "Pole jest wymagane.";
        }
    }
}
