﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kancelaria365.Data
{
    public enum SaveStatus
    {
        OK,
        ValidationError,
        Exception
    }
}
