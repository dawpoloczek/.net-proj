﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public enum EntityType
    {
        None,
        Zadanie,
        Czynnosc,
        Sprawa,
        Koszt,
        Kontakt
    }
    public enum GridMode
    {
        Simple,
        Advanced
    }

}