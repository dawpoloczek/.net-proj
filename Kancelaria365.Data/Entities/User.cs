﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class CustomUser
         : BaseEntity<CustomUser>, ICustomEntity
    {
        public override int Id { get; set; }

        public override string Value
        {
            get
            {
                return Title;
            }
        }
        public string Title { get; set; }
        public string Name { get; set; }
        public string ImnName { get; set; }
        public string Department { get; set; }
        public string JobTitle { get; set; }
    }
}