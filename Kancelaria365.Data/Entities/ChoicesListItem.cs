﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class ChoicesListItem
    {
        public int Id { get; set; }

        public string Value { get; set; }

        public ChoicesListItem()
        {

        }
        public ChoicesListItem(int id)
        {
            this.Id = id;
        }
        public ChoicesListItem(int id, string value)
        {
            this.Value = value;
            this.Id = id;
        }
    }
}