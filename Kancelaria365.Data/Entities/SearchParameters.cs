﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class AdvancedSearch
    {
        public bool Sprawy { get; set; }
        public bool Konta { get; set; }
        public bool Zadania { get; set; }
        public bool Czynnosci { get; set; }
        public bool Koszty { get; set; }
        public bool Kontakty { get; set; }
    }

    public class SimpleSearch
    {
        public string PropertyName { get; set; }
        public string Value { get; set; }
    }

    public class SearchParameters
    {
        public bool UseCustomDataSource { get; set; }
        public string CustomDataSourceName { get; set; }
        public bool ClearDataSource { get; set; }
        public string SearchtText { get; set; }
        public bool IsAdvancedSearch { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
        public string SortBy { get; set; }
        public string Direction { get; set; }
        public AdvancedSearch AdvancedSearch { get; set; }
        public SimpleSearch[] SimpleSearch { get; set; }
    }

    public class SearchResult<T>
    {
        public T[] Records { get; set; }

        public int Total { get; set; }
    }
}