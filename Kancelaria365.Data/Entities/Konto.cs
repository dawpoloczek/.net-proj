﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class Konto
            : BaseEntity<Konto>, ICustomEntity
    {
        public override int Id { get; set; }

        public override string Value
        {
            get
            {
                return Title;
            }
        }

        public Konto()
        {
            this.Kontakt = new DictionaryItem();
        }
        
        [Display(Name = "Kontakt Id")]
        public string KontaktId { get; set; }

        [Display(Name = "Kontakt")]
        public DictionaryItem Kontakt { get; set; }
        
        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole jest wymagane!")]
        [Display(Name = "Konto")]
        public string Title { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole jest wymagane!")]
        [Display(Name = "Swift")]
        public string Swift { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole jest wymagane!")]
        [Display(Name = "IBAN")]
        public string IBAN { get; set; }

        [Display(Name = "Komentarz")]
        public string Komentarz { get; set; }
    }
}