﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class GridModel
    {
        public int? Id { get; set; }
        public EntityType Type { get; set; }
        public GridMode Mode { get; set; }

        public GridModel()
        {
            Type = EntityType.None;
            Mode = GridMode.Advanced;
        }

        public GridModel(EntityType type, GridMode mode, int id)
        {
            Id = id;
            Type = type;
            Mode = mode;
        }
    }
}