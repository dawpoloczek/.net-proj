﻿using Kancelaria365.Data.CustomAttributes;
using Kancelaria365.Common;
using Microsoft.SharePoint.Client.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;

namespace Kancelaria365.Data
{
    public class Kontakt
            : BaseEntity<Kontakt>, ICustomEntity
    {
        public static string EntityListCacheKey { get { return "KontaktEntityListCacheKey"; } }

        public Kontakt()
            :base()
        {
            this.Sprawy = new List<Sprawa>();
            this.Kategoria = new DictionaryItem();
            this.Kancelaria = new DictionaryItem();
            this.FirmaSpolkaMatka = new DictionaryItem();
            this.DomyslnaStawkaVat = new DictionaryItem();
            this.WalutaFaktury = new DictionaryItem();
            this.WalutaStawekGodzinowych = new DictionaryItem();
            this.OutlookContact = new StringDictionaryItem();
        }

        public Kontakt(int id, string title)
        {
            this.Id = id;
            this.Title = title;
            this.Sprawy = new List<Sprawa>();
            this.Kategoria = new DictionaryItem();
            this.FirmaSpolkaMatka = new DictionaryItem();
            this.Kancelaria = new DictionaryItem();
            this.DomyslnaStawkaVat = new DictionaryItem();
            this.WalutaStawekGodzinowych = new DictionaryItem();
            this.WalutaFaktury = new DictionaryItem();
            this.OutlookContact = new StringDictionaryItem();
        }

        public override int Id { get; set; }

        public override string Value
        {
            get
            {
                return Title;
            }
        }

        [Display(Name = "Outlook kontakt")]
        public StringDictionaryItem OutlookContact{ get; set; }

        [Display(Name = "Nazwa")]
        [CustomRequired(AllowEmptyStrings = false)]
        public string Title { get; set; }

        [Display(Name = "Nazwa pełna")]
        public string NazwaPelna { get; set; }

        [Display(Name = "Kategoria")]
        public DictionaryItem Kategoria { get; set; }

        [Display(Name = "Wprowadzający")]
        public UserDictionaryItem Wprowadzajacy { get; set; }

        [Display(Name = "Kancelaria")]
        public DictionaryItem Kancelaria { get; set; }

        [Display(Name = "Prowadzący")]
        public UserDictionaryItem Prowadzacy { get; set; }

        #region Kontakt typu osoba

        [Display(Name = "Nazwisko rodowe")]
        public string OsobaNazwiskoRodowe { get; set; }

        [Display(Name = "Imiona rodziców")]
        public string OsobaImionaRodzicow { get; set; }

        #endregion

        #region Kontakt typu firma

        [Display(Name = "Spółka matka")]
        public DictionaryItem FirmaSpolkaMatka { get; set; }

        [Display(Name = "Forma prawna")]
        public string FirmaFormaPrawna { get; set; }

        #endregion

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Data wprowadzenia")]
        [DisplayFormat(DataFormatString = GlobalNames.FormatStrings.DateFormatString, ApplyFormatInEditMode = true)]
        [DateAttributeValidator()]
        public DateTime? DataWprowadzenia { get; set; }

        [Display(Name = "Ulica")]
        public string Ulica { get; set; }

        [Display(Name = "Kod")]
        public string Kod { get; set; }

        [Display(Name = "Miasto")]
        public string Miasto { get; set; }

        [Display(Name = "Kraj")]
        public string Kraj { get; set; }

        [Display(Name = "Terytorium")]
        public string Terytorium { get; set; }

        [Display(Name = "Województwo")]
        public string Wojewodztwo { get; set; }

        [Display(Name = "Inny adres korespondencyjny")]
        public bool InnyAdresKorespondencyjny { get; set; }

        [Display(Name = "Ulica")]
        public string KorespondencjaUlica { get; set; }

        [Display(Name = "Kod")]
        public string KorespondencjaKod { get; set; }

        [Display(Name = "Miasto")]
        public string KorespondencjMiasto { get; set; }

        [Display(Name = "Kraj")]
        public string KorespondencjKraj { get; set; }

        [Display(Name = "Terytorium")]
        public string KorespondencjTerytorium { get; set; }

        [Display(Name = "Województwo")]
        public string KorespondencjWojewodztwo { get; set; }

        #region Dane kontaktowe

        [Display(Name = "Firma")]
        public string TelefonFirma { get; set; }

        [Display(Name = "Komórka")]
        public string TelefonKomorka { get; set; }

        [Display(Name = "Praca")]
        public string TelefonPraca { get; set; }

        [Display(Name = "Asystent")]
        public string TelefonAsystent { get; set; }

        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Skype")]
        public string Skype { get; set; }

        [Display(Name = "WWW")]
        public string WWWUrl { get; set; }

        #endregion

        [Display(Name = "Typ kontaktu")]
        public string TypKontaktu { get; set; }

        [Display(Name = "NIP")]
        public string NIP { get; set; }

        [Display(Name = "KRS")]
        public string KRS { get; set; }

        [Display(Name = "NIP UE")]
        public string NIPUE { get; set; }

        [Display(Name = "Regon")]
        public string Regon { get; set; }

        [Display(Name = "Numer w PK")]
        public string NumerWPK { get; set; }

        [Display(Name = "Dodatkowy ident.")]
        public string DodatkowyIdentyfikator { get; set; }

        [Display(Name = "EKD")]
        public string EKD { get; set; }

        [Display(Name = "Godziny pracy")]
        public string GodzinyPracy { get; set; }

        [Display(Name = "Data rozpoczęcia działalności")]
        [DisplayFormat(DataFormatString = GlobalNames.FormatStrings.DateFormatString, ApplyFormatInEditMode = true)]
        [DateAttributeValidator()]
        public Nullable<DateTime> DataRozpoczeciaDzialalnosci { get; set; }

        [Display(Name = "Wpis do rejestru")]
        public string WpisDoRejestru { get; set; }

        [Display(Name = "Rodzaj działalności")]
        public string RodzajDzialalnosci { get; set; }

        [Display(Name = "Zarząd")]
        public string Zarzad { get; set; }

        [Display(Name = "Uwagi")]
        public string Uwagi { get; set; }

        [Display(Name = "Źródło danych")]
        public string ZrodloDanych { get; set; }

        [Display(Name = "Forma rozliczenia")]
        public string FormaRozliczenia { get; set; }

        [Display(Name = "Opis na fakturę")]
        public string OpisNaFakture { get; set; }

        [Display(Name = "Domyślne konto kancelarii")]
        public string DomyslneKontoKancelarii { get; set; }

        [Display(Name = "Waluta faktury")]
        public DictionaryItem WalutaFaktury { get; set; }

        [Display(Name = "Domyślna stawka VAT")]
        public DictionaryItem DomyslnaStawkaVat { get; set; }

        [Display(Name = "Oświadczenie o wystawianiu faktury bez podpisu")]
        public bool OswiadczenieOWystawieniuBezPodpisu { get; set; }

        [Display(Name = "Ryczałt administracyjny")]
        public double RyczaltAdministracyjny { get; set; }

        [Display(Name = "Domyślny rabat")]
        public double DomyslnyRabat { get; set; }

        [Display(Name = "Pokazuj zestawienie godzinowe dla ryczałtów")]
        public bool PokazujZestawienieGodzinoweDlaRyczaltow { get; set; }

        [Display(Name = "Forma płatności")]
        public string FormaPlatnosci { get; set; }

        [Display(Name = "Domyślny szablon faktury")]
        public string DomyslnySzablonFaktury { get; set; }

        [Display(Name = "Domyślne opisy pozycji na fakturze - polskie")]
        public bool DomyslneOpisyPozycjiNaFakturzePolskie { get; set; }

        [Display(Name = "PKWiU")]
        public string PKWiU { get; set; }

        [Display(Name = "Premia za sukces")]
        public double PremiaZaSukces { get; set; }

        [Display(Name = "Od kwoty")]
        public double OdKwoty { get; set; }

        [Display(Name = "Limit godzin niezafakturowanych do alarmu")]
        public double LimitGodzinNiezafakturowanychDoAlarmu { get; set; }

        [Display(Name = "Budżet - limit kosztów niezafakturowanych do alarmu")]
        public string BudzetLimitKosztowNiezafakturowanychDoAlarmu { get; set; }

        [Display(Name = "Waluta stawek godzinowych")]
        public DictionaryItem WalutaStawekGodzinowych { get; set; }

        [Display(Name = "Notatki")]
        public string Notatki { get; set; }        

        public IEnumerable<Sprawa> Sprawy { get; set; }
    }
}