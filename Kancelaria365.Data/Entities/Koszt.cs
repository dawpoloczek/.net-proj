﻿using Kancelaria365.Data.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class Koszt
              : BaseEntity<Koszt>, ICustomEntity
    {
        public Koszt()
             : base()
        {
            Sprawa = new DictionaryItem<Sprawa>();
            Firma = new DictionaryItem<Kontakt>();
            Kategoria = new DictionaryItem();
            Waluta = new DictionaryItem();
            StawkaVat = new DictionaryItem();
            Data = DateTime.Now;
            KwotaNetto = new decimal(0.0);
            KwotaBrutto = new decimal(0.0);
        }

        #region Pola
        public override int Id { get; set; }

        public override string Value
        {
            get
            {
                return Title;
            }
        }

        [Display(Name = "Sprawa Id")]
        public string SprawaId { get; set; }

        [Display(Name = "Kontakt Id")]
        public string KontaktId { get; set; }

        [Display(Name = "Nazwa kosztu")]
        [CustomRequired(AllowEmptyStrings = false)]
        public string Title { get; set; }

        [Display(Name = "Data")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}")]
        public DateTime Data { get; set; }

        [Display(Name = "Kurs")]
        public decimal Kurs { get; set; }

        [Display(Name = "Obciążyć klienta")]
        public bool ObciazycKlienta { get; set; }

        [Display(Name = "Publikuj w PKWeb")]
        public bool PublikujWPKWeb { get; set; }

        [Display(Name = "Opis")]
        public string Opis { get; set; }

        [Display(Name = "Kwota netto")]
        public decimal KwotaNetto { get; set; }

        [Display(Name = "Vat")]
        public DictionaryItem StawkaVat { get; set; }

        [Display(Name = "Kwota brutto")]
        public decimal KwotaBrutto { get; set; }

        [Display(Name = "Uwagi")]
        public string Uwagi { get; set; }

        [Display(Name = "Numer faktury")]
        [Editable(false)]
        public string NumerFaktury { get; set; }

        [Display(Name = "PKWiU")]
        public string PKWiU { get; set; }

        [Display(Name = "Sprawa")]
        [DictItemRequiredAttributeValidator()]
        public DictionaryItem<Sprawa> Sprawa { get; set; }

        [Display(Name = "Firma")]
        [DictItemRequiredAttributeValidator()]
        public DictionaryItem Firma { get; set; }

        [Display(Name = "Waluta")]
        public DictionaryItem Waluta { get; set; }

        [Display(Name = "Kategoria")]
        public DictionaryItem Kategoria { get; set; }

        [Display(Name = "Wprowadzający")]
        [Editable(false)]
        public DictionaryItem Wprowadzajacy { get; set; }
        #endregion
    }
}