﻿using Kancelaria365.Data.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class Czynnosc
         : BaseEntity<Czynnosc>, ICustomEntity
    {
        public Czynnosc()
             : base()
        {
            Kategoria = new DictionaryItem();

            Sprawa = new DictionaryItem<Sprawa>();
            Firma = new DictionaryItem<Kontakt>();
            OsobaFakturowana = new DictionaryItem();

            Koszty = new List<Koszt>();
        }

        public Czynnosc(int id):
            base()
        {
            this.Id = id;
        }

        #region Pola
        public override int Id { get; set; }

        public override string Value
        {
            get
            {
                return Title;
            }
        }

        [CustomRequired(AllowEmptyStrings = false)]
        [Display(Name = "Nazwa czynności")]
        public string Title { get; set; }

        [Display(Name = "Data wykonania")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}")]
        [EndDateAttributeValidator("DataWprowadzenia", "Data wprowadzenia")]
        public Nullable<DateTime> DataWykonania { get; set; }

        [Display(Name = "Data wprowadzenia")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}")]
        public DateTime DataWprowadzenia { get; set; }

        [Display(Name = "Czas zarejestrowany")]
        public double CzasZarejestrowany { get; set; }

        [Display(Name = "Zatiwerdzona")]
        public bool Zatwierdzona { get; set; }

        [Display(Name = "Obciążyć klienta")]
        public bool ObciazycKlienta { get; set; }

        [Display(Name = "Publikuj w PKWeb")]
        public bool PublikujWPKWeb { get; set; }

        [Display(Name = "Opis wykonania")]
        public string OpisWykonania { get; set; }

        [Display(Name = "Uwagi")]
        public string Uwagi { get; set; }

        [Display(Name = "Numer faktury")]
        [Editable(false)]
        public string NumerFaktury { get; set; }

        [Display(Name = "Numer korekty")]
        [Editable(false)]
        public string NumerKorekty { get; set; }

        [Display(Name = "Czas fakturowany")]
        public int CzasFakturowany { get; set; }

        [Display(Name = "Zafakturowana")]
        public bool Zafakturowana { get; set; }

        [Display(Name = "Czas w godzinach")]
        public string CzasWGodzinach { get; set; }

        [Display(Name = "Sprawa")]
        [DictItemRequiredAttributeValidator()]
        public DictionaryItem<Sprawa> Sprawa { get; set; }

        [Display(Name = "Firma")]
        [DictItemRequiredAttributeValidator()]
        public DictionaryItem Firma { get; set; }

        [Display(Name = "Kategoria")]
        public DictionaryItem Kategoria { get; set; }

        [Display(Name = "Wprowadzający")]
        [Editable(false)]
        public UserDictionaryItem Wprowadzajacy { get; set; }

        [Display(Name = "Osoba fakturowana")]
        public DictionaryItem OsobaFakturowana { get; set; }

        [Display(Name = "Wykonawca")]
        public UserDictionaryItem Wykonawca { get; set; }

        [Display(Name = "Koszty")]
        public List<Koszt> Koszty { get; set; }

        #endregion
    }
}