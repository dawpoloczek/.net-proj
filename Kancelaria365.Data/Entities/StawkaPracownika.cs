﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class StawkaPracownika
            : BaseEntity<StawkaPracownika>, ICustomEntity
    {
        public override int Id { get; set; }

        public override string Value
        {
            get
            {
                return Title;
            }
        }


        [Display(Name = "KontaktId")]
        public string KontaktId { get; set; }

        [Display(Name = "Stawka pracownika")]
        public string Title { get; set; }

        [Display(Name = "Kategoria")]
        public DictionaryItem KategoriaPracownika { get; set; }

        [Display(Name = "Kontakt")]
        public DictionaryItem Kontakt { get; set; }

        [Display(Name = "Data")]
        public DateTime? StawkiData { get; set; }

        [Display(Name = "Stawka")]
        public double Stawka { get; set; }

        [Display(Name = "Opis")]
        public string Opis { get; set; }
    }
}