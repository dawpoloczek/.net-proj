﻿
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kancelaria365.Data
{
    public class Kalendarz
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string IsDefaultCalendar { get; set; }

        public Kalendarz(Calendar x)
        {
            Name = x.Name;
            Id = x.Id.ToString();
        }
    }
}
