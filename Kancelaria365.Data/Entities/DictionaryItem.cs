﻿using Kancelaria365.Data;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SP = Microsoft.SharePoint.Client;


namespace Kancelaria365.Data
{
    public class DictionaryItem
        : ICustomEntity, IComparable<DictionaryItem>
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        public int Id { get; set; }

        public string Value { get; set; }

        public DictionaryItem()
        {

        }
        public DictionaryItem(int id)
        {
            this.Id = id;
        }
        public DictionaryItem(int id, string value)
        {
            this.Value = value;
            this.Id = id;
        }

        public string ToUpper()
        {
            if (!string.IsNullOrEmpty(Value))
            {
                return Value.ToString().ToUpper();
            }
            else
            {
                return string.Empty;
            }
        }

        public override string ToString()
        {
            return this.Value;
        }

        public int CompareTo(DictionaryItem other)
        {
            if (this.Value == null && other.Value != null)
                return -1;
            else if (this.Value == null && other.Value == null)
                return 0;
            else
                return Value.CompareTo(other.Value);
        }
    }

    public class StringDictionaryItem
    {
        public string Id { get; set; }

        public string Value { get; set; }

        public StringDictionaryItem()
        {

        }
        public StringDictionaryItem(string id, string value)
        {
            this.Value = value;
            this.Id = id;
        }

        public override string ToString()
        {
            return this.Value;
        }
    }
    public class DictionaryItem<T> : DictionaryItem
        where T : ICustomEntity
    {
        public DictionaryItem()
        {

        }

        public DictionaryItem(int id)
            : base(id)
        {
        }

        public DictionaryItem(int id, string value)
            : base(id, value)
        {
        }

        public T GetEntity(SP.ClientContext context)
        {
            IRepositoryBase<T> repository = RepositoryFactory.Create<T>(context);
            return repository.GetEntityById(Id);
        }
    }

    public class UserDictionaryItem
        : DictionaryItem
    {
        public UserDictionaryItem() { }

        public UserDictionaryItem(DictionaryItem dicItem)
        {
            if (dicItem != null)
            {
                this.Value = dicItem.Value;
                this.Id = dicItem.Id;
            }
        }

        public string AdditionalValue { get; set; }
        public UserDictionaryItem(int id, string value, string additionalValue)
        {
            this.Value = value;
            this.Id = id;
            this.AdditionalValue = additionalValue;
        }
        public UserDictionaryItem(SP.User user)
        {
            this.Value = user.Title;
            this.Id = user.Id;
            this.AdditionalValue = user.LoginName;
        }
    }
}