﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SP = Microsoft.SharePoint.Client;
using Kancelaria365.Data.CustomAttributes;

namespace Kancelaria365.Data
{
    public class Sprawa
          : BaseEntity<Sprawa>, ICustomEntity
    {
        public Sprawa()
        {
            Kontakt = new DictionaryItem();
            Kategoria = new DictionaryItem();

            Prowadzacy = new UserDictionaryItem();
            Zlecajacy = new UserDictionaryItem();
            Wprowadzajacy = new UserDictionaryItem();
            FormaRozliczenia = new DictionaryItem();

            Czynnosci = new List<Czynnosc>();
        }

        #region ICustomEntity
        public override int Id { get; set; }

        public override string Value
        {
            get
            {
                return Title;
            }
        }

        [Display(Name = "Kontakt Id")]
        public string KontaktId { get; set; }

        [Display(Name = "Plan Id")]
        public string PlanId { get; set; }

        #region Zakładka Dane

        #region Nazwa

        [Display(Name = "Firma")]
        [DictItemRequiredAttributeValidator()]
        public DictionaryItem Kontakt { get; set; }

        [Display(Name = "Sprawa")]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Kategoria")]
        [DictItemRequiredAttributeValidator()]
        public DictionaryItem Kategoria { get; set; }

        public string KategoriaText
        {
            get
            {
                return Kategoria.Value;
            }
        }

        [Display(Name = "Etap")]
        public string Etap { get; set; }

        [Display(Name = "Stan")]
        public string Stan { get; set; }

        #endregion

        #region Identyfikacja

        [Display(Name = "Nr porządkowy")]
        public string NrPorzadkowy { get; set; }

        [Display(Name = "Nr wewnętrzny")]
        public string NrWewnetrzny { get; set; }

        [Display(Name = "Sygnatura")]
        public string Sygnatura { get; set; }

        [Display(Name = "Wartość uniwer.")]
        public string WartUniwer { get; set; }

        [Display(Name = "Data wprowadzenia")]
        [DateAttributeValidator()]
        public DateTime DataWprowadzenia { get; set; }

        [Display(Name = "Data przyjęcia")]
        [DateAttributeValidator()]
        public DateTime DataPrzyjecia { get; set; }

        [Display(Name = "Data zakończenia")]
        [EndDateAttributeValidator("DataPrzyjecia", "Data przyjęcia")]
        public Nullable<DateTime> DataZakończenia { get; set; }

        [Display(Name = "Prowadzący")]
        [UserAttributeValidator]
        public UserDictionaryItem Prowadzacy { get; set; }

        [Display(Name = "Zlecający")]
        [UserAttributeValidator]
        public UserDictionaryItem Zlecajacy { get; set; }

        [Display(Name = "Wprowadzający")]
        [UserAttributeValidator]
        public UserDictionaryItem Wprowadzajacy { get; set; }

        #endregion

        #region Uwagi

        [Display(Name = "Uwagi")]
        public string Uwagi { get; set; }

        #endregion

        #endregion

        #region Zakładka Finansowe

        [Display(Name = "Forma rozliczenia")]
        public DictionaryItem FormaRozliczenia { get; set; }
        public string FormaRozliczeniaText
        {
            get
            {
                return FormaRozliczenia.Value;
            }
        }

        [Display(Name = "Opis na fakturę")]
        public string OpisNaKature { get; set; }

        [Display(Name = "PKWiU")]
        public string PKWiU { get; set; }

        [Display(Name = "Premia za skuces")]
        public decimal BonusProcent { get; set; }

        [Display(Name = "od kwoty")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal BonusKwota { get; set; }

        [Display(Name = "Limit godzin niezafakturowanych do alarmu")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal LimitGodzinDoAlarmu { get; set; }

        [Display(Name = "Budżet - limit kwoty kosztów niezafakturowanych do alarmu")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal LimitKosztowNiezafaktDoAlarmu { get; set; }

        [Display(Name = "Rozliczać na bieżąco")]
        public bool CzyRozliczacNaBiezaco { get; set; }

        [Display(Name = "Zakończona")]
        public bool CzyZakonczona { get; set; }

        [Display(Name = "Rozliczona ostatecznie")]
        public bool CzyRozliczonaOstatecznie { get; set; }

        [Display(Name = "Waluta stawek godzinowych")]
        public int WalutaStawekGodz { get; set; }

        [Display(Name = "Waluta ryczałtu")]
        public string WalutaRyczaltu { get; set; }

        [Display(Name = "Notatki")]
        public string Notatki { get; set; }

        [Display(Name = "Stawka ryczałtu")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal StawkaRyczaltu { get; set; }

        [Display(Name = "Limit godzin w ramach ryczałtu")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal LimitGodznWRamachRyczaltu { get; set; }

        [Display(Name = "Okres roliczenia nadgodzin")]
        public string OkresRozliczeniaNadg { get; set; }

        [Display(Name = "Data kolejnego rozliczenia nadgodzin")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}")]
        public DateTime DataKolejnegoRozliczeniaNadg { get; set; }

        #endregion

        public IEnumerable<Czynnosc> Czynnosci { get; set; }

        #endregion
    }
}