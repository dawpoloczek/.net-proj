﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class GrupaSpraw
    {
        public int IdKontaktu { get; set; }

        public string NazwaKontaktu { get; set; }

        public  List<Sprawa> Sprawy { get; set; }
    }
}