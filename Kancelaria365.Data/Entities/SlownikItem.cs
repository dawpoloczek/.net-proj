﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class SlownikItem
    {
        public string Nazwa { get; set; }
        public string ServerRelativeUrl { get; set; }
        public string ServerUrl { get; set; }
        public string Url
        {
            get
            {
                return string.Format("{0}{1}", ServerUrl, ServerRelativeUrl);
            }
        }
    }
}