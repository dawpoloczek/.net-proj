﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class CostomGridFieldMetadata
    {
        public string Title;

        public string Name;

        public bool IsEditable;

        public bool IsSortable = true;

        public bool IsDate = false;

        public List<DictionaryItem> ItemEditDropdownValues = new List<DictionaryItem>();

        public string FieldType;

        public string HeaderCssClass = "cust-grid-header";

        public string CssClass = "cust-grid-cell";

        public bool ShowFieldValueProperty;
    }
}