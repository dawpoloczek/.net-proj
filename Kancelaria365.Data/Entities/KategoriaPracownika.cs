﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class KategoriaPracownika
            : BaseEntity<KategoriaPracownika>, ICustomEntity
    {
        public override int Id { get; set; }

        public override string Value
        {
            get
            {
                return Title;
            }
        }

        [Display(Name = "Kategoria pracownika")]
        public string Title { get; set; }
        
        [Display(Name = "Priorytet")]
        public double Priorytet { get; set; }

        [Display(Name = "Stawka")]
        public double Stawka { get; set; }

        [Display(Name = "Opis")]
        public string Opis { get; set; }
    }
}