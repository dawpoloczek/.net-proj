﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class DokumentFinansowy
            : BaseEntity<DokumentFinansowy>, ICustomEntity
    {
        public override int Id { get; set; }

        public override string Value
        {
            get
            {
                return Title;
            }
        }

        public DokumentFinansowy()
        {
            this.Kontakt = new DictionaryItem();
        }

        [Display(Name = "Kontakt Id")]
        public string KontaktId { get; set; }

        [Display(Name = "Nazwa")]
        public string LinkFilenameNoMenu { get; set; }        

        [Display(Name = "Nazwa")]
        public string Title { get; set; }

        [Display(Name = "FileRef")]
        public string FileRef { get; set; }

        [Display(Name = "SiteUrl")]
        public string SiteUrl { get; set; }

        [Display(Name = "DocumentURL")]
        public string DocumentURL
        {
            get
            {
                return string.Format("{0}{1}", SiteUrl, FileRef);
            }
        }

        [Display(Name = "Kontakt")]
        public DictionaryItem Kontakt { get; set; }


    }
}