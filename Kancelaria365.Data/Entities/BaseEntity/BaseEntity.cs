﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public class BaseEntity<T>
        where T : ICustomEntity
    {
        public static CultureInfo plCulture = new CultureInfo("pl-PL");

        public static int PageSize = 15;

        public virtual int Id { get; set; }

        public virtual string Value { get; set; }

        public DictionaryItem<T> GetDictionaryItem()
        {
            return new DictionaryItem<T>(Id, Value);
        }
    }
}