﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public interface ICustomEntity
    {
        int Id { get; set; }

        string Value { get; set; }
    }
}