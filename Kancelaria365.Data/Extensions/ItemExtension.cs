﻿using Kancelaria365.Data;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SP = Microsoft.SharePoint.Client;

namespace Kancelaria365.Data
{
    public static class ItemExtension
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static T GetValue<T>(this SP.ListItem item, string columnName)
        {
            T value = default(T);
            try
            {
                if (item[columnName] != null)
                {
                    try
                    {
                        value = (T)Convert.ChangeType(item[columnName], typeof(T));
                    }
                    catch(InvalidCastException ex)
                    {
                        value = (T)item[columnName];
                    }      
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return value;
        }

        public static DateTime GetDateTimeValue(this SP.ListItem item, string columnName)
        {
            DateTime value = default(DateTime);
            try
            {
                if (item[columnName] != null)
                {
                    value = (DateTime)Convert.ChangeType(item[columnName], typeof(DateTime));
                    value = value.ToLocalTime();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return value;
        }

        public static DictionaryItem<T> GetTypedDictionaryItemValue<T, Z>(this SP.ListItem item, string columnName)
            where Z : SP.FieldLookupValue
            where T : ICustomEntity
        {
            DictionaryItem<T> returnValue = default(DictionaryItem<T>);
            try
            {
                if (item[columnName] != null)
                {
                    Z lookupValue = (Z)Convert.ChangeType(item[columnName], typeof(Z));
                    returnValue = new DictionaryItem<T>(lookupValue.LookupId, lookupValue.LookupValue);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return returnValue;
        }

        public static DictionaryItem GetDictionaryItemValue<Z>(this SP.ListItem item, string columnName)
            where Z : SP.FieldLookupValue
        {
            DictionaryItem returnValue = new DictionaryItem();
            try
            {
                if (item[columnName] != null)
                {
                    Z lookupValue = (Z)Convert.ChangeType(item[columnName], typeof(Z));
                    returnValue = new DictionaryItem(lookupValue.LookupId, lookupValue.LookupValue);
                }
            }
            catch (Exception ex)
            {
            }
            return returnValue;
        }

        public static DictionaryItem GetChoiceListValue(this SP.ListItem item, string columnName)
        {
            DictionaryItem returnValue = default(DictionaryItem);
            try
            {
                if (item[columnName] != null)
                {
                    string value = (string)Convert.ChangeType(item[columnName], typeof(string));
                    returnValue = new DictionaryItem(0, value);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return returnValue;
        }

        public static void SetValue<Z>(this SP.ListItem item, string columnName, Z value)
        {
            Z defaultValue = default(Z);
            try
            {
                if (value != null && !EqualityComparer<Z>.Default.Equals(value, default(Z)))
                {
                    item[columnName] = value;
                }
                else
                {
                    item[columnName] = defaultValue;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
        }

        public static void SetDictionaryItemValue<Z>(this SP.ListItem item, string columnName, DictionaryItem value)
                         where Z : SP.FieldLookupValue, new()
        {
            Z defaultValue = default(Z);
            try
            {
                if (value != null && value.Id > 0)
                {
                    Z newValue = new Z();
                    newValue.LookupId = value.Id;
                    item[columnName] = newValue;
                }
                else
                {
                    item[columnName] = defaultValue;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
        }
    }
}
