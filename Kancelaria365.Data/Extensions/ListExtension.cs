﻿using Microsoft.SharePoint.Client;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Data
{
    public static class ListExtension
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void SetProperty(this List list, string key, string value)
        {
            try
            {
                var allProperties = list.RootFolder.Properties;
                allProperties[key] = value;

                list.Update();
                list.Context.ExecuteQuery();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
        }
        public static string GetProperty(this List list, string key)
        {
            string returnValue = string.Empty;
            try
            {
                var allProperties = list.RootFolder.Properties;
                list.Context.Load(allProperties);
                list.Context.ExecuteQuery();

                if (list.RootFolder.Properties[key] != null)
                {
                    returnValue = list.RootFolder.Properties[key].ToString();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return returnValue;
        }
    }
}
