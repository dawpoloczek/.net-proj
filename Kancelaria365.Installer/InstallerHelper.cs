﻿using Kancelaria365.Common;
using Kancelaria365.Data;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Kancelaria365.Installer
{
    public static class InstallerHelper
    {
        public static void FillCredentials(ClientContext clientContext)
        {
            SecureString passWord = new SecureString();
            foreach (char c in "P@ssw0rd$".ToCharArray()) passWord.AppendChar(c);

            clientContext.Credentials = new SharePointOnlineCredentials("amachnik@machsoft2.onmicrosoft.com", passWord);
        }

        public static string CombineUrl(string path1, string path2)
        {

            return path1.TrimEnd('/') + '/' + path2.TrimStart('/');
        }
        public static void ClearWeb(ClientContext clientContext)
        {
            try
            {
                Web web = clientContext.Web;

                IEnumerable<List> result = clientContext.LoadQuery(web.Lists.Include(
                                                                                   list => list.Title,
                                                                                   list => list.Id));
                clientContext.ExecuteQuery();

                foreach (List list in result)
                {
                    try
                    {
                        if (list.Title != GlobalNames.Lists.KancelariaPliki &&
                            list.Title != GlobalNames.Lists.KancelariaStrony
                            )
                        {
                            List oList = web.Lists.GetByTitle(list.Title);
                            oList.DeleteObject();
                            clientContext.ExecuteQuery();
                        }
                    }
                    catch (Exception ex) { }
                }
            }
            catch (Exception ex)
            {
                LogEvent(MethodBase.GetCurrentMethod().DeclaringType.Name + "." +
                    MethodBase.GetCurrentMethod().Name, ex.Message, ex,
                    System.Diagnostics.EventLogEntryType.Error);
            }
        }

        public static void AddSPList(ClientContext clientContext, string title, string url, int templateType)
        {
            try
            {
                ListCreationInformation creationInfo = new ListCreationInformation();
                creationInfo.Title = title;
                creationInfo.Url = url;
                creationInfo.Description = title;
                creationInfo.TemplateType = templateType;

                List newList = clientContext.Web.Lists.Add(creationInfo);
                clientContext.Load(newList);
                clientContext.ExecuteQuery();
                Console.WriteLine(title + " - OK");
            }
            catch (Exception ex)
            {
                LogEvent(MethodBase.GetCurrentMethod().DeclaringType.Name + "." +
                    MethodBase.GetCurrentMethod().Name, "Wystąpił bład w trakcie tworzenia listy:" + title, null,
                    System.Diagnostics.EventLogEntryType.Error);

                //LogEvent(MethodBase.GetCurrentMethod().DeclaringType.Name + "." +
                //    MethodBase.GetCurrentMethod().Name, ex.Message, ex,
                //    System.Diagnostics.EventLogEntryType.Error);
            }
        }

        public static List AddCustomSPList(ClientContext clientContext, string title, string url, int templateType, bool isDictionary = false, bool allowContentTypes = false)
        {
            List newList = null;
            try
            {
                ListCreationInformation creationInfo = new ListCreationInformation();
                creationInfo.Title = title;
                creationInfo.TemplateFeatureId = new Guid("08e20315-575d-4bd8-acf9-fd0f40f47613");
                creationInfo.Url = url;
                creationInfo.Description = title;
                creationInfo.TemplateType = templateType;

                newList = clientContext.Web.Lists.Add(creationInfo);
                clientContext.Load(newList);
                clientContext.ExecuteQuery();
                if (isDictionary)
                {
                    newList.SetProperty("IsDictionary", "1");
                }
                if (allowContentTypes)
                {
                    newList.ContentTypesEnabled = true;
                    newList.Update();
                    newList.Context.ExecuteQuery();
                }
                Console.WriteLine(title + " - OK");
            }
            catch (Exception ex)
            {
                LogEvent(MethodBase.GetCurrentMethod().DeclaringType.Name + "." +
                    MethodBase.GetCurrentMethod().Name, "Wystąpił bład w trakcie tworzenia listy:" + title, null,
                    System.Diagnostics.EventLogEntryType.Error);
                Console.ReadLine();
            }
            return newList;
        }

        public static void DeleteSiteColumns(ClientContext clientContext)
        {
            clientContext.Load(clientContext.Web);
            clientContext.Load(clientContext.Web.Fields);
            clientContext.ExecuteQuery();

            var fields = clientContext.Web.Fields.Where(x => x.Group == "Kancelaria 365");
            clientContext.LoadQuery(fields);
            fields.ToList().ForEach(x => { x.DeleteObject(); });
            clientContext.ExecuteQuery();

            Console.WriteLine("Kolumny witryny zostały usunięte");
        }

        public static void AddSiteColumns(ClientContext clientContext)
        {
            string currentDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            currentDirectory = Path.Combine(currentDirectory, "..\\..\\..\\");
            String[] allfiles =
    //System.IO.Directory.GetFiles(@"D:\projekty\Kancelaria365\Kancelaria365.AppData\Kancelaria365.AppData\Lists", "*.xml",
    System.IO.Directory.GetFiles(currentDirectory + @"\Kancelaria365.AppData\Kancelaria365.AppData\Lists", "*.xml",
    System.IO.SearchOption.AllDirectories);

            allfiles = allfiles.Where(x => x.EndsWith("Schema.xml")).ToArray();

            List<XmlNode> allFilds = new List<XmlNode>();

            foreach (var filePath in allfiles)
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filePath);
                    XmlNodeList filds = doc.GetElementsByTagName("Field");

                    filds.Cast<XmlNode>().ToList().ForEach(x => allFilds.Add(x));
                }
                catch (Exception)
                {
                    Console.WriteLine(filePath);
                }
            }
            List<XmlNode> lookupFilds =
                    allFilds.Where(x =>
                    x.Attributes["Type"] != null &&
                    x.Attributes["Type"].Value != "Lookup" &&
                    x.Attributes["Group"] != null &&
                    x.Attributes["Group"].Value == "Kancelaria 365").ToList();

            clientContext.Load(clientContext.Web);
            clientContext.Load(clientContext.Web.Fields);
            clientContext.ExecuteQuery();

            foreach (var x in lookupFilds.GroupBy(x => x.Attributes["Name"].Value).ToList().Distinct())
            {
                try
                {
                    clientContext.Web.Fields.AddFieldAsXml(x.First().OuterXml, false, AddFieldOptions.DefaultValue);
                    clientContext.ExecuteQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Błąd dla kolumny:" + x.Key + "-" + x.Count());
                }
            }
            Console.WriteLine("Kolumny zostały dodane.");
        }

        public static void AddSiteLookupColumns(ClientContext clientContext)
        {

            string currentDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            currentDirectory = Path.Combine(currentDirectory, "..\\..\\..\\");
            String[] allfiles =
    //System.IO.Directory.GetFiles(@"D:\projekty\Kancelaria365\Kancelaria365.AppData\Kancelaria365.AppData\Listss", "*.xml",
    System.IO.Directory.GetFiles(currentDirectory + @"\Kancelaria365.AppData\Kancelaria365.AppData\Lists", "*.xml",
    System.IO.SearchOption.AllDirectories);

            allfiles = allfiles.Where(x => x.EndsWith("Schema.xml")).ToArray();

            List<XmlNode> allFilds = new List<XmlNode>();

            foreach (var filePath in allfiles)
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filePath);
                    XmlNodeList filds = doc.GetElementsByTagName("Field");

                    filds.Cast<XmlNode>().ToList().ForEach(x => allFilds.Add(x));
                }
                catch (Exception)
                {
                    Console.WriteLine(filePath);
                }
            }

            List<XmlNode> lookupFilds =
                    allFilds.Where(x =>
                     x.Attributes["Group"] != null &&
                     x.Attributes["Group"].Value == "Kancelaria 365" &&
                     x.Attributes["Type"] != null &&
                     x.Attributes["Type"].Value == "Lookup").ToList();

            clientContext.Load(clientContext.Web);
            clientContext.Load(clientContext.Web.Fields);
            clientContext.ExecuteQuery();

            foreach (var x in lookupFilds.GroupBy(x => x.Attributes["Name"].Value).ToList().Distinct())
            {
                try
                {
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();

                    string list = x.First().Attributes["List"].Value;
                    List spList = clientContext.Web.GetList(clientContext.Web.Url + "/" + list);
                    clientContext.Load(spList);
                    clientContext.ExecuteQuery();

                    string newColumn = x.First().OuterXml;

                    newColumn = newColumn.Replace(list, "{" + spList.Id + "}");
                    newColumn = newColumn.Replace("Type=\"Lookup\"", "Type =\"Lookup\" WebId =\"" + clientContext.Web.Id + "\"");
                    clientContext.Web.Fields.AddFieldAsXml(newColumn, false, AddFieldOptions.DefaultValue);
                    clientContext.ExecuteQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Błąd dla kolumny:" + x.Key + "-" + x.Count());
                }
            }
            Console.WriteLine("Kolumny typu 'lookup' zostały dodane.");
        }

        /// <summary>
        /// Logowanie.
        /// </summary>
        public static void LogEvent(string source, string message, Exception ex, EventLogEntryType msgType)
        {
            StringBuilder sb = new StringBuilder(message);
            try
            {
                if (msgType == EventLogEntryType.Error)
                {
                    message = "Error : " + message;
                }
                else if (msgType == EventLogEntryType.Information)
                {
                    message = "Information : " + message;
                }
                else if (msgType == EventLogEntryType.Warning)
                {
                    message = "Warning : " + message;
                }

                if (ex != null)
                    sb.Append(": " + ex.Message + "\r\n\r\n");
                while (ex != null)
                {
                    sb.Append(ex.StackTrace);
                    sb.Append("\r\n----\r\n");
                    ex = ex.InnerException;
                }
            }
            catch { }

            Console.WriteLine("Source:{0}\nMessage:{1}\nException:{2}\nType:{3}", source, message, sb.ToString(), msgType);
        }

    }
}
