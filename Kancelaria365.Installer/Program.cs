﻿using Kancelaria365.Common;
using Kancelaria365.Data;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Kancelaria365.Installer
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                string url = "https://machsoft2.sharepoint.com/sites/kancelaria2/";
                using (ClientContext clientContext = new ClientContext(url))
                {
                    InstallerHelper.FillCredentials(clientContext);
                    Installer();
                }
            }
            catch (Exception ex)
            {
                 InstallerHelper.LogEvent(MethodBase.GetCurrentMethod().DeclaringType.Name + "." +
                    MethodBase.GetCurrentMethod().Name, ex.Message, ex,
                    System.Diagnostics.EventLogEntryType.Error);
            }
        }
        private static void CreateDataSchema(ClientContext clientContext)
        {
            InstallerHelper.DeleteSiteColumns(clientContext);
            InstallerHelper.AddSiteColumns(clientContext);

            InstallerHelper.ClearWeb(clientContext);

            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.KategorieCzynnosciTerminowZadan,
                "kategorie-czynnosci-terminow-zadan", 22018, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.Waluty, "waluty", 22015, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.StawkiVAT, "stawki-vat", 22024, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.KategoriePracownikow, "kategorie-pracownikow", 22000, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.KategorieSpraw, "kategorie-spraw", 22013, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.FormyRozliczenia, "formy-rozliczenia", 22001, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.KategorieKontaktow, "kategorie-kontaktow", 22003, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.NazwyKas, "nazwy-kas", 22004, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.DzialyBibliotek, "dzialy-biblioteki", 22005, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.KategorieUczestnikow, "kategorie-uczestnikow", 22006, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.KategorieKosztow, "kategorie-kosztow", 22007, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.OpisyKosztow, "opisy-kosztow", 22008, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.OpisyKosztowInnych, "opisy-kosztow-innych", 22009, true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.KategorieDokumentow, "kategorie-dokumentow", 22010, true);

            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.Kancelarie, "kancelarie", 22012);

            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.DefinicjeOdsetek, "definicje-odesetek", 22014);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.Kontakty, "kontakty", 22017);

            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.StawkiPracownikow, "stawki-pracownikow", 22021);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.Sprawy, "sprawy", 22002);

            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.Konta, "konta", 22011, allowContentTypes: true);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.Czynnosci, "czynnosci", 22016);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.Koszty, "koszty", 22019);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.Zadania, "zadania", 22020);

            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.KontaktyDokumentyFinansowe, "Kontakt-Dokumenty-Finansowe", 22022);
            InstallerHelper.AddCustomSPList(clientContext, GlobalNames.Lists.OsobyKontaktowe, "osoby-kontaktowe", 22023);

            InstallerHelper.AddSiteLookupColumns(clientContext);
        }
        static void Installer()
        {
            try
            {
                string url = "https://machsoft2.sharepoint.com/sites/kancelaria2/";
                using (ClientContext clientContext = new ClientContext(url))
                {
                    InstallerHelper.FillCredentials(clientContext);
                    //CreateDataSchema(clientContext);


                    AddFakeDictionaries(clientContext);
                    AddFakeData(clientContext);

                    Console.WriteLine("\n");
                    Console.WriteLine("Dane testowe zostały dodane.");
                }
                Console.WriteLine("\n");
                Console.WriteLine("Instalator zakończył swoje działanie.");

            }
            catch (Exception ex)
            {
                 InstallerHelper.LogEvent(MethodBase.GetCurrentMethod().DeclaringType.Name + "." +
                    MethodBase.GetCurrentMethod().Name, ex.Message, ex,
                    System.Diagnostics.EventLogEntryType.Error);
                Console.WriteLine("!!!!!BŁĄD!!!!!");
            }
            Console.ReadLine();
        }

        private static void AddFakeDictionaries(ClientContext clientContext)
        {
            #region Waluty
            List oList = clientContext.Web.Lists.GetByTitle(GlobalNames.Lists.Waluty);

            ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
            ListItem oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "PLN";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "EUR";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "USD";
            oListItem.Update();
            clientContext.ExecuteQuery();
            #endregion

            #region Stawki VAT
            oList = clientContext.Web.Lists.GetByTitle(GlobalNames.Lists.StawkiVAT);

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "0";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "5";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "5";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "8";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "11";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "23";
            oListItem.Update();
            clientContext.ExecuteQuery();
            #endregion

            #region Kategorie czynności terminów zadań"
            oList = clientContext.Web.Lists.GetByTitle(GlobalNames.Lists.KategorieCzynnosciTerminowZadan);

            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "analiza umowy";
            oListItem["Domyslna"] = false;
            //oListItem["Poczta"] = false;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "konsultacje telefoniczne";
            oListItem["Domyslna"] = false;
            //oListItem["Poczta"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "przedmiot reklamacji";
            oListItem["Domyslna"] = false;
            //oListItem["Poczta"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "przygotowanie umowy";
            oListItem["Domyslna"] = true;
            //oListItem["Poczta"] = false;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "spotkanie z klientem";
            oListItem["Domyslna"] = false;
            //oListItem["Poczta"] = false;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();
            #endregion

            #region Kategorie pracowników
            oList = clientContext.Web.Lists.GetByTitle(GlobalNames.Lists.KategoriePracownikow);

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "Prtner";
            oListItem["Priorytet"] = "1";
            oListItem["Stawka"] = "200";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "Pranik";
            oListItem["Priorytet"] = "2";
            oListItem["Stawka"] = "150";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "Stażysta";
            oListItem["Priorytet"] = "3";
            oListItem["Stawka"] = "10";
            oListItem.Update();
            clientContext.ExecuteQuery();
            #endregion

            #region Kategorie kontaktów
            oList = clientContext.Web.Lists.GetByTitle(GlobalNames.Lists.KategorieKontaktow);

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "BS";
            oListItem["Sad"] = true;
            oListItem["Klient"] = false;
            oListItem["Aktywna"] = false;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "dostawca";
            oListItem["Sad"] = true;
            oListItem["Klient"] = false;
            oListItem["Aktywna"] = false;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "inne";
            oListItem["Sad"] = true;
            oListItem["Klient"] = false;
            oListItem["Aktywna"] = false;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "klient";
            oListItem["Sad"] = true;
            oListItem["Klient"] = false;
            oListItem["Aktywna"] = false;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "kooperant";
            oListItem["Sad"] = true;
            oListItem["Klient"] = false;
            oListItem["Aktywna"] = false;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "potencjalny klient";
            oListItem["Sad"] = true;
            oListItem["Klient"] = false;
            oListItem["Aktywna"] = false;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "sąd";
            oListItem["Sad"] = true;
            oListItem["Klient"] = false;
            oListItem["Aktywna"] = false;
            oListItem.Update();
            clientContext.ExecuteQuery();
            #endregion

            #region Kategorie kosztów
            oList = clientContext.Web.Lists.GetByTitle(GlobalNames.Lists.KategorieKosztow);

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "delegacja";
            oListItem["Domyslna"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "duplikat korespondencji";
            oListItem["Domyslna"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "kilometrówka";
            oListItem["Domyslna"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "kilometrówka samochód służbowy";
            oListItem["Domyslna"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "koszt zastępstwa";
            oListItem["Domyslna"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "koszt zwykły";
            oListItem["Domyslna"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "opłata reklamacyjna";
            oListItem["Domyslna"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "znaczki pocztowe";
            oListItem["Domyslna"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "znaki sądowe";
            oListItem["Domyslna"] = true;
            oListItem["Aktywna"] = true;
            oListItem.Update();
            clientContext.ExecuteQuery();
            #endregion

            #region Kategorie spraw
            oList = clientContext.Web.Lists.GetByTitle(GlobalNames.Lists.KategorieSpraw);

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "komisja przetargowa";
            oListItem["Typ"] = "zwykła";
            oListItem.Update();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "cywilna";
            oListItem["Typ"] = "sądowa z windyjacją";
            oListItem.Update();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "karna";
            oListItem["Typ"] = "sądowa z windyjacją";
            oListItem.Update();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "komitet kredytowy";
            oListItem["Typ"] = "zwykła";
            oListItem.Update();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "opinia";
            oListItem["Typ"] = "zwykła";
            oListItem.Update();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "postępowanie upadłościowe";
            oListItem["Typ"] = "zwykła";
            oListItem.Update();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "sądowa bank";
            oListItem["Typ"] = "zwykła";
            oListItem.Update();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "sądowa klient";
            oListItem["Typ"] = "zwykła";
            oListItem.Update();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "uchwała";
            oListItem["Typ"] = "zwykła";
            oListItem.Update();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "kontrola";
            oListItem["Typ"] = "zwykła";
            oListItem.Update();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            oListItem["Title"] = "reklamacja";
            oListItem["Typ"] = "zwykła";
            oListItem.Update();
            #endregion

            #region Formy rozliczenia
            oList = clientContext.Web.Lists.GetByTitle(GlobalNames.Lists.FormyRozliczenia);

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "Godzinowo";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "Godzinowo z limitem godzin";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "Godzinowo z limitem kwoty";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "Ryczałtowo z limitem godzin";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "Ryczałtowo";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "Nierozliczana";
            oListItem.Update();
            clientContext.ExecuteQuery();

            itemCreateInfo = new ListItemCreationInformation();
            oListItem = oList.AddItem(itemCreateInfo);
            clientContext.ExecuteQuery();
            oListItem["Title"] = "z firm";
            oListItem.Update();
            clientContext.ExecuteQuery();
            #endregion
        }

        private static void AddFakeData(ClientContext clientContext)
        {
            try
            {

                #region Kontakty
                AddFakeKontakty(clientContext);
                #endregion

                clientContext.ExecuteQuery();
            }
            catch (Exception ex)
            {
                 InstallerHelper.LogEvent(MethodBase.GetCurrentMethod().DeclaringType.Name + "." +
                    MethodBase.GetCurrentMethod().Name, ex.Message, ex,
                    System.Diagnostics.EventLogEntryType.Error);
            }
        }

        private static void AddFakeKoszty(ClientContext clientContext, Sprawa sprawa)
        {
            SlownikRepository slownikRepository =
                    new SlownikRepository(clientContext);

            Random rnd = new Random();
            int count = rnd.Next(1, 5);

            KosztRepository kosztRepository =
                    new KosztRepository(clientContext);

            for (int i = 0; i < count; i++)
            {
                Koszt entity = new Koszt();
                entity.Title = "Rakieta " + i;
                entity.Data = DateTime.Now;
                entity.NumerFaktury = "fn/232/2015/" + i;
                entity.PublikujWPKWeb = true;
                entity.Kategoria.Id = 1;
                entity.Wprowadzajacy = new DictionaryItem(9);
                entity.Sprawa = sprawa.GetDictionaryItem();
                entity.Firma = sprawa.Kontakt;
                kosztRepository.AddUpdateItem(entity);
            }
        }

        private static void AddFakeCzynnosci(ClientContext clientContext, Sprawa sprawa)
        {
            SlownikRepository slownikRepository =
                    new SlownikRepository(clientContext);

            CzynnoscRepository czynnoscRepository =
                    new CzynnoscRepository(clientContext);

            Random rnd = new Random();
            int count = rnd.Next(1, 5);

            for (int i = 0; i < count; i++)
            {
                Czynnosc entity = new Czynnosc();
                entity.Title = "Wizyta w więzieniu";
                entity.CzasFakturowany = 14;
                entity.CzasZarejestrowany = 13;
                entity.DataWprowadzenia = DateTime.Now;
                entity.NumerFaktury = "fn/232/2015";
                entity.NumerKorekty = "ZE4/43/43/" + i;
                entity.PublikujWPKWeb = true;
                entity.DataWykonania = DateTime.Now;
                entity.Kategoria.Id = 1;
                entity.Wprowadzajacy = new UserDictionaryItem(new DictionaryItem(9));
                entity.Sprawa = sprawa.GetDictionaryItem();
                entity.Firma = sprawa.Kontakt;

                czynnoscRepository.AddUpdateItem(entity);
            }
        }

        private static void AddFakeZadania(ClientContext clientContext)
        {
            SlownikRepository slownikRepository =
                    new SlownikRepository(clientContext);

            SprawaRepository sprawaRepository =
                    new SprawaRepository(clientContext);

            CzynnoscRepository czynnoscRepository =
                    new CzynnoscRepository(clientContext);

            KontaktRepository kontaktReposiotry =
                    new KontaktRepository(clientContext);

        }

        private static void AddFakeKontakty(ClientContext clientContext)
        {
            KontaktRepository kontaktRepository =
                     new KontaktRepository(clientContext);

            List<string> NazwaPelnaList = Datainitializer.NazwaPelna.Split(new[] { "; " }, StringSplitOptions.None).ToList();
            List<string> NazwaList = Datainitializer.Nazwa.Split(new[] { "; " }, StringSplitOptions.None).ToList();
            List<string> MiastaList = Datainitializer.Miasta.Split(new[] { "; " }, StringSplitOptions.None).ToList();
            List<string> KodList = Datainitializer.Kod.Split(new[] { "; " }, StringSplitOptions.None).ToList();
            List<string> KrajList = Datainitializer.Kraj.Split(new[] { "; " }, StringSplitOptions.None).ToList();
            List<string> UlicaList = Datainitializer.Ulica.Split(new[] { "; " }, StringSplitOptions.None).ToList();


            for (int i = 0; i < 10; i++)
            {
                Kontakt entity = new Kontakt();

                entity.DataWprowadzenia = DateTime.Now;
                entity.DataRozpoczeciaDzialalnosci = DateTime.Now;
                NipGenerator nipGenerator = new NipGenerator();

                entity.Title = NazwaList[i];
                entity.Kod = KodList[i];
                entity.Miasto = MiastaList[i];


                entity.Status = "Aktywny";
                entity.DataRozpoczeciaDzialalnosci = DateTime.Now;
                entity.Email = NazwaList[i] + "@firma.pl";
             
                entity.NazwaPelna = NazwaPelnaList[i];
                entity.Ulica = UlicaList[i];
                entity.Kraj = KrajList[i];
                entity.NIP = nipGenerator.Generate();
                entity.Wojewodztwo = "Śląskie";

                kontaktRepository.AddUpdateItem(entity);

                AddFakeSprawa(clientContext, entity);
            }
        }

        private static void AddFakeSprawa(ClientContext clientContext, Kontakt kontakt)
        {
            SlownikRepository slownikRepository =
                    new SlownikRepository(clientContext);

            SprawaRepository sprawaRepository =
                    new SprawaRepository(clientContext);

            Random rnd = new Random();
            int count = rnd.Next(1, 4);

            for (int i = 0; i < count; i++)
            {
                Sprawa entity = new Sprawa();
                entity.Title = kontakt.Title + " - Sprawa rozwodowa " + i;
                entity.FormaRozliczenia = new DictionaryItem(1);
                //entity.Opis = "dsds";
                entity.PKWiU = "dsds";
                entity.BonusProcent = 4324;
                entity.BonusKwota = 24324;
                entity.LimitKosztowNiezafaktDoAlarmu = 5435;
                entity.LimitGodzinDoAlarmu = 43;
                entity.CzyRozliczacNaBiezaco = true;
                entity.CzyZakonczona = true;
                entity.CzyRozliczonaOstatecznie = true;
                entity.StawkaRyczaltu = 32;
                entity.LimitGodznWRamachRyczaltu = 3232;
                entity.OkresRozliczeniaNadg = "sasa";
                entity.DataKolejnegoRozliczeniaNadg = DateTime.Now;
                entity.Notatki = "Jes super jest super";
                entity.Kategoria = new DictionaryItem(1);
                entity.DataWprowadzenia = DateTime.Now;
                entity.DataPrzyjecia = DateTime.Now;
                entity.DataZakończenia = DateTime.Now;

                entity.Kontakt = kontakt.GetDictionaryItem();
                entity.KontaktId = entity.Kontakt.Id.ToString();

                sprawaRepository.AddUpdateItem(entity);
                AddFakeCzynnosci(clientContext, entity);
                AddFakeKoszty(clientContext, entity); 

            }
        }
    }
}
