﻿using System;
using System.Web;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using NLog;
using Microsoft.OData.Client;
using Beta = GraphBeta;
using System.IO;

namespace Kancelaria365.ConnectedServices
{
    public class CustomGraphService :
        BaseCustomGraphService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public CustomGraphService(HttpContextBase httpContext) : base(httpContext)
        {
        }

        public async Task<IEnumerable<Beta.OrgContact>> GetContacts()
        {
            IEnumerable<Beta.OrgContact> returnValue = null;
            try
            {
                var bestService = await BetaGraphServiceClient();
                var tasks = (DataServiceQuery<Beta.OrgContact>)bestService.Contacts;
                returnValue = await tasks.ExecuteAsync();
            }
            catch (DataServiceQueryException ex)
            {
                logger.Error(ex);
            }
            catch (DataServiceClientException ex)
            {
                logger.Error(ex);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnValue;
        }

        public async Task<Stream> GetCurrentUserPhoto()
        {
            Stream returnValue = null;
            try
            {
                var request = GraphServiceClient.Me.Photo.Content.Request();
                returnValue = await request.GetAsync();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnValue;
        }

        public async Task<int> GetUnreadMessages()
        {
            int unreadCount = 0;
            try
            {
                var bestService = await BetaGraphServiceClient();
                var inbox = bestService.Me
                    .MailFolders.Where(x => x.DisplayName.Equals("inbox", StringComparison.OrdinalIgnoreCase));

                var inboxFolder = (await ((DataServiceQuery<Beta.MailFolder>)inbox).ExecuteAsync()).FirstOrDefault();

                unreadCount = inboxFolder.UnreadItemCount.HasValue ? inboxFolder.UnreadItemCount.Value : 0;
            }
            catch (DataServiceQueryException ex)
            {
                logger.Error(ex);
            }
            catch (DataServiceClientException ex)
            {
                logger.Error(ex);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return unreadCount;
        }
    }
}
