﻿using System;
using System.Web;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.OData.Client;
using NLog;
using Microsoft.Graph;
using System.Data;
using System.Linq;
using Beta = GraphBeta;


namespace Kancelaria365.ConnectedServices.Services
{
    public class CalendarService : CustomGraphService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public CalendarService(HttpContextBase httpContext) : base(httpContext)
        {
        }

        public async Task<List<Calendar>> GetCallendarTypes()
        {
            List<Calendar> returnResults = new List<Calendar>();
            try
            {
                var request = GraphServiceClient.Me.Calendars.Request();
                logger.Debug(request.RequestUrl);

                var eventsResults = await request.GetAsync();
                returnResults = eventsResults.CurrentPage as List<Calendar>;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnResults;
        }

        public async Task<List<Event>>
            GetCalendarEventsAsync(DateTime start, DateTime end, string id)
        {
            List<Event> returnResults = new List<Event>();
            try
            {
                var request = GraphServiceClient.Me.Calendars[id].Events.Request();
                logger.Debug(request.RequestUrl);

                var eventsResults = await request.GetAsync();
                returnResults = eventsResults.CurrentPage as List<Event>;
            }
            catch(Microsoft.Graph.ServiceException ex)
            {
                logger.Error(ex);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnResults;
        }
    }
}
