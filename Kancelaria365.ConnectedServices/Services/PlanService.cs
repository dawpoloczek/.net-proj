﻿using Kancelaria365.ConnectedServices.Data;
using Microsoft.Graph;
using Microsoft.OData.Client;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Beta = GraphBeta;

namespace Kancelaria365.ConnectedServices.Services
{
    public class PlanService : CustomGraphService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public PlanService(HttpContextBase httpContext) : base(httpContext)
        {
        }

        public async Task<IEnumerable<Beta.Task>> GetPlanTasks(string planId)
        {
            IEnumerable<Beta.Task> returnValue = new List<Beta.Task>();
            try
            {
                var bestService = await BetaGraphServiceClient();
                DataServiceQuery<Beta.Task> serviceQuery = (DataServiceQuery<Beta.Task>)bestService.CreateQuery<Beta.Task>("plans/"+ planId +"/tasks");
                returnValue = await serviceQuery.ExecuteAsync();
            }
            catch (DataServiceQueryException ex)
            {
                logger.Error(ex);
            }
            catch (DataServiceClientException ex)
            {
                logger.Error(ex);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnValue;
        }
    }
}
