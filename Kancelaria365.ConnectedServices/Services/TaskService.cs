﻿using Kancelaria365.ConnectedServices.Data;
using Microsoft.Graph;
using Microsoft.OData.Client;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Beta = GraphBeta;

namespace Kancelaria365.ConnectedServices.Services
{
    public class TaskService : CustomGraphService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public TaskService(HttpContextBase httpContext) : base(httpContext)
        {
        }

        public async Task<IEnumerable<Beta.Task>> GetAssignedToMeTasks()
        {
            IEnumerable<Beta.Task> returnValue = null;
            try
            {
                var bestService = await BetaGraphServiceClient();
                var tasks = (DataServiceQuery<Beta.Task>)bestService.Tasks.Where(x => x.AssignedTo == "me");
                returnValue = await tasks.ExecuteAsync();
            }
            catch (DataServiceQueryException ex)
            {
                logger.Error(ex);
            }
            catch (DataServiceClientException ex)
            {
                logger.Error(ex);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnValue;
        }

        public async Task<IEnumerable<Beta.Task>> GetAssignedToMeTasksForLastMonth()
        {
            IEnumerable<Beta.Task> returnValue = null;
            try
            {
                var bestService = await BetaGraphServiceClient();

                DateTimeOffset dtOffsetLastMonth = DateTimeOffset.Now.AddMonths(-1);
                DateTimeOffset dtOffsetNow = DateTimeOffset.Now;
                var tasks = (DataServiceQuery<Beta.Task>)bestService
                    .Tasks.Where(x => x.AssignedTo == "me");
                returnValue = await tasks.ExecuteAsync();
                returnValue = returnValue.Where(x => (x.StartDateTime >= dtOffsetLastMonth && x.StartDateTime <= dtOffsetNow) ||
                                                        (x.DueDateTime >= dtOffsetLastMonth && x.DueDateTime <= dtOffsetNow));
            }
            catch (DataServiceQueryException ex)
            {
                logger.Error(ex);
            }
            catch (DataServiceClientException ex)
            {
                logger.Error(ex);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnValue;
        }

        public async Task<IEnumerable<Beta.Task>> GetCreatedByMeTasks()
        {
            IEnumerable<Beta.Task> returnValue = null;
            try
            {
                var bestService = await BetaGraphServiceClient();
                var tasks = (DataServiceQuery<Beta.Task>)bestService.Tasks.Where(x => x.CreatedBy == "me");
                returnValue = await tasks.ExecuteAsync();
            }
            catch (DataServiceQueryException ex)
            {
                logger.Error(ex);
            }
            catch (DataServiceClientException ex)
            {
                logger.Error(ex);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnValue;
        }
    }
}
