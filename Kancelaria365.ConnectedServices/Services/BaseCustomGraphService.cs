﻿using System.Threading.Tasks;
using System;
using System.Web;
using Microsoft.Graph;
using System.Net.Http.Headers;
using Microsoft.OData.Client;
using System.Net.Http;
using NLog;

namespace Kancelaria365.ConnectedServices
{
    public class BaseCustomGraphService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public HttpContextBase HttpContext { get; set; }

        private GraphServiceClient graphServiceClient;
        public GraphServiceClient GraphServiceClient
        {
            get
            {
                if (graphServiceClient == null)
                {
                    graphServiceClient = GetAuthenticatedClient();
                }
                return graphServiceClient;
            }
        }

        private GraphBeta.GraphService betaGraphServiceClient;
        public async Task<GraphBeta.GraphService> BetaGraphServiceClient()
        {
            GraphBeta.GraphService client = null;
            try
            {
                if (betaGraphServiceClient == null)
                {
                    string token = await AuthenticationHelper.GetUserAccessTokenAsync();

                    client = new GraphBeta.GraphService(new Uri("https://graph.microsoft.com/beta"));
                    client.IgnoreMissingProperties = true;

                    client.BuildingRequest += (object sender, BuildingRequestEventArgs e) =>
                    {
                        e.Headers.Add("Authorization", "Bearer " + token);
                    };
                    client.ReceivingResponse += (object sender, ReceivingResponseEventArgs e) =>
                    {
                        if (e.ResponseMessage.StatusCode != 200)
                        {
                            logger.Error(e.ResponseMessage.StatusCode);
                        }
                    };
                    betaGraphServiceClient = client;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return betaGraphServiceClient;
        }

        public HttpClient httpClientGraphService;
        public HttpClient HttpClientGraphService
        {
            get
            {
                try
                {
                    if (httpClientGraphService == null)
                    {
                        var httpClient = new HttpClient();
                        Task<string> token = AuthenticationHelper.GetUserAccessTokenAsync();
                        token.Wait();

                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Result);

                        httpClientGraphService = httpClient;
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
                return httpClientGraphService;
            }
        }

        public BaseCustomGraphService(HttpContextBase httpContext)
        {
            this.HttpContext = httpContext;
        }

        public static GraphServiceClient GetAuthenticatedClient()
        {
            GraphServiceClient graphClient = null;
            try
            {
                graphClient = new GraphServiceClient(
                   new DelegateAuthenticationProvider(
                       async (requestMessage) =>
                       {
                           string accessToken = await AuthenticationHelper.GetUserAccessTokenAsync();

                        // Append the access token to the request.
                        requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", accessToken);
                       }));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return graphClient;
        }
    }
}
