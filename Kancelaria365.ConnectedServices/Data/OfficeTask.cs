﻿using Microsoft.OData.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kancelaria365.ConnectedServices.Data
{
    public class Body
    {
        public string ContentType { get; set; }
        public string Content { get; set; }
    }

    public class DueDateTime
    {
        public string DateTime { get; set; }
        public string TimeZone { get; set; }
    }

    public class StartDateTime
    {
        public string DateTime { get; set; }
        public string TimeZone { get; set; }
    }

    public class Value
    {
        public string id { get; set; }
        public string etag { get; set; }
        public string Id { get; set; }
        //public string CreatedDateTime { get; set; }
        //public string LastModifiedDateTime { get; set; }
        //public string ChangeKey { get; set; }
        //public List<object> Categories { get; set; }
        //public string AssignedTo { get; set; }
        //public Body Body { get; set; }
        //public object CompletedDateTime { get; set; }
        //public DueDateTime DueDateTime { get; set; }
        //public bool HasAttachments { get; set; }
        //public string Importance { get; set; }
        //public bool IsReminderOn { get; set; }
        //public string Owner { get; set; }
        //public string ParentFolderId { get; set; }
        //public object Recurrence { get; set; }
        //public object ReminderDateTime { get; set; }
        //public string Sensitivity { get; set; }
        //public StartDateTime StartDateTime { get; set; }
        //public string Status { get; set; }
        //public string Subject { get; set; }
    }

    public class RootObject
    {
        public string context { get; set; }
        public List<Value> value { get; set; }
    }
}
