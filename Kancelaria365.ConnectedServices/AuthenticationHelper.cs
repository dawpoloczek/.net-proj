﻿using Microsoft.Identity.Client;
using Microsoft.Owin.Builder;
using Microsoft.Owin.Security.Cookies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin.Security.Notifications;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Owin.Security;
using Owin;

namespace Kancelaria365.ConnectedServices
{
    public static class AuthenticationHelper
    {
        // Wszystkie dane ustawioane https://apps.dev.microsoft.com/#/appList

        public static string appClientSecret = ConfigurationManager.AppSettings["app:ClientSecret"];//"za2GskUAue0cnqnoc29kkWr";
        public static string appAuthority = ConfigurationManager.AppSettings["app:Authority"];// "https://login.microsoftonline.com/common/v2.0";
        public static string appClientID = ConfigurationManager.AppSettings["app:ClientID"]; //"431bd476-e69a-4955-b168-fd4201c7badc";
        public static string appRedirectUri = ConfigurationManager.AppSettings["app:RedirectUri"];// "https://localhost:44342"; 
        public static string[] appScopes;

        public static void ConfigurationAuthenticatio(IAppBuilder app, string[] scopes)
        {
            appScopes = scopes;
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseOpenIdConnectAuthentication(
              new OpenIdConnectAuthenticationOptions
              {
                  ClientId = appClientID,
                  Authority = appAuthority,
                  Scope = "openid offline_access profile email " + string.Join(" ", appScopes),
                  RedirectUri = appRedirectUri,
                  PostLogoutRedirectUri = "/",
                  TokenValidationParameters = new System.IdentityModel.Tokens.TokenValidationParameters
                  {
                      ValidateIssuer = false
                  },
                  Notifications = new OpenIdConnectAuthenticationNotifications
                  {
                      AuthenticationFailed = OnAuthenticationFailed,
                      AuthorizationCodeReceived = OnAuthorizationCodeReceived
                  }
              }
            );
        }

        private static Task OnAuthenticationFailed(AuthenticationFailedNotification<OpenIdConnectMessage,
          OpenIdConnectAuthenticationOptions> notification)
        {
            notification.HandleResponse();
            string redirect = "/Home/Error?message=" + notification.Exception.Message;
            if (notification.ProtocolMessage != null && !string.IsNullOrEmpty(notification.ProtocolMessage.ErrorDescription))
            {
                redirect += "&debug=" + notification.ProtocolMessage.ErrorDescription;
            }
            notification.Response.Redirect(redirect);
            return Task.FromResult(0);
        }

        private static async Task OnAuthorizationCodeReceived(AuthorizationCodeReceivedNotification notification)
        {
            // Get the signed in user's id and create a token cache
            string signedInUserId = notification.AuthenticationTicket.Identity.FindFirst(ClaimTypes.NameIdentifier).Value;
            SessionTokenCache tokenCache = new SessionTokenCache(signedInUserId,
                notification.OwinContext.Environment["System.Web.HttpContextBase"] as HttpContextBase);

            ConfidentialClientApplication cca = new ConfidentialClientApplication(
                appClientID, appRedirectUri, new ClientCredential(appClientSecret), tokenCache);

            try
            {
                var result = await cca.AcquireTokenByAuthorizationCodeAsync(appScopes, notification.Code);
            }
            catch (MsalException ex)
            {
                string message = "AcquireTokenByAuthorizationCodeAsync threw an exception";
                string debug = ex.Message;
                notification.HandleResponse();
                notification.Response.Redirect("/Home/Error?message=" + message + "&debug=" + debug);
            }
        }

        public static async Task<string> GetUserAccessTokenAsync()
        {
            string signedInUserID = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
            SessionTokenCache tokenCache = new SessionTokenCache(
                signedInUserID,
                HttpContext.Current.GetOwinContext().Environment["System.Web.HttpContextBase"] as HttpContextBase);

            ConfidentialClientApplication cca = new ConfidentialClientApplication(
                appClientID,
                appRedirectUri,
                new Microsoft.Identity.Client.ClientCredential(appClientSecret),
                tokenCache);

            try
            {
                Microsoft.Identity.Client.AuthenticationResult result =
                    await cca.AcquireTokenSilentAsync(appScopes);
                return result.Token;
            }

            catch (MsalSilentTokenAcquisitionException ex)
            {
                HttpContext.Current.Request.GetOwinContext().Authentication.Challenge(
                    new Microsoft.Owin.Security.AuthenticationProperties() { RedirectUri = "/" },
                    Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationDefaults.AuthenticationType);
                throw ex;
                // throw new Exception(Resource.Error_AuthChallengeNeeded);
            }
        }
    }
}
