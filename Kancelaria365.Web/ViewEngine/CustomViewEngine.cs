﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kancelaria365.Web
{
    public class CustomViewEngine: RazorViewEngine
    {
        private static string[] NewPartialViewFormats = new[] {
                "~/Views/Shared/{1}/{0}.cshtml",
                "~/Views/Shared/{1}/Details/{0}.cshtml",
                "~/Views/Shared/_Grids/{0}.cshtml",
                "~/Views/Shared/_MenuTreeView/{0}.cshtml"
        };

        public CustomViewEngine()
        {
            base.PartialViewLocationFormats = base.PartialViewLocationFormats.Union(NewPartialViewFormats).ToArray();
        }
    }
}