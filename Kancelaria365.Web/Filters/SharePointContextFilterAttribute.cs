using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using System.Web.Mvc;
using System.Web;
using Kancelaria365.ConnectedServices;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.OData.ProxyExtensions;

using System.Threading.Tasks;
using Kancelaria365.ConnectedServices.Services;
using System.Security.Claims;
using NLog;
using System.Configuration;
using Microsoft.Owin.Security.Cookies;
using Kancelaria365.Web.Controllers;
using System.Web.Routing;

namespace Kancelaria365.Web
{
    /// <summary>
    /// SharePoint action filter attribute.
    /// </summary>
    public class SharePointContextFilterAttribute : ActionFilterAttribute
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            #if DEBUG
            AzureFilter(filterContext);
            #else
            Uri redirectUrl;
            switch (SharePointContextProvider.CheckRedirectionStatus(filterContext.HttpContext, out redirectUrl))
            {
                case RedirectionStatus.Ok:
                    AzureFilter(filterContext);
                    return;
                case RedirectionStatus.ShouldRedirect:
                    filterContext.Result = new RedirectResult(redirectUrl.AbsoluteUri);
                    break;
                case RedirectionStatus.CanNotRedirect:
                    filterContext.Result = new ViewResult { ViewName = "Error" };
                    break;
            }
            #endif
        }

        private void AzureFilter(ActionExecutingContext filterContext)
        {
            try
            {
                HttpContextBase httpContext = filterContext.RequestContext.HttpContext;
                if (!httpContext.Request.IsAuthenticated)
                {
                    filterContext.Result = new RedirectToRouteResult(
                             new RouteValueDictionary(new { controller = "Base", action = "SignIn", CurrentUrl = httpContext.Request.Url.PathAndQuery }));
                }
                else
                {
                    string userName = ClaimsPrincipal.Current.FindFirst("name").Value;
                    string userId = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
                    if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(userId))
                    {
                        filterContext.Result = new RedirectToRouteResult(
                         new RouteValueDictionary(new { controller = "Base", action = "SignOut", CurrentUrl = httpContext.Request.Url.PathAndQuery })); ;
                    }
                    SessionTokenCache tokenCache = new SessionTokenCache(userId, httpContext);
                    if (tokenCache.Count <= 0)
                    {
                        filterContext.Result = new RedirectToRouteResult(
                         new RouteValueDictionary(new { controller = "Base", action = "SignOut", CurrentUrl = httpContext.Request.Url.PathAndQuery }));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
    }
}