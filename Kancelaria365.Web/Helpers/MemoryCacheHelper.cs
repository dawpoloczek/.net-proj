﻿using System;
using SP = Microsoft.SharePoint.Client;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Threading.Tasks;
using System.Threading;
using NLog;
using Kancelaria365.Data;
using Kancelaria365.Common;

namespace Kancelaria365.Web.Helpers
{
    public class MemoryCacheHelper<T, Z>
        where T : GenericRepositoryBase<Z>, new()
        where Z : ICustomEntity, new()
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        public SP.ClientContext Context { get; set; }
        public T Repository { get; set; }

        private static object _lock = new Object();
        private static MemoryCache _cache = MemoryCache.Default;

        private string entityListCacheKey = string.Empty;

        public MemoryCacheHelper(SP.ClientContext context)
        {
            this.Context = context;
            this.Repository = new T() { Context = Context };

        }
        public IEnumerable<Z> EntityList
        {
            get
            {
                lock (_lock)
                {
                    IEnumerable<Z> items = null;
                    try
                    {
                        var cache = _cache.Get(Repository.EntityListCacheKey);
                        if (cache != null)
                        {
                            items = cache as IEnumerable<Z>;
                        }
                        else
                        {
                            items = AddToCache(items);
                        }
                    }
                    catch (Exception)
                    {
                        items = AddToCache(items);
                    }
                    return items;
                }
            }
        }

        private IEnumerable<Z> AddToCache(IEnumerable<Z> items)
        {
            try
            {
                items = Repository.GetEntityList();
                _cache.Add(Repository.EntityListCacheKey, items, new CacheItemPolicy());
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return items;
        }

        public async Task RefreshCache()
        {
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    _cache.Remove(Repository.EntityListCacheKey);
                    var x = EntityList;
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            });
        }

        public List<DictionaryItem> GetDictionaryEntityList(bool addDefaultValue = true)
        {
            var returnList = new List<DictionaryItem>();
            try
            {
                EntityList.ToList().ForEach(x => returnList.Add(new DictionaryItem(x.Id, x.Value)));
                if (addDefaultValue)
                {
                    var item = new DictionaryItem(0, GlobalNames.Constants.DefaultListValue);
                    returnList.Insert(0, item);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnList;
        }
    }
}