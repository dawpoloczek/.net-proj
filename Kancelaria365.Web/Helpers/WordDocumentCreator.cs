﻿using SP = Microsoft.SharePoint.Client;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Kancelaria365.Data;

namespace Kancelaria365.Common.Helpers
{
    public class WordDocumentCreator
    {
        public SP.ClientContext Context { get; set; }
        public static Logger logger = LogManager.GetCurrentClassLogger();
        public WordDocumentCreator(SP.ClientContext context)
        {
            this.Context = context;
        }
        public byte[] GetDocumentFromLibrary(string documnetServerRealtiveUrl, object obj)
        {
            byte[] bytesArrey = null;
            try
            {
                SP.Web web = Context.Web;
                Context.Load(web, website => website.ServerRelativeUrl);
                Context.ExecuteQuery();

                string strServerRelativeURL = CombineUrl(web.ServerRelativeUrl, documnetServerRealtiveUrl);

                Microsoft.SharePoint.Client.File oFile = web.GetFileByServerRelativeUrl(strServerRelativeURL);
                Context.Load(oFile);
                SP.ClientResult<System.IO.Stream> stream = oFile.OpenBinaryStream();
                Context.ExecuteQuery();
                bytesArrey = CreateDocument(stream, obj);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return bytesArrey;
        }

        private Novacode.DocX ReplaceTokens(Novacode.DocX wordDocumnet, object obj)
        {
            foreach (var prop in obj.GetType().GetProperties())
            {
                try
                {
                    if (wordDocumnet.Text.Contains("[" + prop.Name + "]"))
                    {
                        if (prop.GetValue(obj, null) != null)
                        {
                            if (prop.GetValue(obj, null).GetType() == typeof(DictionaryItem) ||
                                prop.GetValue(obj, null).GetType() == typeof(UserDictionaryItem))
                            {
                                DictionaryItem dicItem = (DictionaryItem)prop.GetValue(obj, null);
                                if (!string.IsNullOrEmpty(dicItem.Value))
                                {
                                    wordDocumnet.ReplaceText("[" + prop.Name + "]", dicItem.Value);
                                }
                            }
                            else
                            {
                                wordDocumnet.ReplaceText("[" + prop.Name + "]", prop.GetValue(obj, null).ToString());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }
            return wordDocumnet;
        }

        private Novacode.DocX ReplaceEmptyTokens(Novacode.DocX wordDocumnet)
        {
            MatchCollection emptyTokens = Regex.Matches(wordDocumnet.Text, @"\[.*?\]");
            foreach (Match token in emptyTokens)
            {
                try
                {
                    wordDocumnet.ReplaceText(token.Value, "");
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }
            return wordDocumnet;
        }

        private byte[] CreateDocument(SP.ClientResult<System.IO.Stream> stream, object obj)
        {
            byte[] bytesArrey = null;
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {

                    stream.Value.CopyTo(ms);

                    Novacode.DocX wordDocumnet = Novacode.DocX.Load(ms);
                    wordDocumnet = ReplaceTokens(wordDocumnet, obj);
                    wordDocumnet = ReplaceEmptyTokens(wordDocumnet);

                    wordDocumnet.Save();
                    bytesArrey = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return bytesArrey;
        }

        public static string CombineUrl(string path1, string path2)
        {
            return path1.TrimEnd('/') + '/' + path2.TrimStart('/');
        }
    }
}
