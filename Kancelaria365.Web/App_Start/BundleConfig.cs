﻿using System.Web;
using System.Web.Optimization;

namespace Kancelaria365.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                                         "~/Scripts/jquery-2.2.4.min.js",
                                         "~/Scripts/jquery-ui-1.12.1.min.js"));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                                         "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/spcontext").Include(
                                         "~/Scripts/spcontext.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                                         "~/Scripts/custom/kancelariaSP365.Main.js",
                                         "~/Scripts/custom/kancelariaSP365.CustFieldInput.js",
                                         "~/Scripts/custom/kancelariaSP365.CustAdvancedSearchPanel.js",
                                         "~/Scripts/custom/kancelariaSP365.CascadeKontaktSprawaDropDowns.js"));

            bundles.Add(new ScriptBundle("~/bundles/gentelella_top").Include(
                                         "~/Scripts/js/moment/moment.min.js",
                                         "~/Scripts/js/bootstrap.min.js",
                                         "~/Scripts/js/datepicker/bootstrap-datetimepicker.js",
                                         "~/Scripts/js/datepicker/bootstrap-datepicker.js",
                                         "~/Scripts/bootstrap-dialog.js",
                                         "~/Scripts/locales/bootstrap-datepicker.pl.min.js",
                                         "~/Scripts/js/progressbar/bootstrap-progressbar.min.js",
                                         "~/Scripts/js/nicescroll/jquery.nicescroll.min.js",
                                         "~/Scripts/js/icheck/icheck.min.js",
                                         "~/Scripts/js/moment/moment.min.js",
                                         "~/Scripts/js/datepicker/daterangepicker.js",
                                         "~/Scripts/js/input_mask/jquery.inputmask.js",
                                         "~/Scripts/js/pace/pace.min.js",
                                         "~/Scripts/js/chartjs/chart.min.js",
                                         "~/Scripts/respond.js",
                                         "~/Scripts/js/calendar/fullcalendar.js",
                                         "~/Scripts/js/locale/pl.js",
                                         "~/Scripts/js/jsgrid/jsgrid.js"));

            bundles.Add(new ScriptBundle("~/bundles/gentelella_bottom").Include(
                                         "~/Scripts/js/custom.js",
                                         "~/Scripts/js/chartjs/chart.min.js"));

            bundles.Add(new StyleBundle("~/bundles/gentelella").Include(
                                        "~/Content/gentelella/css/bootstrap.min.css",                                        
                                        "~/Content/gentelella/css/animate.min.css",
                                        "~/Content/gentelella/css/datepicker/bootstrap-datepicker.min.css",
                                        "~/Content/gentelella/css/custom.css",
                                        "~/Content/gentelella/css/kancelaria.css",
                                        "~/Content/gentelella/css/calendar/fullcalendar.css",
                                        "~/Content/gentelella/css/jsgrid/jsgrid-theme.css",
                                        "~/Content/gentelella/css/jsgrid/jsgrid.css",
                                        "~/Content/bootstrap-dialog.css")
                                        .Include("~/Content/gentelella/fonts/css/font-awesome.min.css", new CssRewriteUrlTransform())
                                        .Include("~/Content/gentelella/css/icheck/flat/green.css", new CssRewriteUrlTransform()));

        }
    }
}
