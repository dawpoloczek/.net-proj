﻿using Kancelaria365.ConnectedServices;
using Microsoft.Owin;
using Owin;
using System;
using System.Configuration;

[assembly: OwinStartup(typeof(Kancelaria365.Web.Startup))]

namespace Kancelaria365.Web
{
    public class Startup
    {
        public static string[] appScopesGraph = ConfigurationManager.AppSettings["app:AppScopes"].Replace(' ', ',').Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        public static string[] appScopesOffice = ConfigurationManager.AppSettings["app:AppScopesOffice"].Replace(' ', ',').Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        public void Configuration(IAppBuilder app)
        {
            AuthenticationHelper.ConfigurationAuthenticatio(app, appScopesGraph);
        }
    }
}
