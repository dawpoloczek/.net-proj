﻿function CascadeKontaktSprawaDropDowns() { };

CascadeKontaktSprawaDropDowns.init = function (kontaktyDropDownId, sprawyDropDownId, getKontaktyControllerAction, getSprawyControllerAction) {
    var firma = $("#" + kontaktyDropDownId);
    firma.change(function () {
        var selectedFirma = firma.val();

        var sprawa = $("#" + sprawyDropDownId);

        var spHostUrl = getSPHostUrlFromQueryString(window.location.search);

        $.ajax({
            type: "POST",
            url: getSprawyControllerAction + '?spHostUrl=' + spHostUrl,
            data: "{'kontaktId':" + selectedFirma + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "accept": "application/json;odata=verbose",
                "content-type": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            success: function (data) {
                if (data != null) {

                    sprawa.empty();
                    var items = ""
                    $(data.items).map(function (i, item) {
                        items = items + "<option  value='" + item.Id + "'>" + item.Value + "</option>";
                    });
                    sprawa.append('<option value="0">--Wybierz--</option>');
                    sprawa.append(items);
                }
            },
            error: function (xhr, status, error) {
                Logger.logAjaxRequestError(xhr, status, error);
            }
        });
    });

    //var sprawa = $("#" + sprawyDropDownId);
    //sprawa.change(function () {
    //    var firma = $("#" + kontaktyDropDownId);

    //    var selectedSprawa = sprawa.val();

    //    var spHostUrl = getSPHostUrlFromQueryString(window.location.search);

    //    $.ajax({
    //        type: "POST",
    //        url: getKontaktyControllerAction + '?spHostUrl=' + spHostUrl,
    //        data: "{'sprawaId':" + selectedSprawa + "}",
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        headers: {
    //            "accept": "application/json;odata=verbose",
    //            "content-type": "application/json;odata=verbose",
    //            "X-RequestDigest": $("#__REQUESTDIGEST").val()
    //        },

    //        success: function (data) {
    //            if (data != null) {
    //                var items = ""
    //                $(data.items).map(function (i, item) {
    //                    items = items + "<option  value='" + item.Id + "'>" + item.Value + "</option>";
    //                });
    //                if (data.items.length > 1) {
    //                    firma.append('<option value="0">--Wybierz--</option>');
    //                }
    //                firma.append(items);
    //            }
    //        },
    //        error: function (xhr, status, error) {
    //            Logger.logAjaxRequestError(xhr, status, error);
    //        }
    //    });
    //})
}

