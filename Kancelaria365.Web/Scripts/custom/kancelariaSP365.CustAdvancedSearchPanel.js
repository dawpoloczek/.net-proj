﻿$.fn.advancedSearchPanel = function (objectsList, searchClickedCallback) {
	var _self = this;
	var idGen = new CustomRandGenerator();
	var searchTextId = "searchTextId_" + idGen.getId();
	var searchButtonId = "dokumentyFileInputText" + idGen.getId();
    
	var checkBoxesDiv = "<div class='col-md-6' style='margin-top:5px'></div>";
	var textBoxDiv = "<div class='col-md-5'><input placeholder=\"Wyszukiwarka zaawansowana korzysta z mechanizmu 'Search' SharePoint'a\" class='form-control col-md-12' id='" + searchTextId + "' type='text'></div>";
	var searchButtonDiv = "<div class='col-md-1'><button id='" + searchButtonId + "' type='button' class='btn btn-info'>Szukaj</button></div>"
    
	if (objectsList != null) {
	    for (var i = 0; i < objectsList.length; i++) {
	        var content = "<div class='filter-selector'>" +
                                    "<label class='filter-selector-label'>" + objectsList[i]["Label"] + "</label>" +
                                    "<div class='material-switch pull-right'>" +
                                        "<input id='" + objectsList[i]["Name"] + "' name='" + objectsList[i]["Name"] + "' type='checkbox' />" +
                                        "<label for='" + objectsList[i]["Name"] + "' class='label-default'></label>" +
                                    "</div>" +
                                "</div>";

	        checkBoxesDiv = $(checkBoxesDiv).append(content);
	    }
	}

	var result = $(this)
        .append(checkBoxesDiv)
		.append(textBoxDiv)
        .append(searchButtonDiv);

	this.searchClicked = function(){
	    searchClickedCallback();
	};

	this.getCheckedStatus = function(){
	    var items = $(this).find(".filter-selector div input");
	    var checkedStatusTable = {};
	    $.each(items, function (index, value) {
	        checkedStatusTable[$(value).attr("name")] = $(value).is(":checked");
	    });
	    var searchText = $(this).find("#" + searchTextId).val();
	    return { 'CheckboxesStatus': checkedStatusTable, 'SearchTest': searchText };
	};

	$(document).ready(function () {
	    $("#" + searchButtonId).on("click", function () {
	        var searchText = $(_self).find("#" + searchTextId).val();
	        if (searchText.length > 0) {
	            _self.searchClicked();
	        }
	    });

	    $("#"+ searchTextId).keypress(function (e) {
	        if (e.which == 13) {
	            $("#" + searchButtonId).click();
	            return false;
	        }
	    });
	});

	return this;
};
