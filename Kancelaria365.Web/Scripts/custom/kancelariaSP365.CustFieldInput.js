﻿$.fn.custFieldInput = function (ajaxLoadURL, loadFileClickCallback) {
    var _self = this;
    var idGen = new CustomRandGenerator();
    var sendButtonId = "sendFileButton_" + idGen.getId();
    var dokumentyFileInputTextId = "dokumentyFileInputText" + idGen.getId();
    this.attr("style", "display: none;");
    var result = $(this)
		.wrap("<div class=\"col-md-12\"></div>")
		.after("<div class=\"col-md-2\"><input type=\"button\" id=\"" + sendButtonId + "\" value=\"Zapisz\" class=\"btn btn-success\"/></div><div class=\"col-md-5\" />")
		.wrap("<div class=\"col-md-5\" style=\"padding-left: 0px;\"></div>")
		.wrap("<div class=\"input-group\"></div")
		.after("<input id=\"" + dokumentyFileInputTextId + "\" type=\"text\" class=\"form-control\" readonly>")
		.wrap("<label class=\"input-group-btn\"></label>")
		.wrap("<span class=\"btn btn-primary\"></span>")
		.before("Wybierz&hellip;");

    this.getFileInputContent = function () {
        var fileInput = $(_self)[0];
        // Create a formdata object and add the files
        var files = fileInput.files;
        var data = new FormData();
        $.each(files, function (key, value) {
            data.append(key, value);
        });
        $(fileInput).wrap('<form>').closest('form').get(0).reset();
        $(fileInput).unwrap();
        $("#" + dokumentyFileInputTextId).val('');
        return data;
    },

	this.custUploadFile = function (event) {
	    loadFileClickCallback(_self, event);
	    //$.ajax({
	    //    url: ajaxLoadURL,
	    //    type: 'POST',
	    //    data: data,
	    //    cache: false,
	    //    dataType: 'json',
	    //    processData: false, // Don't process the files
	    //    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	    //    success: function (data, textStatus, jqXHR) {
	    //        if (typeof data.error === 'undefined') {
	    //            // Success so call function to process the form
	    //            $(fileInput).wrap('<form>').closest('form').get(0).reset();
	    //            $(fileInput).unwrap();
	    //            $("#" + dokumentyFileInputTextId).val('');
	    //            loadCallbackSuccess(data);
	    //        }
	    //        else {
	    //            $(fileInput).wrap('<form>').closest('form').get(0).reset();
	    //            $(fileInput).unwrap();
	    //            $("#" + dokumentyFileInputTextId).val('');
	    //            loadCallbackError();
	    //        }
	    //    },
	    //    error: function (jqXHR, textStatus, errorThrown) {
	    //        $(fileInput).wrap('<form>').closest('form').get(0).reset();
	    //        $(fileInput).unwrap();
	    //        $("#" + dokumentyFileInputTextId).val('');
	    //        loadCallbackError();
	    //    }
	    //});
	};

    $(document).ready(function () {
        $(_self).on('change', function (event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
				log = this.files.length > 0 ? this.files[0].name : "";

            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }

        });

        $("#" + sendButtonId).on('click', _self.custUploadFile);
    });

    return result;
};
