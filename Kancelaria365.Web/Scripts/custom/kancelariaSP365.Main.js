﻿function CustomRandGenerator() { };

CustomRandGenerator.prototype.rand = Math.floor(Math.random() * 26) + Date.now();

CustomRandGenerator.prototype.getId = function () {
    return this.rand++;
}


var Logger =
{
    logError: function (e) {
        console.log(e.message);
        console.log(e.stack);
        var message = e.stack + "\nUrl:\n" + window.location.href;
        Logger.logToServer("Error", e.message, message);
    },
    logAjaxRequestError: function (xhr, status, error) {
        console.log(error);
        console.log(xhr.responseText);
        var message = xhr.responseText + "\nUrl:\n" + window.location.href;
        Logger.logToServer("AjaxRequestError", error, message);
    },
    logClientContextError: function (message, stackTrace) {
        console.log(message);
        console.log(stackTrace);
        var stackTraceMessage = stackTrace + "\nUrl:\n" + window.location.href;
        Logger.logToServer("ClientContextError", message, stackTraceMessage);
    },
    logWarrning: function (text) {
        console.log(text);
        var message = text + "\nUrl:\n" + window.location.href;
        Logger.logToServer("Warrning", text, message);
    },
    logInfo: function (text) {
        console.log(text);
        Logger.logToServer("Info", text, text);
    },
    logToServer: function (type, message, stackTrace) {

    }
}

var UserHelper =
    {
        createUserPicker: function (additionalValue, value, id) {

            $("#" + value).autocomplete({
                source: function (request, response) {
                    var spHostUrl = getSPHostUrlFromQueryString(window.location.search);
                    var loaderControl = "<img id='" + value + "_loader" + "' src='/Images/squares.gif' style='float:left;position:absolute;'>";

                    $("#" + additionalValue + "_loader").remove();
                    $("#" + value).after(loaderControl);

                    $.ajax({
                        type: "POST",
                        url: '/Base/SearchPeople?spHostUrl=' + spHostUrl,
                        data: "{'searchText':'" + request.term + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        headers:
                            {
                                "accept": "application/json;odata=verbose",
                                "content-type": "application/json;odata=verbose",
                                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                            },
                        success: function (data) {
                            $("#" + additionalValue).val("");
                            if (data.length > 0) {
                                $("#" + additionalValue).val(data[0].AdditionalValue)
                            }
                            response($.map(data, function (item) {
                                return {
                                    value: item.Value,
                                    additionalValue: item.AdditionalValue,
                                    id: item.Id
                                }
                            }));
                            $("#" + value + "_loader").remove();
                        },
                        error: function (xhr, status, error) {
                            Logger.logAjaxRequestError(xhr, status, error);

                            $("#" + additionalValue).val("");
                            $("#" + value + "_loader").remove();
                        }
                    })
                },
                minLength: 3,
                select: function (event, ui) {
                    event.preventDefault();
                    $("#" + value).val(ui.item.value);
                    $("#" + additionalValue).val(ui.item.additionalValue);
                    $("#" + id).val(ui.item.id)
                }
            });
        }
    }


var GuiHelper =
    {
        Count: 0,

        ajaxIndicatorStart: function (text, containerId) {
            if (GuiHelper.Count == 0) {
                if (containerId === undefined) {
                    containerId = "body";
                }
                else {
                    containerId = "#" + containerId;
                }
                if (jQuery(containerId).find('#resultLoading').attr('id') !== 'resultLoading') {
                    jQuery(containerId).append('<div id="resultLoading" style="display:none"><div><img src="/Images/squares.gif"><div>' + text + '</div></div><div class="bg"></div></div>');
                }

                jQuery(containerId + ' #resultLoading').css({
                    'width': '100%',
                    'height': '100%',
                    'position': 'fixed',
                    'z-index': '10000000',
                    'top': '0',
                    'left': '0',
                    'right': '0',
                    'bottom': '0',
                    'margin': 'auto'
                });

                jQuery(containerId + ' #resultLoading .bg').css({
                    'background': '#000000',
                    'opacity': '0.7',
                    'width': '100%',
                    'height': '100%',
                    'position': 'absolute',
                    'top': '0'
                });

                jQuery(containerId + ' #resultLoading>div:first').css({
                    'width': '250px',
                    'height': '75px',
                    'text-align': 'center',
                    'position': 'fixed',
                    'top': '0',
                    'left': '0',
                    'right': '0',
                    'bottom': '0',
                    'margin': 'auto',
                    'font-size': '16px',
                    'z-index': '10',
                    'color': '#ffffff'

                });

                jQuery(containerId + ' #resultLoading .bg').height('100%');
                jQuery(containerId + ' #resultLoading').fadeIn(300);
                //jQuery(containerId).css('cursor', 'wait');
            }
            GuiHelper.Count++;
        },

        ajaxIndicatorStop: function (containerId) {
            if (GuiHelper.Count > 0)
            {
                GuiHelper.Count--;
            }
            if (GuiHelper.Count == 0) {
                if (containerId === undefined) {
                    containerId = "body";
                }
                else {
                    containerId = "#" + containerId;
                }
                jQuery(containerId + ' #resultLoading .bg').height('100%');
                jQuery(containerId + ' #resultLoading').fadeOut(300);
                //jQuery(containerId).css('cursor', 'default');

                $(document).trigger("ajaxIndicatorComplited");
            }
        },
               
        deleteItem: function (url) {
           BootstrapDialog.confirm({
                title: "Ostrzeżenie",
                type: BootstrapDialog.TYPE_DANGER,
                message: "Czy na pewno chcesz usunąć element ?",
                callback: function (result) {
                    if (result) {
                        window.location.href = url;
                    }
                }
            });
        }
    }

var DateTimeHelper =
{
    createDatePicker: function (controlId, formatString) {

        //$("#" + controlId).datepicker({
        //    language: "pl"
        //});

        $("#" + controlId).daterangepicker({
            format: 'YYYY-MM-DD',
            singleDatePicker: true,
            calender_style: "picker_2"
        });
    }
}

var GraphHelper =
{
    UnreadMessagesCount: function () {
        var returnValue = 0;
        $.ajax({
            type: "GET",
            url: '/Base/GetUnreadMessagesCount',
            contentType: "application/json; charset=utf-8",
            async: false,
            cache: false,
            dataType: "json",
            headers:
                {
                    "accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                },
            success: function (data) {
                returnValue = data;
            },
            error: function (xhr, status, error) {
                Logger.logAjaxRequestError(xhr, status, error);
            }
        });
        return returnValue;
    }
}