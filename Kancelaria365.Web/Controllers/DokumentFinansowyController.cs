﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SP = Microsoft.SharePoint.Client;
using System.Net;
using Microsoft.SharePoint.Client;
using System.IO;
using Kancelaria365.Data;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Web.Controllers
{
    public class DokumentFinansowyController :
        BaseCacheGenericController<DokumentyFinansoweRepository, DokumentFinansowy>
    {
        private new static Logger logger = LogManager.GetCurrentClassLogger();

        [SharePointContextFilter]
        [HttpPost]
        public HttpStatusCode Create(DokumentFinansowy model)
        {
            using (Context)
            {
                try
                {
                    SP.File fileUploaded = UploadDocumentToRepository(GlobalNames.Lists.KontaktyDokumentyFinansowe, "");
                    Context.Load(fileUploaded.ListItemAllFields);
                    Context.ExecuteQuery();
                    model.Id = (int)fileUploaded.ListItemAllFields["ID"];
                    DokumentyFinansoweRepository.AddUpdateItem(model);
                    Refresh();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return HttpStatusCode.InternalServerError;
                }
            }
            return HttpStatusCode.OK;
        }

        [SharePointContextFilter]
        [HttpPost]
        public HttpStatusCode Edit(DokumentFinansowy model)
        {
            using (Context)
            {
                try
                {
                    DokumentyFinansoweRepository.AddUpdateItem(model);
                    Refresh();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return HttpStatusCode.InternalServerError;
                }
            }
            return HttpStatusCode.OK;
        }


        public SP.File UploadDocumentToRepository(string listName, string filePath)
        {
            try
            {
                using (Context)
                {
                    List list = Context.Web.Lists.GetByTitle(listName);
                    // Get to folder to upload into 
                    Context.Load(list, l => l.RootFolder);
                    // Get the information about the folder that will hold the file
                    Context.Load(list.RootFolder, f => f.ServerRelativeUrl);
                    Context.ExecuteQuery();
                    SP.File uploadFile = null;
                    int fileChunkSizeInMB = 3;
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                                    //Use the following properties to get file's name, size and MIMEType
                        int fileSize = file.ContentLength;
                        string fileName = file.FileName;
                        string mimeType = file.ContentType;
                        System.IO.Stream fileContent = file.InputStream;
                        ClientResult<long> bytesUploaded = null;
                        try
                        {
                            // Each sliced upload requires a unique id
                            Guid uploadId = Guid.NewGuid();

                            // Get the name of the file
                            string uniqueFileName = list.RootFolder.ServerRelativeUrl + "/" + filePath + fileName;

                            // Calculate block size in bytes
                            int blockSize = fileChunkSizeInMB * 1024 * 1024;

                            if (fileSize <= blockSize)
                            {
                                // Use regular approach
                                using (FileStream fs = fileContent as FileStream)
                                {
                                    FileCreationInformation fileInfo = new FileCreationInformation();
                                    fileInfo.ContentStream = fileContent;
                                    fileInfo.Url = uniqueFileName;
                                    fileInfo.Overwrite = true;
                                    uploadFile = list.RootFolder.Files.Add(fileInfo);
                                    Context.Load(uploadFile);
                                    Context.ExecuteQuery();
                                }
                            }
                            else
                            {
                                try
                                {
                                    using (BinaryReader br = new BinaryReader(fileContent))
                                    {
                                        byte[] buffer = new byte[blockSize];
                                        Byte[] lastBuffer = null;
                                        long fileoffset = 0;
                                        long totalBytesRead = 0;
                                        int bytesRead;
                                        bool first = true;
                                        bool last = false;

                                        // Read data from filesystem in blocks 
                                        while ((bytesRead = br.Read(buffer, 0, buffer.Length)) > 0)
                                        {
                                            totalBytesRead = totalBytesRead + bytesRead;

                                            // We've reached the end of the file
                                            if (totalBytesRead == fileSize)
                                            {
                                                last = true;
                                                // Copy to a new buffer that has the correct size
                                                lastBuffer = new byte[bytesRead];
                                                Array.Copy(buffer, 0, lastBuffer, 0, bytesRead);
                                            }

                                            if (first)
                                            {
                                                using (MemoryStream contentStream = new MemoryStream())
                                                {
                                                    // Add an empty file.
                                                    FileCreationInformation fileInfo = new FileCreationInformation();
                                                    fileInfo.ContentStream = contentStream;
                                                    fileInfo.Url = uniqueFileName;
                                                    fileInfo.Overwrite = true;
                                                    uploadFile = list.RootFolder.Files.Add(fileInfo);

                                                    // Start upload by uploading the first slice. 
                                                    using (MemoryStream s = new MemoryStream(buffer))
                                                    {
                                                        // Call the start upload method on the first slice
                                                        bytesUploaded = uploadFile.StartUpload(uploadId, s);
                                                        Context.ExecuteQuery();
                                                        // fileoffset is the pointer where the next slice will be added
                                                        fileoffset = bytesUploaded.Value;
                                                    }

                                                    // we can only start the upload once
                                                    first = false;
                                                }
                                            }
                                            else
                                            {
                                                // Get a reference to our file
                                                uploadFile = Context.Web.GetFileByServerRelativeUrl(uniqueFileName);

                                                if (last)
                                                {
                                                    // Is this the last slice of data?
                                                    using (MemoryStream s = new MemoryStream(lastBuffer))
                                                    {
                                                        // End sliced upload by calling FinishUpload
                                                        uploadFile = uploadFile.FinishUpload(uploadId, fileoffset, s);
                                                        Context.ExecuteQuery();
                                                    }
                                                }
                                                else
                                                {
                                                    using (MemoryStream s = new MemoryStream(buffer))
                                                    {
                                                        // Continue sliced upload
                                                        bytesUploaded = uploadFile.ContinueUpload(uploadId, fileoffset, s);
                                                        Context.ExecuteQuery();
                                                        // update fileoffset for the next slice
                                                        fileoffset = bytesUploaded.Value;
                                                    }
                                                }
                                            }

                                        } // while ((bytesRead = br.Read(buffer, 0, buffer.Length)) > 0)
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex);
                                    return null;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex);
                            return null;
                        }
                        return uploadFile;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            return null;
        }
    }
}

