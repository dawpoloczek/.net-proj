﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kancelaria365.Data;
using NLog;

namespace Kancelaria365.Web.Controllers
{
    public class StawkaPracownikaController:
        BaseCacheGenericController<StawkaPracownikaRepository, StawkaPracownika>
    {
        private new static Logger logger = LogManager.GetCurrentClassLogger();

        // GET: StawkaPracownika
        public ActionResult Index()
        {
            return View();
        }

        [SharePointContextFilter]
        [HttpPost]
        public HttpStatusCode Edit(StawkaPracownika model)
        {
            using (Context)
            {
                try
                {
                    StawkaPracownikaRepository.AddUpdateItem(model);
                    Refresh();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return HttpStatusCode.InternalServerError;
                }
            }
            return HttpStatusCode.OK;
        }

        [SharePointContextFilter]
        [HttpPost]
        public HttpStatusCode Create(StawkaPracownika model)
        {
            using (Context)
            {
                try
                {
                    StawkaPracownikaRepository.AddUpdateItem(model);
                    Refresh();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return HttpStatusCode.InternalServerError;
                }
            }
            return HttpStatusCode.OK;
        }
    }
}