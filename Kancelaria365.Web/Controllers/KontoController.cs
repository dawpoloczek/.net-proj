﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Kancelaria365.Data;
using NLog;

namespace Kancelaria365.Web.Controllers
{
    public class KontoController :
        BaseCacheGenericController<KontoRepository, Konto>
    {
        private new static Logger logger = LogManager.GetCurrentClassLogger();

        [SharePointContextFilter]
        public ActionResult Create(int kancelariaId)
        {
            Konto model = new Konto();
            return View(model);
        }

        [SharePointContextFilter]
        [HttpPost]
        public HttpStatusCode Create(Konto model)
        {
            using (Context)
            {
                try
                {
                    KontoRepository.AddUpdateItem(model);
                    Refresh();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return HttpStatusCode.InternalServerError;
                }
            }
            return HttpStatusCode.OK;
        }

        [SharePointContextFilter]
        [HttpPost]
        public HttpStatusCode Edit(Konto model)
        {
            using (Context)
            {
                try
                {
                    KontoRepository.AddUpdateItem(model);
                    Refresh();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return HttpStatusCode.InternalServerError;
                }
            }
            return HttpStatusCode.OK;
        }

        [SharePointContextFilter]
        public ActionResult Edit(int id)
        {
            Konto model = new Konto();
            using (Context)
            {
                KontoRepository kontoRepository =
                        new KontoRepository(Context);

                model = kontoRepository.GetEntityById(id);

            }
            return View(model);
        }
    }
}

