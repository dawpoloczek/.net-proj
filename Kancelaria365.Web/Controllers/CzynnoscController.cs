﻿using Kancelaria365.Common;
using Kancelaria365.Data;
using Kancelaria365.Web.Helpers;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kancelaria365.Web.Controllers
{
    public class CzynnoscController :
        BaseSessionGenericController<CzynnoscRepository, Czynnosc>
    {
        private new static Logger logger = LogManager.GetCurrentClassLogger();

        [SharePointContextFilter]
        public ActionResult Index()
        {
            CzynnoscListViewModel viewModel = null;
            using (Context)
            {
                viewModel = new CzynnoscListViewModel(Context);
            }
            return View(viewModel);
        }

        [SharePointContextFilter]
        public ActionResult Details(int id)
        {
            CzynnoscViewModel viewModel = null;
            using (Context)
            {
                Czynnosc czynnoscEntity = CzynnoscRepository.GetEntityById(id);
                viewModel = new CzynnoscViewModel(this.HttpContext, Context, czynnoscEntity, FormMode.Dispaly);
            }
            return View("Edit", viewModel);
        }

        [SharePointContextFilter]
        public ActionResult Create(int? sprawaId)
        {
            CzynnoscViewModel viewModel = null;

            using (Context)
            {
                viewModel = new CzynnoscViewModel(this.HttpContext, Context, sprawaId);
            }
            return View("Edit", viewModel);
        }

        [HttpPost]
        [SharePointContextFilter]
        public ActionResult Create(CzynnoscViewModel viewModel, FormCollection collection)
        {
            using (Context)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        CzynnoscRepository.AddUpdateItem(viewModel.Czynnosc);
                        this.ViewBag.SaveStatus = SaveStatus.OK;
                        this.ViewBag.Controller = this.RouteData.Values["controller"];
                        Refresh();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        this.ViewBag.SaveStatus = SaveStatus.Exception;
                    }
                }
                else
                {
                    this.ViewBag.SaveStatus = SaveStatus.ValidationError;
                }
                CzynnoscViewModel newViewModel =
                    new CzynnoscViewModel(this.HttpContext, Context, viewModel.Czynnosc, FormMode.New);
                return PartialView("EditPartial", newViewModel);
            }
        }

        [SharePointContextFilter]
        [HttpPost]
        public ActionResult Edit(CzynnoscViewModel viewModel, FormCollection collection)
        {
            using (Context)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        CzynnoscRepository.AddUpdateItem(viewModel.Czynnosc);
                        this.ViewBag.SaveStatus = SaveStatus.OK;
                        this.ViewBag.Controller = this.RouteData.Values["controller"];
                        Refresh();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        this.ViewBag.SaveStatus = SaveStatus.Exception;
                    }
                }
                else
                {
                    this.ViewBag.SaveStatus = SaveStatus.ValidationError;
                }
                CzynnoscViewModel newViewModel =
                    new CzynnoscViewModel(this.HttpContext, Context, viewModel.Czynnosc, FormMode.Edit);
                return PartialView("EditPartial", newViewModel);
            }
        }

        [SharePointContextFilter]
        public ActionResult Edit(CzynnoscViewModel viewModel, int id)
        {
            using (Context)
            {
                Czynnosc czynnosctEntity = CzynnoscRepository.GetEntityById(id);
                viewModel = new CzynnoscViewModel(this.HttpContext, Context, czynnosctEntity, FormMode.Edit);
            }
            return View(viewModel);
        }
    }
}
