﻿using Kancelaria365.ConnectedServices;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.OData.ProxyExtensions;

using System.Threading.Tasks;
using Kancelaria365.ConnectedServices.Services;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using System.Security.Claims;
using NLog;
using System.Configuration;
using Microsoft.Owin.Security.Cookies;

namespace Kancelaria365.Web.Controllers
{
    public class HomeController : BaseServiceController
    {
        private static new Logger logger = LogManager.GetCurrentClassLogger();
        
        [SharePointContextFilter]
        public ActionResult Index()
        {
            HomeListViewModel viewModel = null;
            using (Context)
            {
                viewModel = new HomeListViewModel(Context);
            }
            return View(viewModel);
        }
    }
}
