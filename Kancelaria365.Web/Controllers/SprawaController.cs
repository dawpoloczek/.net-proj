﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Kancelaria365.Data;
using NLog;

namespace Kancelaria365.Web.Controllers
{
    public class SprawaController :
        BaseCacheGenericController<SprawaRepository, Sprawa>
    {
        private new static Logger logger = LogManager.GetCurrentClassLogger();

        [SharePointContextFilter]
        public ActionResult Index()
        {
            SprawaListViewModel model = null;
            using (Context)
            {
                model = new SprawaListViewModel(Context);
            }
            return View(model);
        }

        [SharePointContextFilter]
        public ActionResult Details(int id)
        {
            SprawaViewModel viewModel = null;
            using (Context)
            {
                Sprawa sprawaEntity = SprawaRepository.GetEntityById(id);
                viewModel = new SprawaViewModel(this.HttpContext, Context, sprawaEntity, FormMode.Dispaly);
            }
            return View("Edit", viewModel);
        }

        [SharePointContextFilter]
        public ActionResult Create()
        {
            SprawaViewModel viewModel = null;

            using (Context)
            {
                viewModel = new SprawaViewModel(this.HttpContext,Context);
            }
            return View("Edit", viewModel);
        }

        [SharePointContextFilter]
        [HttpPost]
        public ActionResult Create(SprawaViewModel viewModel, FormCollection collection)
        {
            using (Context)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        SprawaRepository.AddUpdateItem(viewModel.Sprawa);
                        this.ViewBag.SaveStatus = SaveStatus.OK;
                        this.ViewBag.Controller = this.RouteData.Values["controller"];
                        Refresh();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        this.ViewBag.SaveStatus = SaveStatus.Exception;
                    }
                }
                else
                {
                    this.ViewBag.SaveStatus = SaveStatus.ValidationError;
                }
                SprawaViewModel newViewModel =
                    new SprawaViewModel(this.HttpContext, Context, viewModel.Sprawa, FormMode.New);
                return PartialView("EditPartial", newViewModel);
            }
        }

        [SharePointContextFilter]
        public ActionResult Edit(SprawaViewModel viewModel, int id)
        {
            using (Context)
            {
                Sprawa sprawaEntity = SprawaRepository.GetEntityById(id);
                viewModel = new SprawaViewModel(this.HttpContext, Context, sprawaEntity, FormMode.Edit);
            }
            return View(viewModel);
        }

        [SharePointContextFilter]
        [HttpPost]
        public ActionResult Edit(SprawaViewModel viewModel, FormCollection collection)
        {
            using (Context)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        SprawaRepository.AddUpdateItem(viewModel.Sprawa);
                        this.ViewBag.SaveStatus = SaveStatus.OK;
                        this.ViewBag.Controller = this.RouteData.Values["controller"];
                        Refresh();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        this.ViewBag.SaveStatus = SaveStatus.Exception;
                    }
                }
                else
                {
                    this.ViewBag.SaveStatus = SaveStatus.ValidationError;
                }
                SprawaViewModel newViewModel =
                    new SprawaViewModel(this.HttpContext, Context, viewModel.Sprawa, FormMode.Edit);
                return PartialView("EditPartial", newViewModel);
            }
        }
    }
}
