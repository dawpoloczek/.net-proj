﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kancelaria365.Data;
using NLog;

namespace Kancelaria365.Web.Controllers
{
    public class KancelariaController : BaseController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static string ClassName { get { return "KancelariaController"; } }

        [SharePointContextFilter]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SomeActionMethod(int? current, int? rowCount,
                Dictionary<string, string> sort, string searchPhrase = null)
        {
            List<Sprawa> model = new List<Sprawa>();
            model.Add(new Sprawa() { Title = "Test1" });
            model.Add(new Sprawa() { Title = "Test2" });

            //var spContext = SharePointContextProvider.Current.GetSharePointContext(HttpContext);

            //using (var clientContext = spContext.CreateUserClientContextForSPHost())
            //{
            //    SprawaRepository sprawaRepository =
            //            new SprawaRepository(clientContext);

            //    model = sprawaRepository.GetSprawaList(Sprawa.CamlQuery);
            //}

            return Json(model);
        }
    }
}