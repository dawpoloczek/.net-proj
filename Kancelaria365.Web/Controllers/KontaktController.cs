﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SP = Microsoft.SharePoint.Client;
using System.Linq.Dynamic;
using System.Web;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.IdentityModel.Claims;
using Microsoft.Identity.Client;
using Beta = GraphBeta;
using Kancelaria365.Data;
using Kancelaria365.Common;
using Kancelaria365.ConnectedServices;
using Kancelaria365.Common.Helpers;
using NLog;

namespace Kancelaria365.Web.Controllers
{
    public class KontaktController :
        BaseCacheGenericController<KontaktRepository, Kontakt>
    {
        private new static Logger logger = LogManager.GetCurrentClassLogger();

        [SharePointContextFilter]
        public ActionResult Index()
        {
            KontaktListViewModel model = null;
            using (Context)
            {
                model = new KontaktListViewModel(Context);
            }
            return View(model);
        }

        [SharePointContextFilter]
        public async Task<ActionResult> Details(int id)
        {
            KontaktViewModel viewModel = null;
            using (Context)
            {
                Kontakt kontaktEntity = KontaktRepository.GetEntityById(id);
                viewModel = new KontaktViewModel(this.HttpContext, Context, kontaktEntity, FormMode.Dispaly);
                await viewModel.Init();
            }
            return View("Edit", viewModel);
        }

        [SharePointContextFilter]
        public async Task<ActionResult> Create()
        {
            KontaktViewModel viewModel = null;

            using (Context)
            {
                viewModel = new KontaktViewModel(this.HttpContext, Context, FormMode.New);
                await viewModel.Init();
            }
            return View("Edit", viewModel);
        }

        [HttpPost]
        [SharePointContextFilter]
        public async Task<ActionResult> Create(KontaktViewModel viewModel, FormCollection collection)
        {
            using (Context)
            {
                viewModel = new KontaktViewModel(this.HttpContext, Context, viewModel.Kontakt, FormMode.New);
                await viewModel.Init(this.HttpContext);

                Beta.OrgContact contact = viewModel.KontaktyOrganizacji.FirstOrDefault(x => x.Id == viewModel.Kontakt.OutlookContact.Id);
                viewModel.Kontakt.OutlookContact.Value = contact == null ? string.Empty : contact.DisplayName;

                if (ModelState.IsValid)
                {
                    try
                    {
                        KontaktRepository.AddUpdateItem(viewModel.Kontakt);
                        this.ViewBag.SaveStatus = SaveStatus.OK;
                        this.ViewBag.Controller = this.RouteData.Values["controller"];
                        Refresh();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        this.ViewBag.SaveStatus = SaveStatus.Exception;
                    }
                }
                else
                {
                    this.ViewBag.SaveStatus = SaveStatus.ValidationError;
                }

                return PartialView("EditPartial", viewModel);
            }
        }

        [SharePointContextFilter]
        public async Task<ActionResult> Edit(KontaktViewModel viewModel, int id)
        {
            using (Context)
            {
                Kontakt kontaktEntity = KontaktRepository.GetEntityById(id);
                viewModel = new KontaktViewModel(this.HttpContext, Context, kontaktEntity, FormMode.Edit);
                await viewModel.Init();
            }
            return View(viewModel);
        }

        [SharePointContextFilter]
        [HttpPost]
        public async Task<ActionResult> Edit(KontaktViewModel viewModel, FormCollection collection)
        {
            using (Context)
            {
                viewModel =
                    new KontaktViewModel(this.HttpContext, Context, viewModel.Kontakt, FormMode.Edit);
                await viewModel.Init(this.HttpContext);

                Beta.OrgContact contact = viewModel.KontaktyOrganizacji.FirstOrDefault(x => x.Id == viewModel.Kontakt.OutlookContact.Id);
                viewModel.Kontakt.OutlookContact.Value = contact == null ? string.Empty : contact.DisplayName;

                if (ModelState.IsValid)
                {
                    try
                    { 
                        KontaktRepository.AddUpdateItem(viewModel.Kontakt);
                        this.ViewBag.SaveStatus = SaveStatus.OK;
                        this.ViewBag.Controller = this.RouteData.Values["controller"];
                        Refresh();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        this.ViewBag.SaveStatus = SaveStatus.Exception;
                    }
                }
                else
                {
                    this.ViewBag.SaveStatus = SaveStatus.ValidationError;
                }
                return PartialView("EditPartial", viewModel);
            }
        }

        [SharePointContextFilter]
        [HttpPost]
        public async Task<JsonResult> GetOutLookContact(string outlookContactID)
        {
            Beta.OrgContact returnValue = null;
            using (Context)
            {
                KontaktViewModel viewModel = new KontaktViewModel(this.HttpContext, Context);
                await viewModel.Init(this.HttpContext);
                returnValue = viewModel.KontaktyOrganizacji.FirstOrDefault(x => x.Id == outlookContactID);
            }
            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        [SharePointContextFilter]
        [HttpGet]
        public JsonResult GetDokumentyFinansoweForKontaktId(int kontaktId)
        {
            IEnumerable<DokumentFinansowy> returnList = new List<DokumentFinansowy>();

            using (Context)
            {
                returnList = DokumentyFinansoweRepository.GetEntityList(listName: GlobalNames.Lists.KontaktyDokumentyFinansowe);
            }

            var returnObject = new { records = returnList.ToList(), total = returnList.Count() };

            return Json(returnObject, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFile(int id)
        {
            byte[] contents = null;
            Kontakt entity = null;

            using (Context)
            {
                WordDocumentCreator wrodCreator = new WordDocumentCreator(Context);
                entity = KontaktRepository.GetEntityById(id);
                contents = wrodCreator.GetDocumentFromLibrary("/Dokumenty/Kontakt.docx", entity);
            }

            return File(contents, "application/octet-stream", entity.Title + ".docx");
        }
    }
}