﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kancelaria365.Data;
using NLog;

namespace Kancelaria365.Web.Controllers
{
    public class SlownikController : BaseController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        [SharePointContextFilter]
        public ActionResult Index()
        {
            var model = new List<SlownikItem>();

            using (Context)
            {
                if (Context != null)
                {
                    model = SlownikRepository.GetDictionaryItems();
                }
            }

            return View(model);
        }
    }
}
