﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Threading;
using Kancelaria365.Data;
using Kancelaria365.Web.Helpers;
using Microsoft.SharePoint.Client;
using Kancelaria365.Web.Extensions;
using NLog;

namespace Kancelaria365.Web.Controllers
{
    public class BaseGenericController<T, Z> :
        BaseController
        where T : GenericRepositoryBase<Z>, new()
        where Z : ICustomEntity, new()
    {
        public static new Logger logger = LogManager.GetCurrentClassLogger();

        private IEnumerable<Z> entityList = null;

        public void ClearEntityList(SearchParameters searchParameters)
        {
            if (searchParameters != null && searchParameters.ClearDataSource)
            {
                entityList = null;
                this.HttpContext.SetSessionValue<IEnumerable<Z>>("EntityList" + typeof(Z).Name, entityList);
            }
        }

        public virtual IEnumerable<Z> GetEntityList(SearchParameters searchParameters = null)
        {
            try
            {
                ClearEntityList(searchParameters);
                entityList = this.HttpContext.GetSessionValue<IEnumerable<Z>>("EntityList" + typeof(Z).Name);
                if (entityList == null || entityList.Count() == 0)
                {
                    T repository = new T() { Context = Context };
                    entityList = repository.GetEntityList(searchParameters);
                    this.HttpContext.SetSessionValue<IEnumerable<Z>>("EntityList" + typeof(Z).Name, entityList);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return entityList;
        }

        public BaseGenericController()
        {

        }

        [SharePointContextFilter]
        public virtual ActionResult Delete(int id)
        {
            try
            {
                using (Context)
                {
                    T repository = new T() { Context = Context };
                    repository.DeleteItem(id);
                    Refresh();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("DeleteError", ex.Message);
                logger.Error(ex);
                return View();
            }
        }

        public virtual void Refresh() { }

        [SharePointContextFilter]
        [HttpPost]
        public JsonResult GetEntities(SearchParameters searchParameters)
        {
            SearchResult<Z> returnObject = new SearchResult<Z>();
            IEnumerable<Z> returnList = new List<Z>();

            try
            {
                T repository = new T() { Context = Context };

                if (!string.IsNullOrEmpty(searchParameters.SearchtText) ||
                    searchParameters.SimpleSearch.Any(x => !string.IsNullOrEmpty(x.Value)))
                {
                    if (searchParameters.IsAdvancedSearch)
                    {
                        if (searchParameters.AdvancedSearch.Czynnosci ||
                            searchParameters.AdvancedSearch.Konta ||
                            searchParameters.AdvancedSearch.Kontakty ||
                            searchParameters.AdvancedSearch.Koszty ||
                            searchParameters.AdvancedSearch.Sprawy ||
                            searchParameters.AdvancedSearch.Zadania)
                        {
                            List<int> ids = repository.SearchEngine(searchParameters.SearchtText, searchParameters.AdvancedSearch);
                            returnList = GetEntityList(searchParameters).Where(x => ids.Contains(x.Id));
                        }
                        else
                        {
                            returnList = new List<Z>();
                        }
                    }
                    else
                    {
                        returnList = SimpleSearch(searchParameters);
                    }
                }
                else
                {
                    returnList = GetEntityList(searchParameters);
                }
                returnObject = GetSerachResult(returnList, searchParameters);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return Json(returnObject, JsonRequestBehavior.AllowGet);
        }

        [SharePointContextFilter]
        [HttpPost]
        public JsonResult GetRelatedEntities(SearchParameters searchParameters)
        {
            SearchResult<Z> returnObject = new SearchResult<Z>();
            try
            {
                T repository = new T() { Context = Context };
                IEnumerable<Z> returnList = GetEntityList();

                if (searchParameters.SimpleSearch == null)
                {
                    searchParameters.SimpleSearch = new SimpleSearch[0];
                }

                string whereQuery = string.Empty;
                int i = 0;
                foreach (var x in searchParameters.SimpleSearch)
                {
                    if (!string.IsNullOrEmpty(x.Value))
                    {
                        whereQuery = whereQuery + "(" + x.PropertyName + "!= null and " + x.PropertyName + ".ToString() == @" + i + ") AND ";
                        i++;
                    }
                }
                if (!string.IsNullOrEmpty(whereQuery))
                {
                    string[] objets = searchParameters.SimpleSearch.Where(x => !string.IsNullOrEmpty(x.Value)).Select(x => x.Value.ToUpper()).ToArray();
                    whereQuery = whereQuery.Remove(whereQuery.Length - 5);
                    returnList = returnList.Where(whereQuery, objets);
                }
                returnObject = GetSerachResult(returnList, searchParameters);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return Json(returnObject, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<Z> SimpleSearch(SearchParameters searchParameters)
        {
            IEnumerable<Z> returnList = new List<Z>();
            try
            {
                returnList = GetEntityList(searchParameters);

                if (searchParameters.SimpleSearch == null)
                {
                    searchParameters.SimpleSearch = new SimpleSearch[0];
                }

                string whereQuery = string.Empty;
                int i = 0;
                foreach (var x in searchParameters.SimpleSearch)
                {
                    if (!string.IsNullOrEmpty(x.Value))
                    {
                        whereQuery = whereQuery + "(" + x.PropertyName + "!= null and " + x.PropertyName + ".ToUpper().Contains(@" + i + ")) AND ";
                        i++;
                    }
                }
                if (!string.IsNullOrEmpty(whereQuery))
                {
                    string[] objets = searchParameters.SimpleSearch.Where(x => !string.IsNullOrEmpty(x.Value)).Select(x => x.Value.ToUpper()).ToArray();
                    whereQuery = whereQuery.Remove(whereQuery.Length - 5);
                    returnList = returnList.Where(whereQuery, objets);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnList;
        }

        private SearchResult<Z> GetSerachResult(IEnumerable<Z> returnList, SearchParameters searchParameters)
        {
            SearchResult<Z> returnObject = new SearchResult<Z>();
            try
            {
                if (!string.IsNullOrEmpty(searchParameters.SortBy) && !string.IsNullOrEmpty(searchParameters.Direction))
                {
                    returnList = returnList.OrderBy(searchParameters.SortBy + " " + searchParameters.Direction);
                }

                Z[] record = returnList.Skip((searchParameters.Page - 1) * searchParameters.Limit).Take(searchParameters.Limit).ToArray();

                returnObject = new SearchResult<Z>()
                {
                    Records = record,
                    Total = returnList.Count()
                };
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnObject;
        }
    }
}