﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Threading;
using Kancelaria365.Data;
using Kancelaria365.Web.Helpers;
using Microsoft.SharePoint.Client;
using Kancelaria365.Web.Extensions;
using NLog;

namespace Kancelaria365.Web.Controllers
{
    public class BaseSessionGenericController<T, Z> :
        BaseGenericController<T, Z>
        where T : GenericRepositoryBase<Z>, new()
        where Z : ICustomEntity, new()
    {
        public BaseSessionGenericController()
        {
        }

        public override void Refresh()
        {
            this.HttpContext.SetSessionValue<IEnumerable<Z>>("EntityList" + typeof(Z).Name, null);
        }
    }
}