﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Threading;
using Kancelaria365.Data;
using Kancelaria365.Web.Helpers;

namespace Kancelaria365.Web.Controllers
{
    public class BaseCacheGenericController<T, Z> :
        BaseGenericController<T, Z>
        where T : GenericRepositoryBase<Z>, new()
        where Z : ICustomEntity, new()
    {
        private MemoryCacheHelper<T, Z> cacheHelper;
        public MemoryCacheHelper<T, Z> CacheHelper
        {
            get
            {
                if(cacheHelper == null)
                {
                    cacheHelper = new MemoryCacheHelper<T, Z>(Context);
                }
                return cacheHelper;
            }
            set { cacheHelper = value; }
        }

        public BaseCacheGenericController()
        {

        }
        public override IEnumerable<Z> GetEntityList(SearchParameters searchParameters = null)
        {
            return CacheHelper.EntityList;
        }

        public override void Refresh()
        {
            CacheHelper.RefreshCache();
        }
    }
}