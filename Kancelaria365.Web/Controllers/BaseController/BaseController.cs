﻿using Kancelaria365.Common;
using Kancelaria365.ConnectedServices;
using Kancelaria365.Data;
using Kancelaria365.Web.Helpers;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.SharePoint.Client.Utilities;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SP = Microsoft.SharePoint.Client;

namespace Kancelaria365.Web.Controllers
{
    public class BaseController : Controller
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();
        public bool UseTempData { get; set; }
        public BaseController()
        {
            UseTempData = false;
        }

        protected override RedirectToRouteResult RedirectToAction(string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            if (routeValues == null)
            {
                routeValues = new RouteValueDictionary();
            }
            if (!routeValues.ContainsKey("SPHostUrl"))
            {
                routeValues.Add("SPHostUrl", SharePointContext.GetSPHostAbsoluteUri(HttpContext.Request));
            }
            return base.RedirectToAction(actionName, controllerName, routeValues);
        }

        SP.ClientContext context = null;
        public SP.ClientContext Context
        {
            get
            {
                if (context == null)
                {
                    //var spContext = SharePointContextProvider.Current.GetSharePointContext(HttpContext);
                    //context = spContext.CreateUserClientContextForSPHost();
                    context = SharePointContext.GetContext(this.HttpContext.ApplicationInstance.Context);
                }
                return context;
            }
            set
            {
                context = value;
            }
        }

        [SharePointContextFilter]
        [HttpPost]
        public JsonResult SearchPeople(string searchText)
        {
            List<UserDictionaryItem> people = new List<UserDictionaryItem>();
            try
            {
                using (Context)
                {
                    SP.Web web = Context.Web;
                    Context.Load(web);

                    var items = Utility.SearchPrincipals(Context, web, searchText,
                           PrincipalType.User, PrincipalSource.UserInfoList, null, 10);

                    Context.ExecuteQuery();
                    items.ToList().ForEach(x => people.Add(new UserDictionaryItem(x.PrincipalId, x.DisplayName, x.LoginName)));
                }
            }
            catch
            { }
            return Json(people);
        }

        private SprawaRepository sprawaRepository;
        public SprawaRepository SprawaRepository
        {
            get
            {
                if (sprawaRepository == null)
                {
                    sprawaRepository = new SprawaRepository(Context);
                }
                return sprawaRepository;

            }
        }

        private KontaktRepository kontaktRepository;
        public KontaktRepository KontaktRepository
        {
            get
            {
                if (kontaktRepository == null)
                {
                    kontaktRepository = new KontaktRepository(Context);
                }
                return kontaktRepository;

            }
        }

        private KontoRepository kontoRepository;
        public KontoRepository KontoRepository
        {
            get
            {
                if (kontoRepository == null)
                {
                    kontoRepository = new KontoRepository(Context);
                }
                return kontoRepository;

            }
        }

        private CzynnoscRepository czynnoscRepository;
        public CzynnoscRepository CzynnoscRepository
        {
            get
            {
                if (czynnoscRepository == null)
                {
                    czynnoscRepository = new CzynnoscRepository(Context);
                }
                return czynnoscRepository;

            }
        }

        private KosztRepository kosztRepository;
        public KosztRepository KosztRepository
        {
            get
            {
                if (kosztRepository == null)
                {
                    kosztRepository = new KosztRepository(Context);
                }
                return kosztRepository;

            }
        }

        private SlownikRepository slownikRepository;
        public SlownikRepository SlownikRepository
        {
            get
            {
                if (slownikRepository == null)
                {
                    slownikRepository = new SlownikRepository(Context);
                }
                return slownikRepository;

            }
        }

        private UserRepository userRepository;
        public UserRepository UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(Context);
                }
                return userRepository;
            }
        }

        private StawkaPracownikaRepository stawkaPracownikaRepository;
        public StawkaPracownikaRepository StawkaPracownikaRepository
        {
            get
            {
                if (stawkaPracownikaRepository == null)
                {
                    stawkaPracownikaRepository = new StawkaPracownikaRepository(Context);
                }
                return stawkaPracownikaRepository;
            }
        }

        private KategoriaPracownikaRepository kategoriaPracownikaRepository;
        public KategoriaPracownikaRepository KategoriaPracownikaRepository
        {
            get
            {
                if (kategoriaPracownikaRepository == null)
                {
                    kategoriaPracownikaRepository = new KategoriaPracownikaRepository(Context);
                }
                return kategoriaPracownikaRepository;
            }
        }

        private DokumentyFinansoweRepository dokumentyFinansoweRepository;
        public DokumentyFinansoweRepository DokumentyFinansoweRepository
        {
            get
            {
                if (dokumentyFinansoweRepository == null)
                {
                    dokumentyFinansoweRepository = new DokumentyFinansoweRepository(Context);
                }
                return dokumentyFinansoweRepository;
            }
        }

        private OsobaKontaktowaRepository osobaKontaktowaRepository;
        public OsobaKontaktowaRepository OsobaKontaktowaRepository
        {
            get
            {
                if (osobaKontaktowaRepository == null)
                {
                    osobaKontaktowaRepository = new OsobaKontaktowaRepository(Context);
                }
                return osobaKontaktowaRepository;
            }
        }

        public Dictionary<string, string> GetFieldsInView(string listTitle, string viewName)
        {
            SP.List list = Context.Web.Lists.GetByTitle(listTitle);
            SP.View view = list.Views.GetByTitle(viewName);
            Context.Load(view.ViewFields);
            Context.Load(list.Fields);
            Context.ExecuteQuery();

            foreach (string internalName in view.ViewFields)
            {
                Context.Load(list.Fields.GetByInternalNameOrTitle(internalName));
            }
            Context.ExecuteQuery();

            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (string internalName in view.ViewFields)
            {
                string name = internalName;

                SP.Field filed = list.Fields.GetByInternalNameOrTitle(internalName);
                string title = list.Fields.GetByInternalNameOrTitle(internalName).Title;

                dic.Add(name, title);
            }
            return dic;
        }

        public List<string> GetEditableColumns(string listTitle, string viewName, Dictionary<string, string> dic)
        {
            if (listTitle == GlobalNames.Lists.KontaktyDokumentyFinansowe)
            {
                if (viewName == GlobalNames.Lists.Views.KontaktyDokumentyFinansowe.KontaktyDokumentyFinansoweGrid)
                {

                    return (new List<string>(dic.Keys)).Where(x => x == "Title").ToList();
                }
            }
            return null;
        }

        public Dictionary<string, string> GetNumberFieldProperties(string listTitle, string fieldName)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            SP.List list = Context.Web.Lists.GetByTitle(listTitle);
            SP.Field field = list.Fields.GetByInternalNameOrTitle(fieldName);
            SP.FieldNumber fieldNumber = context.CastTo<SP.FieldNumber>(field);
            Context.Load(fieldNumber);
            Context.ExecuteQuery();

            dic.Add("MinVal", fieldNumber.MinimumValue.ToString());
            dic.Add("MaxVal", fieldNumber.MaximumValue.ToString());

            return dic;
        }

        private string GetContentType(ImageFormat fmt)
        {
            string resturnValue = string.Empty;


            if (fmt.Guid == ImageFormat.Bmp.Guid)
                resturnValue = "image/x-ms-bmp";
            else if (fmt.Guid == ImageFormat.Jpeg.Guid)
                resturnValue = "image/jpeg";
            else if (fmt.Guid == ImageFormat.Gif.Guid)
                resturnValue = "image/gif";
            else if (fmt.Guid == ImageFormat.Png.Guid)
                resturnValue = "image/png";
            else
            {
                resturnValue = "application/octet-stream";
            }

            return resturnValue;
        }

        public async Task<ActionResult> GetCurrentUserPhoto()
        {
            CustomGraphService service = new CustomGraphService(this.HttpContext);
            try
            {
                var memStream = new MemoryStream();
                using (Stream stream = await service.GetCurrentUserPhoto())
                {

                    await stream.CopyToAsync(memStream);
                    memStream.Seek(0, SeekOrigin.Begin);
                }
                Image image = Image.FromStream(memStream);
                return File(memStream.ToArray(), GetContentType(image.RawFormat));
            }
            catch (Exception ex)
            {
                return File("/Images/UserDefault.png", "image/png");
            }
        }

        [SharePointContextFilter]
        public string GetCurrentUserDisplayName()
        {
            string returnString = String.Empty;
            try
            {
                using (Context)
                {
                    Context.Load(Context.Web.CurrentUser);
                    Context.ExecuteQuery();
                    returnString = Context.Web.CurrentUser.Title;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnString;
        }

        [SharePointContextFilter]
        public async Task<int> GetUnreadMessagesCount()
        {
            int count = 0;
            try
            {
                CustomGraphService service = new CustomGraphService(this.HttpContext);
                count = await service.GetUnreadMessages();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return count;
        }

        public void SignIn()
        {
            if (!Request.IsAuthenticated)
            {
                string url = Request.QueryString["CurrentUrl"];

                // Signal OWIN to send an authorization request to Azure
                HttpContext.GetOwinContext().Authentication.Challenge(
                    new AuthenticationProperties { RedirectUri = url },
                    OpenIdConnectAuthenticationDefaults.AuthenticationType);
            }
        }

        public void SignOut()
        {
            if (Request.IsAuthenticated)
            {
                string userId = ClaimsPrincipal.Current.FindFirst(Microsoft.IdentityModel.Claims.ClaimTypes.NameIdentifier).Value;

                if (!string.IsNullOrEmpty(userId))
                {
                    string appId = ConfigurationManager.AppSettings["ida:AppId"];
                    // Get the user's token cache and clear it
                    SessionTokenCache tokenCache = new SessionTokenCache(userId, HttpContext);
                    tokenCache.Clear(appId);
                }
            }
            string url = Request.QueryString["CurrentUrl"];
            // Send an OpenID Connect sign-out request. 
            HttpContext.GetOwinContext().Authentication.SignOut(
                CookieAuthenticationDefaults.AuthenticationType);
            Response.Redirect(url);
        }

        [SharePointContextFilter]
        public int GetTotalClientsCount()
        {
            int returnValue = 0;
            try
            {
                using (Context)
                {
                    returnValue = KontaktRepository.GetEntitiesCount();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnValue;
        }

        [SharePointContextFilter]
        public int GetTotalCasesCount()
        {
            int returnValue = 0;
            try
            {
                using (Context)
                {
                    returnValue = SprawaRepository.GetEntitiesCount();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnValue;
        }

        [SharePointContextFilter]
        public double GetSumaKosztow()
        {
            double returnValue = 0;
            try
            {
                using (Context)
                {
                    returnValue = KosztRepository.GetColumnSum("KwotaBrutto");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnValue;
        }

        [SharePointContextFilter]
        public double GetSumaCzasuCzynnosci()
        {
            double returnValue = 0;
            try
            {
                using (Context)
                {
                    returnValue = CzynnoscRepository.GetColumnSum("CzasZarejestrowany");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return returnValue;
        }

        [HttpPost]
        [SharePointContextFilter]
        public JsonResult GetSprawy(int kontaktId)
        {
            List<DictionaryItem> items = new List<DictionaryItem>();
            using (Context)
            {
                MemoryCacheHelper<SprawaRepository, Sprawa>
                    helper = new MemoryCacheHelper<SprawaRepository, Sprawa>(Context);


                IEnumerable<Sprawa> resultList = new List<Sprawa>();
                if (kontaktId != 0)
                {
                    resultList = helper.EntityList
                                    .Where(x => x.Kontakt.Id == kontaktId);
                }
                resultList.ToList().ForEach(x => items.Add(x.GetDictionaryItem()));

            }
            return Json(new { items });
        }

        [HttpPost]
        [SharePointContextFilter]
        public JsonResult GetKontakt(int sprawaId)
        {
            List<DictionaryItem> items = new List<DictionaryItem>();
            using (Context)
            {
                if (sprawaId != 0)
                {
                    MemoryCacheHelper<SprawaRepository, Sprawa>
                        helper = new MemoryCacheHelper<SprawaRepository, Sprawa>(Context);

                    IEnumerable<Sprawa> resultList = new List<Sprawa>();
                    helper.EntityList.Where(x => x.Id == sprawaId).ToList().ForEach(x => items.Add(x.Kontakt));
                }
                else
                {
                    MemoryCacheHelper<KontaktRepository, Kontakt>
                        helper = new MemoryCacheHelper<KontaktRepository, Kontakt>(Context);
                    helper.EntityList.ToList().ForEach(x => items.Add(x.GetDictionaryItem()));
                }
            }
            return Json(new { items });
        }
    }
}