﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Kancelaria365.Data;
using NLog;

namespace Kancelaria365.Web.Controllers
{
    public class OsobaKontaktowaController :
        BaseCacheGenericController<OsobaKontaktowaRepository, OsobaKontaktowa>
    {
        private new static Logger logger = LogManager.GetCurrentClassLogger();

        [SharePointContextFilter]
        [HttpPost]
        public HttpStatusCode Create(OsobaKontaktowa model)
        {
            using (Context)
            {
                try
                {
                    OsobaKontaktowaRepository.AddUpdateItem(model);
                    Refresh();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return HttpStatusCode.InternalServerError;
                }
            }
            return HttpStatusCode.OK;
        }

        [SharePointContextFilter]
        [HttpPost]
        public HttpStatusCode Edit(OsobaKontaktowa model)
        {
            using (Context)
            {
                try
                {
                    OsobaKontaktowaRepository.AddUpdateItem(model);
                    Refresh();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return HttpStatusCode.InternalServerError;
                }
            }
            return HttpStatusCode.OK;
        }
    }
}

