﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Kancelaria365.Data;
using Kancelaria365.Common;
using NLog;
using System.Threading.Tasks;
using System.Web.Routing;

namespace Kancelaria365.Web.Controllers
{
    public class KosztController :
        BaseSessionGenericController<KosztRepository, Koszt>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [SharePointContextFilter]
        public ActionResult Index()
        {
            KosztListViewModel viewModel = null;
            using (Context)
            {
                viewModel = new KosztListViewModel(Context);
            }
            return View(viewModel);
        }

        [SharePointContextFilter]
        public ActionResult Details(int id)
        {
            KosztViewModel viewModel = null;
            using (Context)
            {
                Koszt koszt = KosztRepository.GetEntityById(id);
                viewModel = new KosztViewModel(this.HttpContext, Context, koszt, FormMode.Dispaly);
            }
            return View("Edit", viewModel);
        }

        [HttpPost]
        [SharePointContextFilter]
        public ActionResult Create(KosztViewModel viewModel, FormCollection collection)
        {
            using (Context)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        KosztRepository.AddUpdateItem(viewModel.Koszt);
                        this.ViewBag.SaveStatus = SaveStatus.OK;
                        this.ViewBag.Controller = this.RouteData.Values["controller"];
                        Refresh();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        this.ViewBag.SaveStatus = SaveStatus.Exception;
                    }
                }
                else
                {
                    this.ViewBag.SaveStatus = SaveStatus.ValidationError;
                }
                KosztViewModel newViewModel =
                    new KosztViewModel(this.HttpContext, Context, viewModel.Koszt, FormMode.New);
                return PartialView("EditPartial", newViewModel);
            }
        }

        [SharePointContextFilter]
        public ActionResult Create(int? sprawaId)
        {
            KosztViewModel viewModel = null;

            using (Context)
            {
                viewModel = new KosztViewModel(this.HttpContext, Context, sprawaId);
            }
            return View("Edit", viewModel);
        }

        [SharePointContextFilter]
        public ActionResult Edit(KosztViewModel viewModel, int id)
        {
            using (Context)
            {
                Koszt kosztEntity = KosztRepository.GetEntityById(id);
                viewModel = new KosztViewModel(this.HttpContext, Context, kosztEntity, FormMode.Edit);
            }
            return View(viewModel);
        }

        [SharePointContextFilter]
        [HttpPost]
        public ActionResult Edit(KosztViewModel viewModel, FormCollection collection)
        {
            using (Context)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        KosztRepository.AddUpdateItem(viewModel.Koszt);
                        this.ViewBag.SaveStatus = SaveStatus.OK;
                        this.ViewBag.Controller = this.RouteData.Values["controller"];
                        Refresh();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        this.ViewBag.SaveStatus = SaveStatus.Exception;
                    }
                }
                else
                {
                    this.ViewBag.SaveStatus = SaveStatus.ValidationError;
                }
                KosztViewModel newViewModel =
                    new KosztViewModel(this.HttpContext, Context, viewModel.Koszt, FormMode.Edit);
                return PartialView("EditPartial", newViewModel);
            }
        }
    }
}
