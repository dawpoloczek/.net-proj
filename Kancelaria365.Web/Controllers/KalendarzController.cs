﻿using Kancelaria365.ConnectedServices.Services;
using Microsoft.OData.ProxyExtensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Kancelaria365.Data;
using NLog;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Kancelaria365.ConnectedServices;
using System.Configuration;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Graph;

namespace Kancelaria365.Web.Controllers
{
    public class KalendarzController : BaseServiceController
    {
        private new static Logger logger = LogManager.GetCurrentClassLogger();
        [SharePointContextFilter]
        public async Task<ActionResult> Index()
        {
            KalendarzViewModel model = new KalendarzViewModel(this.HttpContext);
            await model.Init();

            return View(model);
        }

        [HttpGet]
        public async Task<JsonResult> GetCalendar()
        {
            CalendarService service = new CalendarService(this.HttpContext);
            List<Calendar> data = await service.GetCallendarTypes();

            List<Kalendarz> returnData = new List<Kalendarz>();
            data.ToList().ForEach(x => returnData.Add(new Kalendarz(x)));

            return Json(returnData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> GetCalendarEvents(DateTime start, DateTime end, string id)
        {
            CalendarService service = new CalendarService(this.HttpContext);
            List<Event> data = await service.GetCalendarEventsAsync(start, end, id);
            
            var eventList = from eventItem in data
                            select new
                            {
                                title = eventItem.Subject,
                                start = eventItem.Start.DateTime,
                                location = eventItem.Location,
                                end = eventItem.End.DateTime,
                                allDay = eventItem.IsAllDay,
                                webLink = eventItem.WebLink,
                                eventColor = GetRandomHexColor(),
                                isCanceled = eventItem.IsCancelled.HasValue ? eventItem.IsCancelled : false
                            };

            return Json(eventList, JsonRequestBehavior.AllowGet);
        }

        public string GetRandomHexColor()
        {
            var result = "#" + Guid.NewGuid().ToString().Substring(0, 6);
            return result;
        }

        [HttpGet]
        public async Task<JsonResult> GetDiaryEvents(DateTime start, DateTime end, string id)
        {
            CalendarService service = new CalendarService(this.HttpContext);
            List<Event> data = await service.GetCalendarEventsAsync(start, end, id);

            var eventList = from eventItem in data
                            select new
                            {
                                title = eventItem.Subject,
                                start = eventItem.Start,
                                end = eventItem.End,
                                allDay = eventItem.IsAllDay,
                                eventColor = GetRandomHexColor(),
                                webLink = eventItem.WebLink
                            };

            return Json(eventList, JsonRequestBehavior.AllowGet);
        }
    }
}
