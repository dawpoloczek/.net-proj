﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kancelaria365.Data;
using Kancelaria365.Web.Controllers;
using Microsoft.SharePoint.Client;
using Kancelaria365.Web.Extensions;
using Kancelaria365.Common;
using Kancelaria365.Web.Helpers;

namespace Kancelaria365.Web
{
    public class CzynnoscViewModel
            : BaseViewModel
    {
        HttpContextBase HttpContextBase;
        public bool Enabled { get; set; }

        public List<DictionaryItem> Sprawy
        {
            get
            {
                List<DictionaryItem> items = new List<DictionaryItem>();
                if (Czynnosc.Firma.Id != 0)
                {
                    MemoryCacheHelper<SprawaRepository, Sprawa>
                        helper = new MemoryCacheHelper<SprawaRepository, Sprawa>(ClientContext);

                    List<Sprawa> sprawy = helper.EntityList.Where(x => x.Kontakt.Id == Czynnosc.Firma.Id).ToList();

                    sprawy.ForEach(x => items.Add(x.GetDictionaryItem()));
                    items.Insert(0, new DictionaryItem(0, "--Wybierz--"));
                }
                return items;
            }
        }
        public List<DictionaryItem> Kontakty
        {
            get
            {
                List<DictionaryItem> kontakty = HttpContextBase.GetSessionValue<List<DictionaryItem>>("Kontakty");
                if (kontakty == null)
                {
                    kontakty = SlownikRepository.GetEntityList(GlobalNames.Lists.Kontakty, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("Kontakty", kontakty);
                }
                return kontakty;
            }
        }
        public Czynnosc Czynnosc { get; set; }

        private void SetEnabled(FormMode mode)
        {
            if (mode == FormMode.Dispaly)
            {
                Enabled = false;
            }
            else
            {
                Enabled = true;
            }
        }

        public CzynnoscViewModel()
        {
            Enabled = true;
        }

        public CzynnoscViewModel(HttpContextBase httpContextBase, ClientContext ctx, int? sprawaId, FormMode mode = FormMode.New)
            :base(ctx)
        {
            this.HttpContextBase = httpContextBase;
            this.FormMode = mode;
            this.Czynnosc = new Czynnosc();

            if(sprawaId.HasValue)
            {
                MemoryCacheHelper<SprawaRepository, Sprawa>
                        helper = new MemoryCacheHelper<SprawaRepository, Sprawa>(ClientContext);

              Sprawa sprawa=  helper.EntityList.Where(x => x.Id == sprawaId.Value).FirstOrDefault();

                this.Czynnosc.Sprawa = sprawa.GetDictionaryItem();
                this.Czynnosc.Firma = sprawa.Kontakt;
            }
            this.Czynnosc.DataWprowadzenia = DateTime.Now.Date;
            this.Czynnosc.Wprowadzajacy =
                new UserDictionaryItem(
                    UserRepository.CurrentUser.Id,
                    UserRepository.CurrentUser.Title,
                    UserRepository.CurrentUser.LoginName);
            this.Czynnosc.Wykonawca =
                new UserDictionaryItem(
                    UserRepository.CurrentUser.Id,
                    UserRepository.CurrentUser.Title,
                    UserRepository.CurrentUser.LoginName);
            SetEnabled(mode);
        }

        public CzynnoscViewModel(HttpContextBase httpContextBase,
            ClientContext ctx, Czynnosc czynnosc, FormMode mode)
            : base(ctx)
        {
            this.HttpContextBase = httpContextBase;
            this.FormMode = mode;
            this.Czynnosc = czynnosc;

            SetEnabled(mode);
        }
    }
}