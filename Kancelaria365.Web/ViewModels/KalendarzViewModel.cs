﻿using Kancelaria365.ConnectedServices.Services;
using Kancelaria365.Data;
using Microsoft.Graph;
using Microsoft.OData.ProxyExtensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Kancelaria365.Web
{
    public class KalendarzViewModel
    {
        HttpContextBase httpContext;
        CalendarService service;

        public string SelectedKalendarz { get; set; }

        public List<Kalendarz> KalendarzeList { get; set; }

        public KalendarzViewModel(HttpContextBase httpContext)
        {
            this.httpContext = httpContext;
            this.service = new CalendarService(httpContext);
        }

        public async Task<List<Kalendarz>> GetKalendarzeList()
        {
            List<Calendar> data = await service.GetCallendarTypes();
            List<Kalendarz> returnData = new List<Kalendarz>();
            data.ForEach(x => returnData.Add(new Kalendarz(x)));

            return returnData;
        }

        public async Task Init()
        {
            this.KalendarzeList = await GetKalendarzeList();
        }
    }
}

