﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kancelaria365.Data;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Web
{
    public class CustomGridViewModel
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public int DetailsElementId;
        public string DetailsElementName;
        public bool ResizableColumns;
        public string UILibrary;
        public string AjaxGridDataSource;
        public string AjaxAddItemUrl;
        public string AjaxEditItemUrl;
        public string AjaxRemoveItemUrl;
        public List<CostomGridFieldMetadata> CostomGridFieldsMetadataList;
        public bool IsDetailsButton;
        public string DetailsButtonHeaderCssClass;
        public string DetailsButtonCssClass;
        public object[] AdvancedSearchObjectList;
        public string AdvancedDivId;
        public string NoDataContent;
        public string CustomDataSourceName;
        public string DownloadLinkObjectProperty;

        public bool IsSearching;
        public bool UseCustomDataSource;
        public bool IsAutoload;
        public bool IsPager;
        public bool IsInserting;
        public bool IsPagerEnable;
        public bool IsEditing;
        public int PageSize;
        public bool IsFileLoader;
        public bool IsDownloadFile;

        public string GuidObject;

        public List<int> PagerSizes;
        public string GridItemDetailsControlerActionUrl;
        public string GridObjectName;

        #region Default values
        private static List<int> DefaultPagerSizes = new List<int>() { 10, 20 };
        private static int DefaultPageSize = 10;
        private static string DefaultUILibrary = "bootstrap";
        private static string DefaultDetailsButtonHeaderCssClass = "cust-grid-header";
        private static string DefaultDetailsButtonCssClass = "cust-grid-cell";
        #endregion

        private CustomGridViewModel()
        { }

        #region Kontakty
        public static CustomGridViewModel GetKontaktyGridViewModel(ClientContext ctx, string gridObjectName)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.AjaxGridDataSource = "../Kontakt/GetEntities";
                viewModel.GridItemDetailsControlerActionUrl = "/Kontakt/Details/";
                viewModel.AdvancedDivId = "kontaktyAdvancedSearchDiv";
                viewModel.AdvancedSearchObjectList = new object[] {
                    new { Label = "Czynności", Name = "Czynnosci" },
                    new { Label = "Sprawy", Name = "Sprawy" },
                    new { Label = "Zadania", Name = "Zadania" },
                    new { Label = "Koszty", Name = "Koszty" },
                };
                viewModel.GridObjectName = gridObjectName;
                viewModel.IsSearching = true;
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.Kontakty, GlobalNames.Lists.Views.Kontakty.WszystkieElementy);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }

        public static CustomGridViewModel GetKontaktSprawyGridViewModel(ClientContext ctx, int kontaktId, FormMode formMode)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.DetailsElementId = kontaktId;
                viewModel.DetailsElementName = "Kontakt.Id";
                viewModel.AjaxGridDataSource = "/../Sprawa/GetRelatedEntities";
                viewModel.GridItemDetailsControlerActionUrl = "/Sprawa/Details/";
                viewModel.GridObjectName = "sprawyGrid_" + DateTime.Now.Millisecond;
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.Sprawy, GlobalNames.Lists.Views.Sprawy.KontaktSprawy);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }

        public static CustomGridViewModel GetKontaktDokumentyFinansoweGridViewModel(ClientContext ctx, int kontaktId, FormMode formMode)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.DetailsElementId = kontaktId;
                viewModel.DetailsElementName = "Kontakt.Id";
                viewModel.IsPager = false;
                viewModel.IsDetailsButton = false;
                viewModel.IsDownloadFile = true;
                viewModel.DownloadLinkObjectProperty = "DocumentURL";
                viewModel.IsFileLoader = formMode == FormMode.Dispaly ? false : true;
                viewModel.AjaxGridDataSource = "/../DokumentFinansowy/GetRelatedEntities";
                viewModel.AjaxAddItemUrl = "/../DokumentFinansowy/Create";
                viewModel.GridObjectName = "dokumentyFinansoweGrid_" + DateTime.Now.Millisecond;
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.KontaktyDokumentyFinansowe, GlobalNames.Lists.Views.KontaktyDokumentyFinansowe.KontaktyDokumentyFinansoweGrid);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }

        public static CustomGridViewModel GetKontaktStawkiPracownikowGridViewModel(ClientContext ctx, int kontaktId, FormMode formMode)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.DetailsElementId = kontaktId;
                viewModel.DetailsElementName = "Kontakt.Id";
                viewModel.IsPager = false;
                viewModel.IsDetailsButton = false;
                viewModel.IsEditing = formMode == FormMode.Dispaly ? false : true;
                viewModel.IsInserting = formMode == FormMode.Dispaly ? false : true;
                viewModel.AjaxGridDataSource = "/../StawkaPracownika/GetRelatedEntities";
                viewModel.AjaxAddItemUrl = "/../StawkaPracownika/Create";
                viewModel.AjaxEditItemUrl = "/../StawkaPracownika/Edit";
                viewModel.AjaxRemoveItemUrl = "/../StawkaPracownika/Delete?id=";
                viewModel.GridObjectName = "stawkiPracownikowGrid_" + DateTime.Now.Millisecond;
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.StawkiPracownikow, GlobalNames.Lists.Views.StawkiPracownikow.KontaktyStawkiPracownikow);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }

        public static CustomGridViewModel GetKontaktKontaBankoweGridViewModel(ClientContext ctx, int kontaktId, FormMode formMode)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.IsDetailsButton = false;
                viewModel.IsEditing = formMode == FormMode.Dispaly ? false : true;
                viewModel.IsInserting = formMode == FormMode.Dispaly ? false : true;
                viewModel.AjaxGridDataSource = "/../Konto/GetRelatedEntities";
                viewModel.GridObjectName = "kontaBankoweGrid_" + DateTime.Now.Millisecond;
                viewModel.AjaxAddItemUrl = "/../Konto/Create";
                viewModel.AjaxEditItemUrl = "/../Konto/Edit";
                viewModel.AjaxRemoveItemUrl = "/../Konto/Delete?id=";
                viewModel.DetailsElementId = kontaktId;
                viewModel.DetailsElementName = "Kontakt.Id";
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.Konta, GlobalNames.Lists.Views.Konta.KontaktyKontaBankoweGrid);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }

        public static CustomGridViewModel GetKontaktOsobyKontaktoweGridViewModel(ClientContext ctx, int kontaktId, FormMode formMode)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.IsDetailsButton = false;
                viewModel.IsEditing = formMode == FormMode.Dispaly ? false : true;
                viewModel.IsInserting = formMode == FormMode.Dispaly ? false : true;
                viewModel.AjaxGridDataSource = "/../OsobaKontaktowa/GetRelatedEntities";
                viewModel.GridObjectName = "osobyKontaktoweGrid_" + DateTime.Now.Millisecond;
                viewModel.AjaxAddItemUrl = "/../OsobaKontaktowa/Create";
                viewModel.AjaxEditItemUrl = "/../OsobaKontaktowa/Edit";
                viewModel.AjaxRemoveItemUrl = "/../OsobaKontaktowa/Delete?id=";
                viewModel.DetailsElementId = kontaktId;
                viewModel.DetailsElementName = "Kontakt.Id";
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.OsobyKontaktowe, GlobalNames.Lists.Views.OsobyKontaktowe.KontaktyOsobyKontaktoweGrid);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }
        #endregion

        #region Zadania
        public static CustomGridViewModel GetZadaniaGridViewModel(string gridObjectName)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.AjaxGridDataSource = "../Zadanie/GetEntities";
                viewModel.GridItemDetailsControlerActionUrl = "/Kontakt/Details/";
                viewModel.AdvancedDivId = "zadanieAdvancedSearchDiv";
                viewModel.GridObjectName = gridObjectName;
                viewModel.CostomGridFieldsMetadataList = new List<CostomGridFieldMetadata>()
            {
                new CostomGridFieldMetadata() { Name="Title", Title="Nazwa"}
            };
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }
        #endregion

        #region Sprawa
        public static CustomGridViewModel GetSprawyGridViewModel(ClientContext ctx, string gridObjectName)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.AjaxGridDataSource = "../Sprawa/GetEntities";
                viewModel.GridItemDetailsControlerActionUrl = "/Sprawa/Details/";
                viewModel.AdvancedDivId = "sprawyAdvancedSearchDiv";
                viewModel.AdvancedSearchObjectList = new object[] {
                new { Label = "Czynności", Name = "Czynnosci" },
                new { Label = "Kontakty", Name = "Kontakty" },
                new { Label = "Zadania", Name = "Zadania" },
                new { Label = "Koszty", Name = "Koszty" },
            };
                viewModel.GridObjectName = gridObjectName;
                viewModel.IsSearching = true;
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.Sprawy, GlobalNames.Lists.Views.Sprawy.WszystkieElementy);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }

        public static CustomGridViewModel GetSprawaCzynnosciGridViewModel(ClientContext ctx, int sprawaId, FormMode formMode)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.DetailsElementId = sprawaId;
                viewModel.DetailsElementName = "Sprawa.Id";
                viewModel.AjaxGridDataSource = "/../Czynnosc/GetRelatedEntities";
                viewModel.GridItemDetailsControlerActionUrl = "/Czynnosc/Details/";
                viewModel.GridObjectName = "czynnosciGrid_" + DateTime.Now.Millisecond;
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.Czynnosci, GlobalNames.Lists.Views.Czynnosci.SprawaCzynnosci);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }

        public static CustomGridViewModel GetSprawaKosztyGridViewModel(ClientContext ctx, int sprawaId, FormMode formMode)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.DetailsElementId = sprawaId;
                viewModel.DetailsElementName = "Sprawa.Id";
                viewModel.AjaxGridDataSource = "/../Koszt/GetRelatedEntities";
                viewModel.GridItemDetailsControlerActionUrl = "/Koszt/Details/";
                viewModel.GridObjectName = "kosztyGrid_" + DateTime.Now.Millisecond;
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.Koszty, GlobalNames.Lists.Views.Koszty.SprawaKoszty);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }

        #endregion

        #region Czynnosci
        public static CustomGridViewModel GetCzynnosciGridViewModel(ClientContext ctx, string gridObjectName)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.AjaxGridDataSource = "../Czynnosc/GetEntities";
                viewModel.GridItemDetailsControlerActionUrl = "/Czynnosc/Details/";
                viewModel.GridObjectName = gridObjectName;
                viewModel.IsSearching = true;
                viewModel.IsAutoload = false;
                viewModel.AdvancedSearchObjectList = new object[] { };
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.Czynnosci, GlobalNames.Lists.Views.Czynnosci.WszystkieElementy);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }

        public static CustomGridViewModel GetMojeCzynnosciGridViewModel(ClientContext ctx, string gridObjectName)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.AjaxGridDataSource = "../Czynnosc/GetEntities/";
                viewModel.GridItemDetailsControlerActionUrl = "/Czynnosc/Details/";
                viewModel.GridObjectName = gridObjectName;
                viewModel.IsSearching = false;
                viewModel.IsAutoload = true;
                viewModel.PageSize = 5;
                viewModel.UseCustomDataSource = true;
                viewModel.CustomDataSourceName = "Czynnosc-Moje";
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.Czynnosci, GlobalNames.Lists.Views.Czynnosci.Moje);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }


        #endregion

        #region Koszty
        public static CustomGridViewModel GetKosztyGridViewModel(ClientContext ctx, string gridObjectName)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.AjaxGridDataSource = "../Koszt/GetEntities";
                viewModel.GridItemDetailsControlerActionUrl = "/Koszt/Details/";
                viewModel.GridObjectName = gridObjectName;
                viewModel.NoDataContent = "Wskaż kontakt i sprawę aby pobrać dane";
                viewModel.IsSearching = true;
                viewModel.IsAutoload = false;
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.Koszty, GlobalNames.Lists.Views.Koszty.WszystkieElementy);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }

        public static CustomGridViewModel GetMojeKosztyGridViewModel(ClientContext ctx, string gridObjectName)
        {
            CustomGridViewModel viewModel = GetStandardGridViewModel();
            try
            {
                viewModel.AjaxGridDataSource = "../Koszt/GetEntities/";
                viewModel.GridItemDetailsControlerActionUrl = "/Koszt/Details/";
                viewModel.GridObjectName = gridObjectName;
                viewModel.IsSearching = false;
                viewModel.IsAutoload = true;
                viewModel.PageSize = 5;
                viewModel.UseCustomDataSource = true;
                viewModel.CustomDataSourceName = "Koszt-Moje";
                viewModel.CostomGridFieldsMetadataList = GetFieldsInView(ctx, GlobalNames.Lists.Koszty, GlobalNames.Lists.Views.Koszty.Moje);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return viewModel;
        }
        #endregion

        private static CustomGridViewModel GetStandardGridViewModel()
        {
            CustomGridViewModel viewModel = new CustomGridViewModel();
            viewModel.IsPager = true;
            viewModel.IsPagerEnable = true;
            viewModel.PagerSizes = DefaultPagerSizes;
            viewModel.PageSize = DefaultPageSize;
            viewModel.UILibrary = DefaultUILibrary;
            viewModel.DetailsButtonCssClass = DefaultDetailsButtonCssClass;
            viewModel.DetailsButtonHeaderCssClass = DefaultDetailsButtonHeaderCssClass;
            viewModel.IsDetailsButton = true;
            viewModel.ResizableColumns = true;
            viewModel.IsEditing = false;
            viewModel.IsInserting = false;
            viewModel.IsSearching = false;
            viewModel.IsFileLoader = false;
            viewModel.IsAutoload = true;
            viewModel.UseCustomDataSource = false;
            viewModel.CustomDataSourceName = String.Empty;
            viewModel.NoDataContent = String.Empty;
            viewModel.GuidObject = DateTime.Now.Millisecond.ToString();

            return viewModel;
        }

        private static List<CostomGridFieldMetadata> GetFieldsInView(ClientContext ctx, string listTitle, string viewName)
        {
            List list = ctx.Web.Lists.GetByTitle(listTitle);
            View view = list.Views.GetByTitle(viewName);
            ctx.Load(view.ViewFields);
            ctx.Load(list.Fields);
            ctx.ExecuteQuery();

            foreach (string internalName in view.ViewFields)
            {
                ctx.Load(list.Fields.GetByInternalNameOrTitle(internalName));
            }
            ctx.ExecuteQuery();

            List<CostomGridFieldMetadata> returnList = new List<CostomGridFieldMetadata>();
            foreach (string internalName in view.ViewFields)
            {

                Field filed = list.Fields.GetByInternalNameOrTitle(internalName);

                CostomGridFieldMetadata element = new CostomGridFieldMetadata();
                element.Name = internalName;
                element.Title = list.Fields.GetByInternalNameOrTitle(internalName).Title;
                SetupAdditionalFieldValues(ctx, element, listTitle, viewName);

                if (filed.FieldTypeKind == FieldType.User ||
                    filed.FieldTypeKind == FieldType.Lookup)
                {
                    element.ShowFieldValueProperty = true;
                }
                if (filed.FieldTypeKind == FieldType.DateTime)
                {
                    element.IsDate = true;
                }
                returnList.Add(element);
            }
            return returnList;
        }

        private static void SetupAdditionalFieldValues(ClientContext ctx, CostomGridFieldMetadata element, string listTitle, string viewName)
        {
            if (listTitle == GlobalNames.Lists.KontaktyDokumentyFinansowe)
            {
                if (viewName == GlobalNames.Lists.Views.KontaktyDokumentyFinansowe.KontaktyDokumentyFinansoweGrid)
                {
                    if (element.Name == "Title")
                    {
                        element.IsEditable = true;
                    }
                }
            }

            if (listTitle == GlobalNames.Lists.StawkiPracownikow)
            {
                if (viewName == GlobalNames.Lists.Views.StawkiPracownikow.KontaktyStawkiPracownikow)
                {
                    if (element.Name == "KategoriaPracownika")
                    {
                        SlownikRepository repo = new SlownikRepository(ctx);
                        element.ItemEditDropdownValues = repo.GetEntityList(GlobalNames.Lists.KategoriePracownikow);
                    }
                }
            }

            //if (listTitle == GlobalNames.Lists.Konta)
            //{
            //    if (viewName == GlobalNames.Lists.Views.Konta.KontaktyKontaBankoweGrid)
            //    {
            //        element.IsEditable = true;
            //    }
            //}
        }
    }
}