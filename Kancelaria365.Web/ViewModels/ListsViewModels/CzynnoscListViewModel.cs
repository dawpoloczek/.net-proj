﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kancelaria365.Data.Interfaces;
using Kancelaria365.Data;
using Microsoft.SharePoint.Client;

namespace Kancelaria365.Web
{
    public class CzynnoscListViewModel
        : BaseViewModel
    {
        public CzynnoscFilterViewModel FilterViewModel { get; set; }

        public CustomGridViewModel GridViewModel { get; set; }


        public CzynnoscListViewModel(ClientContext ctx)
            : base(ctx)
        {
            FilterViewModel = new CzynnoscFilterViewModel(ctx);
            GridViewModel = CustomGridViewModel.GetCzynnosciGridViewModel(ctx, "czynnosci_grid");
        }
    }
}
