﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint.Client;
using Kancelaria365.Data;
using System.Threading.Tasks;
using Kancelaria365.ConnectedServices;
using Kancelaria365.ConnectedServices.Services;
using Kancelaria365.Web.Helpers;

namespace Kancelaria365.Web
{
    public class KontaktListViewModel
            : BaseViewModel
    {
        HttpContextBase httpContext;
        //ContactService service;

        public IEnumerable<Kontakt> Kontakty { get; set; }

        public KontaktFilterViewModel FilterViewModel { get; set; }

        public CustomGridViewModel GridViewModel { get; set; }

        public KontaktListViewModel(HttpContextBase httpContext)
        {
            this.httpContext = httpContext;
        }

        public KontaktListViewModel(ClientContext ctx)
            : base(ctx)
        {
            FilterViewModel = new KontaktFilterViewModel(ctx);
            MemoryCacheHelper<KontaktRepository, Kontakt> cacheHelper =
                new MemoryCacheHelper<KontaktRepository, Kontakt>(ctx);

            Kontakty = cacheHelper.EntityList;

            GridViewModel = CustomGridViewModel.GetKontaktyGridViewModel(ctx, "kontakty_grid");
        }
    }
}