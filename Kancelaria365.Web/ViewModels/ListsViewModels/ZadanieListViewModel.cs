﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kancelaria365.Data;
using Kancelaria365.ConnectedServices.Services;
using System.Threading.Tasks;
using Beta = GraphBeta;

namespace Kancelaria365.Web
{
    public class ZadanieListViewModel
    {
        HttpContextBase httpContext;
        TaskService service;

        public List<Beta.Task> ZadaniaStworzonePrzezemnie { get; set; }
        
        public List<Beta.Task> ZadaniaPrzypisaneDoMnieZOstatniegoMiesiaca { get; set; }

        public List<Beta.Task> ZadaniaPrzypisaneDoMnie { get; set; }

        public CustomGridViewModel ZadaniaGridViewModel { get; set; }

        public ZadanieFilterViewModel FilterViewModel { get; set; }

        public ZadanieListViewModel(HttpContextBase httpContext)
        {
            this.httpContext = httpContext;
            this.service = new TaskService(httpContext);
            this.FilterViewModel = new ZadanieFilterViewModel();
            ZadaniaGridViewModel = CustomGridViewModel.GetZadaniaGridViewModel("zadaniaListGrid");
        }

        public async Task Init()
        {
            this.ZadaniaStworzonePrzezemnie = (await service.GetCreatedByMeTasks()).ToList();
            this.ZadaniaPrzypisaneDoMnie = (await service.GetAssignedToMeTasks()).ToList();
            this.ZadaniaPrzypisaneDoMnieZOstatniegoMiesiaca = (await service.GetAssignedToMeTasksForLastMonth()).ToList();
        }
    }
}