﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kancelaria365.Data.Interfaces;
using Kancelaria365.Data;
using Microsoft.SharePoint.Client;

namespace Kancelaria365.Web
{
    public class KosztListViewModel
        : BaseViewModel
    {
        public KoszrFilterViewModel FilterViewModel { get; set; }

        public CustomGridViewModel GridViewModel { get; set; }


        public KosztListViewModel(ClientContext ctx)
            : base(ctx)
        {
            FilterViewModel = new KoszrFilterViewModel(ctx);
            GridViewModel = CustomGridViewModel.GetKosztyGridViewModel(ctx, "koszty_grid");
        }
    }
}
