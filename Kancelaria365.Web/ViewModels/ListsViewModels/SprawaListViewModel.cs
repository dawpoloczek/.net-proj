﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint.Client;

namespace Kancelaria365.Web
{
    public class SprawaListViewModel
        : BaseViewModel
    {
        public SprawaFilterViewModel FilterViewModel { get; set; }
        public CustomGridViewModel GridViewModel { get; set; }

        public SprawaListViewModel()
        {
        }

        public SprawaListViewModel(ClientContext ctx)
            : base(ctx)
        {
            FilterViewModel = new SprawaFilterViewModel(ctx);
            GridViewModel = CustomGridViewModel.GetSprawyGridViewModel(ctx, "sprawy_grid");
        }
    }
}
