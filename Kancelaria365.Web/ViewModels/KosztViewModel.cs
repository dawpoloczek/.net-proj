﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kancelaria365.Data;
using Kancelaria365.Web.Controllers;
using Microsoft.SharePoint.Client;
using Kancelaria365.Web.Extensions;
using Kancelaria365.Common;
using Kancelaria365.Web.Helpers;

namespace Kancelaria365.Web
{
    public class KosztViewModel
            : BaseViewModel
    {
        HttpContextBase HttpContextBase;
        public bool Enabled { get; set; }

        public List<DictionaryItem> Sprawy
        {
            get
            {
                List<DictionaryItem> items = new List<DictionaryItem>();
                if (Koszt.Firma.Id != 0)
                {
                    MemoryCacheHelper<SprawaRepository, Sprawa>
                        helper = new MemoryCacheHelper<SprawaRepository, Sprawa>(ClientContext);

                    List<Sprawa> sprawy = helper.EntityList.Where(x => x.Kontakt.Id == Koszt.Firma.Id).ToList();
                    
                    sprawy.ForEach(x => items.Add(x.GetDictionaryItem()));
                    items.Insert(0, new DictionaryItem(0, "--Wybierz--"));
                }
                return items;
            }
        }

        public List<DictionaryItem> Kontakty
        {
            get
            {
                MemoryCacheHelper<KontaktRepository, Kontakt>
                    helper = new MemoryCacheHelper<KontaktRepository, Kontakt>(ClientContext);

                List<DictionaryItem> items = new List<DictionaryItem>();
                helper.EntityList.ToList().ForEach(x => items.Add(x.GetDictionaryItem()));
                items.Insert(0, new DictionaryItem(0, "--Wybierz--"));
                return items;
            }
        }

        public List<DictionaryItem> KategorieKosztow
        {
            get
            {
                List<DictionaryItem> kategorieKosztow = HttpContextBase.GetSessionValue<List<DictionaryItem>>("KategorieKosztow");
                if (kategorieKosztow == null)
                {
                    kategorieKosztow = SlownikRepository.GetEntityList(GlobalNames.Lists.KategorieKosztow, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("KategorieKosztow", kategorieKosztow);
                }
                return kategorieKosztow;
            }
        }

        public List<DictionaryItem> Waluty
        {
            get
            {
                List<DictionaryItem> waluty = HttpContextBase.GetSessionValue<List<DictionaryItem>>("Waluty");
                if (waluty == null)
                {
                    waluty = SlownikRepository.GetEntityList(GlobalNames.Lists.Waluty, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("Waluty", waluty);
                }
                return waluty;
            }
        }

        public List<DictionaryItem> StawkiVAT
        {
            get
            {
                List<DictionaryItem> stawkiVAT = HttpContextBase.GetSessionValue<List<DictionaryItem>>("StawkiVAT");
                if (stawkiVAT == null)
                {
                    stawkiVAT = SlownikRepository.GetEntityList(GlobalNames.Lists.StawkiVAT, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("StawkiVAT", stawkiVAT);
                }
                return stawkiVAT;
            }
        }

        public Koszt Koszt { get; set; }

        private void SetEnabled(FormMode mode)
        {
            if (mode == FormMode.Dispaly)
            {
                Enabled = false;
            }
            else
            {
                Enabled = true;
            }
        }

        public KosztViewModel()
        {
            Enabled = true;
        }

        public KosztViewModel(HttpContextBase httpContextBase,
            ClientContext ctx, int? sprawaId, FormMode mode = FormMode.New)
            : base(ctx)
        {
            this.HttpContextBase = httpContextBase;
            this.FormMode = mode;
            this.Koszt = new Koszt();

            if (sprawaId.HasValue)
            {
                Sprawa sprawa = SprawaRepository.GetEntityById(sprawaId.Value);

                this.Koszt.Sprawa = sprawa.GetDictionaryItem();
                this.Koszt.Firma = sprawa.Kontakt;
            }

            this.Koszt.Wprowadzajacy =
                new UserDictionaryItem(
                    UserRepository.CurrentUser.Id,
                    UserRepository.CurrentUser.Title,
                    UserRepository.CurrentUser.LoginName);
            SetEnabled(mode);
        }

        public KosztViewModel(HttpContextBase httpContextBase, ClientContext ctx, Koszt koszt, FormMode mode)
            : base(ctx)
        {
            this.HttpContextBase = httpContextBase;
            this.FormMode = mode;
            this.Koszt = koszt;

            SetEnabled(mode);
        }
    }
}