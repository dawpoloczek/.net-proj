﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kancelaria365.Data.Interfaces;
using Kancelaria365.Data;
using Microsoft.SharePoint.Client;

namespace Kancelaria365.Web
{
    public class HomeListViewModel
        : BaseViewModel
    {
        public CustomGridViewModel CzynnosciGridViewModel { get; set; }

        public CustomGridViewModel KosztyGridViewModel { get; set; }


        public HomeListViewModel(ClientContext ctx)
            : base(ctx)
        {
            CzynnosciGridViewModel = CustomGridViewModel.GetMojeCzynnosciGridViewModel(ctx, "czynnosci_grid");
            KosztyGridViewModel = CustomGridViewModel.GetMojeKosztyGridViewModel(ctx, "koszty_grid");
        }
    }
}
