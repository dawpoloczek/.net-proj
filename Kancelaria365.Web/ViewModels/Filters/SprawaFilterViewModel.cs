﻿using Kancelaria365.Common;
using Kancelaria365.Data;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Web
{
    public class SprawaFilterViewModel
     : BaseViewModel
    {
        [Display(Name = "Stan")]
        public List<string> Stany { get; set; }

        [Display(Name = "Kategoria")]
        public List<DictionaryItem> Kategorie { get; set; }

        public SprawaFilterViewModel()
        {
        }

        public SprawaFilterViewModel(ClientContext ctx)
            : base(ctx)
        {
            Kategorie = SlownikRepository.GetEntityList(GlobalNames.Lists.KategorieSpraw);
            Kategorie.Insert(0, new DictionaryItem(0, "--"));

            Stany = SprawaRepository.GetChoices("Stan");
            Stany.Insert(0, "--");
        }
    }
}