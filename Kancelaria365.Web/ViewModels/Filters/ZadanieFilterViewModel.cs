﻿using Kancelaria365.Common;
using Kancelaria365.Data;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Web
{
    public class ZadanieFilterViewModel
     : BaseViewModel
    {
        [Display(Name = "Przypisane do mnie")]
        public bool PrzypisaneDoMnie{ get; set; }

        [Display(Name = "Z dnia")]
        public DateTime ZDnia { get; set; }

        public ZadanieFilterViewModel()
        {
            ZDnia = DateTime.UtcNow;
        }
    }
}