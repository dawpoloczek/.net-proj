﻿using Kancelaria365.Data.Interfaces;
using Kancelaria365.Common;
using Kancelaria365.Data;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Kancelaria365.Web.Helpers;

namespace Kancelaria365.Web
{
    public class KoszrFilterViewModel
        : BaseViewModel
    {
        [Display(Name = "Kontakt")]
        public List<DictionaryItem> Kontakty
        {
            get
            {
                List<DictionaryItem> items = new List<DictionaryItem>();
                {
                    var helper = new MemoryCacheHelper<KontaktRepository, Kontakt>(ClientContext);

                    items = helper.GetDictionaryEntityList();
                }
                return items;
            }
        }

        [Display(Name = "Sprawa")]
        public List<DictionaryItem> Sprawy
        {
            get
            {
                List<DictionaryItem> items = new List<DictionaryItem>();
                {
                    var item = new DictionaryItem(0, GlobalNames.Constants.DefaultListValue);
                    items.Insert(0, item);
                }
                return items;
            }
        }

        public KoszrFilterViewModel()
        {
        }

        public KoszrFilterViewModel(ClientContext ctx)
            : base(ctx)
        {
        }
    }
}