﻿using Kancelaria365.Common;
using Kancelaria365.Data;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kancelaria365.Web
{
    public class KontaktFilterViewModel
     : BaseViewModel
    {
        [Display(Name = "Status")]
        public List<string> Statusy { get; set; }
        [Display(Name = "Kategoria")]
        public List<DictionaryItem> Kategorie { get; set; }

        public KontaktFilterViewModel()
        {
        }

        public KontaktFilterViewModel(ClientContext ctx)
            : base(ctx)
        {
            Kategorie = SlownikRepository.GetEntityList(GlobalNames.Lists.KategorieKontaktow);
            Kategorie.Insert(0, new DictionaryItem(0, "--"));

            Statusy = KontaktRepository.GetChoices("Status");
            Statusy.Insert(0, "--");
        }
    }
}