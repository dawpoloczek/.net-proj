﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kancelaria365.Data;
using Kancelaria365.Common;

namespace Kancelaria365.Web
{
    public class CustomGraphGridViewModel
    {
        public string DetailsElementId;
        public string DetailsElementName;
        public bool ResizableColumns;
        public string UILibrary;
        public string AjaxGridDataSource;
        public string AjaxAddItemUrl;
        public string AjaxEditItemUrl;
        public string AjaxRemoveItemUrl;
        public List<CostomGridFieldMetadata> CostomGridFieldsMetadataList;
        public bool IsDetailsButton;
        public string DetailsButtonHeaderCssClass;
        public string DetailsButtonCssClass;
        public object[] AdvancedSearchObjectList;
        public string AdvancedDivId;

        public bool IsSearching;
        public bool IsPager;
        public bool IsInserting;
        public bool IsPagerEnable;
        public bool IsEditing;
        public int PageSize;
        public bool IsFileLoader;

        public string GuidObject;

        public List<int> PagerSizes;
        public string GridItemDetailsControlerActionUrl;
        public string GridObjectName;

        #region Default values
        private static List<int> DefaultPagerSizes = new List<int>() { 10, 20 };
        private static int DefaultPageSize = 10;
        private static string DefaultUILibrary = "bootstrap";
        private static string DefaultDetailsButtonHeaderCssClass = "cust-grid-header";
        private static string DefaultDetailsButtonCssClass = "cust-grid-cell";
        #endregion

        private CustomGraphGridViewModel()
        { }

        public static CustomGraphGridViewModel GetZadaniaGridViewModel(string gridObjectName)
        {
            CustomGraphGridViewModel viewModel = GetStandardGridViewModel();
            viewModel.AjaxGridDataSource = "../Zadanie/GetEntities";
            viewModel.GridItemDetailsControlerActionUrl = "/Kontakt/Details/";
            viewModel.AdvancedDivId = "zadanieAdvancedSearchDiv";
            viewModel.GridObjectName = gridObjectName;
            viewModel.CostomGridFieldsMetadataList = new List<CostomGridFieldMetadata>()
            {
                new CostomGridFieldMetadata() { Name="Title", Title="Nazwa"}
            };
            return viewModel;
        }

        #region Sprawa

        public static CustomGraphGridViewModel GetSprawaZadaniaGridViewModel(string sprawaPlanId, FormMode formMode)
        {
            CustomGraphGridViewModel viewModel = GetStandardGridViewModel();
            viewModel.DetailsElementId = sprawaPlanId;
            viewModel.DetailsElementName = "PlanId";
            viewModel.AjaxGridDataSource = "/../Zadanie/GetRelatedEntities";
            viewModel.GridItemDetailsControlerActionUrl = "/Zadanie/Details/";
            viewModel.GridObjectName = "zadaniaGrid_" + DateTime.Now.Millisecond;
            viewModel.CostomGridFieldsMetadataList = new List<CostomGridFieldMetadata>()
            {
                new CostomGridFieldMetadata() { Name="Title", Title="Nazwa"}
            };
            return viewModel;
        }

        #endregion
        private static CustomGraphGridViewModel GetStandardGridViewModel()
        {
            CustomGraphGridViewModel viewModel = new CustomGraphGridViewModel();
            viewModel.IsPager = true;
            viewModel.IsPagerEnable = true;
            viewModel.PagerSizes = DefaultPagerSizes;
            viewModel.PageSize = DefaultPageSize;
            viewModel.UILibrary = DefaultUILibrary;
            viewModel.DetailsButtonCssClass = DefaultDetailsButtonCssClass;
            viewModel.DetailsButtonHeaderCssClass = DefaultDetailsButtonHeaderCssClass;
            viewModel.IsDetailsButton = true;
            viewModel.ResizableColumns = true;
            viewModel.IsEditing = false;
            viewModel.IsInserting = false;
            viewModel.IsSearching = false;
            viewModel.IsFileLoader = false;
            viewModel.GuidObject = DateTime.Now.Millisecond.ToString();

            return viewModel;
        }

        private static List<CostomGridFieldMetadata> GetFieldsInView(ClientContext ctx, string listTitle, string viewName)
        {
            List list = ctx.Web.Lists.GetByTitle(listTitle);
            View view = list.Views.GetByTitle(viewName);
            ctx.Load(view.ViewFields);
            ctx.Load(list.Fields);
            ctx.ExecuteQuery();

            foreach (string internalName in view.ViewFields)
            {
                ctx.Load(list.Fields.GetByInternalNameOrTitle(internalName));
            }
            ctx.ExecuteQuery();

            List<CostomGridFieldMetadata> returnList = new List<CostomGridFieldMetadata>();
            foreach (string internalName in view.ViewFields)
            {

                Field filed = list.Fields.GetByInternalNameOrTitle(internalName);

                CostomGridFieldMetadata element = new CostomGridFieldMetadata();
                element.Name = internalName;
                element.Title = list.Fields.GetByInternalNameOrTitle(internalName).Title;
                SetupAdditionalFieldValues(ctx, element, listTitle, viewName);

                if (filed.FieldTypeKind == FieldType.User ||
                    filed.FieldTypeKind == FieldType.Lookup)
                {
                    element.ShowFieldValueProperty = true;
                }
                if (filed.FieldTypeKind == FieldType.DateTime)
                {
                    element.IsDate = true;
                }
                returnList.Add(element);
            }
            return returnList;
        }

        private static void SetupAdditionalFieldValues(ClientContext ctx, CostomGridFieldMetadata element, string listTitle, string viewName)
        {
            if (listTitle == GlobalNames.Lists.KontaktyDokumentyFinansowe)
            {
                if (viewName == GlobalNames.Lists.Views.KontaktyDokumentyFinansowe.KontaktyDokumentyFinansoweGrid)
                {
                    if (element.Name == "Title")
                    {
                        element.IsEditable = true;
                    }
                }
            }

            if (listTitle == GlobalNames.Lists.StawkiPracownikow)
            {
                if (viewName == GlobalNames.Lists.Views.StawkiPracownikow.KontaktyStawkiPracownikow)
                {
                    if (element.Name == "KategoriaPracownika")
                    {
                        SlownikRepository repo = new SlownikRepository(ctx);
                        element.ItemEditDropdownValues = repo.GetEntityList(GlobalNames.Lists.KategoriePracownikow);
                    }
                }
            }

            //if (listTitle == GlobalNames.Lists.Konta)
            //{
            //    if (viewName == GlobalNames.Lists.Views.Konta.KontaktyKontaBankoweGrid)
            //    {
            //        element.IsEditable = true;
            //    }
            //}
        }
    }
}