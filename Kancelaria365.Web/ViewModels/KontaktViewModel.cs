﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint.Client;
using System.Web.Mvc;
using Kancelaria365.Web.Controllers;
using Kancelaria365.Data;
using Kancelaria365.Common;
using Kancelaria365.Web.Extensions;
using Kancelaria365.ConnectedServices;
using Beta = GraphBeta;
using System.Threading.Tasks;

namespace Kancelaria365.Web
{
    public class KontaktViewModel
            : BaseViewModel
    {
        HttpContextBase HttpContextBase;
        public bool Enabled { get; set; }

        public List<string> Wojewodztwa
        {
            get
            {
                List<string> wojewodztwa = HttpContextBase.GetSessionValue<List<string>>("Wojewodztwo");
                if (wojewodztwa == null)
                {
                    wojewodztwa = KontaktRepository.GetChoices("Wojewodztwo", true);
                    HttpContextBase.SetSessionValue<List<string>>("Wojewodztwo", wojewodztwa);
                }
                return wojewodztwa;
            }
        }

        public List<string> FormyRozliczen
        {
            get
            {
                List<string> formyRozliczen = HttpContextBase.GetSessionValue<List<string>>("FormyRozliczen");
                if (formyRozliczen == null)
                {
                    formyRozliczen = KontaktRepository.GetChoices("FormaRozliczenia", true);
                    HttpContextBase.SetSessionValue<List<string>>("FormyRozliczen", formyRozliczen);
                }
                return formyRozliczen;
            }
        }

        public List<DictionaryItem> WalutyFaktury
        {
            get
            {
                List<DictionaryItem> walutyFaktury = HttpContextBase.GetSessionValue<List<DictionaryItem>>("WalutyFaktury");
                if (walutyFaktury == null)
                {
                    walutyFaktury = SlownikRepository.GetEntityList(GlobalNames.Lists.Waluty, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("WalutyFaktury", walutyFaktury);
                }
                return walutyFaktury;
            }
        }

        public List<DictionaryItem> DomyslneStawkiVatFaktury
        {
            get
            {
                List<DictionaryItem> domyslneStawkiVatFaktury = HttpContextBase.GetSessionValue<List<DictionaryItem>>("DomyslneStawkiVatFaktury");
                if (domyslneStawkiVatFaktury == null)
                {
                    domyslneStawkiVatFaktury = SlownikRepository.GetEntityList(GlobalNames.Lists.StawkiVAT, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("DomyslneStawkiVatFaktury", domyslneStawkiVatFaktury);
                }
                return domyslneStawkiVatFaktury;
            }
        }

        public List<string> FormyPlatnosci
        {
            get
            {
                List<string> formyPlatnosci = HttpContextBase.GetSessionValue<List<string>>("FormyPlatnosci");
                if (formyPlatnosci == null)
                {
                    formyPlatnosci = KontaktRepository.GetChoices("FormaPlatnosci", true);
                    HttpContextBase.SetSessionValue<List<string>>("FormyPlatnosci", formyPlatnosci);
                }
                return formyPlatnosci;
            }
        }

        public List<string> DomyslneSzablonyFaktury
        {
            get
            {
                List<string> domyslneSzablonyFaktury = HttpContextBase.GetSessionValue<List<string>>("DomyslneSzablonyFaktury");
                if (domyslneSzablonyFaktury == null)
                {
                    domyslneSzablonyFaktury = KontaktRepository.GetChoices("DomyslnySzablonFaktury", true);
                    HttpContextBase.SetSessionValue<List<string>>("DomyslneSzablonyFaktury", domyslneSzablonyFaktury);
                }
                return domyslneSzablonyFaktury;
            }
        }

        public List<string> TypyKontaktow
        {
            get
            {
                List<string> typyKontaktow = HttpContextBase.GetSessionValue<List<string>>("TypKontaktu");
                if (typyKontaktow == null)
                {
                    typyKontaktow = KontaktRepository.GetChoices("TypKontaktu", true);
                    HttpContextBase.SetSessionValue<List<string>>("TypKontaktu", typyKontaktow);
                }
                return typyKontaktow;
            }
        }

        public List<DictionaryItem> WalutyStawekGodzinowych
        {
            get
            {
                List<DictionaryItem> walutyStawekGodzinowych = HttpContextBase.GetSessionValue<List<DictionaryItem>>("WalutyStawekGodzinowych");
                if (walutyStawekGodzinowych == null)
                {
                    walutyStawekGodzinowych = SlownikRepository.GetEntityList(GlobalNames.Lists.Waluty, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("WalutyStawekGodzinowych", walutyStawekGodzinowych);
                }
                return walutyStawekGodzinowych;
            }
        }

        public List<DictionaryItem> KategorieKontaktow
        {
            get
            {
                List<DictionaryItem> kategorieKontaktow = HttpContextBase.GetSessionValue<List<DictionaryItem>>("KategorieKontaktow");
                if (kategorieKontaktow == null)
                {
                    kategorieKontaktow = SlownikRepository.GetEntityList(GlobalNames.Lists.KategorieKontaktow, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("KategorieKontaktow", kategorieKontaktow);
                }
                return kategorieKontaktow;
            }
        }

        public List<DictionaryItem> Kancelarie
        {
            get
            {
                List<DictionaryItem> kancelarie = HttpContextBase.GetSessionValue<List<DictionaryItem>>("Kancelarie");
                if (kancelarie == null)
                {
                    kancelarie = SlownikRepository.GetEntityList(GlobalNames.Lists.Kancelarie, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("Kancelarie", kancelarie);
                }
                return kancelarie;
            }
        }

        List<Beta.OrgContact> kontaktyOrganizacji = new List<Beta.OrgContact>();
        public List<Beta.OrgContact> KontaktyOrganizacji
        {
            get
            {
                return kontaktyOrganizacji;
            }
        }
        public async Task<List<Beta.OrgContact>> Init(HttpContextBase httpContextBase = null)
        {
            if (httpContextBase != null)
            {
                HttpContextBase = httpContextBase;
            }
            var viewData = HttpContextBase.GetSessionValue<List<Beta.OrgContact>>("KontaktyOrganizacji");
            if (viewData == null)
            {
                CustomGraphService service = new CustomGraphService(HttpContextBase);
                var contacts = await service.GetContacts();
                if (contacts == null)
                {
                    viewData = new List<Beta.OrgContact>();
                }
                else
                {
                    viewData = contacts.ToList();
                }
                viewData.Insert(0, new Beta.OrgContact() { Id = "", DisplayName = "--Wybierz--" });

                HttpContextBase.SetSessionValue<List<Beta.OrgContact>>("KontaktyOrganizacji", viewData);
            }
            kontaktyOrganizacji = viewData;
            return viewData;
        }

        public CustomGridViewModel SprawyGridViewModel { get; set; }
        public CustomGridViewModel OsobyKontaktoweGridViewModel { get; set; }
        public CustomGridViewModel DokumentyFinansoweGridViewModel { get; set; }
        public CustomGridViewModel StawkiPracownikowGridViewModel { get; set; }
        public CustomGridViewModel KontaBankoweGridViewModel { get; set; }

        public Kontakt Kontakt { get; set; }

        public KontaktViewModel()
        {
            Enabled = true;
        }

        private void SetEnabled(FormMode mode)
        {
            if (mode == FormMode.Dispaly)
            {
                Enabled = false;
            }
            else
            {
                Enabled = true;
            }
        }

        public KontaktViewModel(HttpContextBase httpContextBase, ClientContext ctx, FormMode mode = FormMode.New)
            : base(ctx)
        {
            this.HttpContextBase = httpContextBase;
            this.FormMode = mode;

            this.Kontakt = new Kontakt();
            this.Kontakt.DataWprowadzenia = DateTime.Now.Date;
            this.Kontakt.Status = "Aktywny";
            this.Kontakt.Wojewodztwo = Wojewodztwa.First();
            this.Kontakt.KorespondencjWojewodztwo = Wojewodztwa.First();
            this.Kontakt.TypKontaktu = "osoba";
            Kontakt.Wprowadzajacy = new UserDictionaryItem(UserRepository.CurrentUser);
            Kontakt.Prowadzacy = new UserDictionaryItem(UserRepository.CurrentUser);

            SetEnabled(mode);
        }

        public KontaktViewModel(HttpContextBase httpContextBase, ClientContext ctx, Kontakt kontakt, FormMode mode)
            : base(ctx)
        {
            this.HttpContextBase = httpContextBase;
            this.FormMode = mode;
            this.Kontakt = kontakt;

            SetEnabled(mode);

            SprawyGridViewModel = CustomGridViewModel.GetKontaktSprawyGridViewModel(ctx, kontakt.Id, mode);
            OsobyKontaktoweGridViewModel = CustomGridViewModel.GetKontaktOsobyKontaktoweGridViewModel(ctx, kontakt.Id, mode);
            DokumentyFinansoweGridViewModel = CustomGridViewModel.GetKontaktDokumentyFinansoweGridViewModel(ctx, kontakt.Id, mode);
            StawkiPracownikowGridViewModel = CustomGridViewModel.GetKontaktStawkiPracownikowGridViewModel(ctx, kontakt.Id, mode);
            KontaBankoweGridViewModel = CustomGridViewModel.GetKontaktKontaBankoweGridViewModel(ctx, kontakt.Id, mode);
        }
    }
}