﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint.Client;
using Kancelaria365.Web.Controllers;
using Kancelaria365.Data;
using Kancelaria365.Common;
using Kancelaria365.Web.Extensions;

namespace Kancelaria365.Web
{
    public class SprawaViewModel
            : BaseViewModel
    {
        HttpContextBase HttpContextBase;

        public bool Enabled { get; set; }

        public List<DictionaryItem> Kontakty
        {
            get
            {
                List<DictionaryItem> kontakty = HttpContextBase.GetSessionValue<List<DictionaryItem>>("Kontakty");
                if (kontakty == null)
                {
                    kontakty = SlownikRepository.GetEntityList(GlobalNames.Lists.Kontakty, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("Kontakty", kontakty);
                }
                return kontakty;
            }
        }

        public List<DictionaryItem> KategorieSpraw
        {
            get
            {
                List<DictionaryItem> kategorieSpraw = HttpContextBase.GetSessionValue<List<DictionaryItem>>("KategorieSpraw");
                if (kategorieSpraw == null)
                {
                    kategorieSpraw = SlownikRepository.GetEntityList(GlobalNames.Lists.KategorieSpraw, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("KategorieSpraw", kategorieSpraw);
                }
                return kategorieSpraw;
            }
        }

        public List<DictionaryItem> FormyRozliczenia
        {
            get
            {
                List<DictionaryItem> formyRozliczenia = HttpContextBase.GetSessionValue<List<DictionaryItem>>("FormyRozliczenia");
                if (formyRozliczenia == null)
                {
                    formyRozliczenia = SlownikRepository.GetEntityList(GlobalNames.Lists.FormyRozliczenia, true);
                    HttpContextBase.SetSessionValue<List<DictionaryItem>>("FormyRozliczenia", formyRozliczenia);
                }
                return formyRozliczenia;
            }
        }
       
        public List<string> Etapy
        {
            get
            {
                List<string> etapy = HttpContextBase.GetSessionValue<List<string>>("Etapy");
                if (etapy == null)
                {
                    etapy = SprawaRepository.GetChoices("Etap", true);
                    HttpContextBase.SetSessionValue<List<string>>("Etapy", etapy);
                }
                return etapy;
            }
        }

        public CustomGridViewModel CzynnosciGridViewModel { get; set; }

        public CustomGraphGridViewModel ZadaniaGridViewModel { get; set; }

        public CustomGridViewModel KosztyGridViewModel { get; set; }

        public Sprawa Sprawa { get; set; }

        public bool IsFirmaSelected { get; set; }

        private void SetEnabled(FormMode mode)
        {
            if (mode == FormMode.Dispaly)
            {
                Enabled = false;
            }
            else
            {
                Enabled = true;
            }
        }

        public SprawaViewModel()
        {
        }

        public SprawaViewModel(HttpContextBase httpContextBase, ClientContext ctx)
            : base(ctx)
        {
            FormMode = FormMode.New;
            HttpContextBase = httpContextBase;
            Sprawa = new Sprawa();
            Sprawa.DataWprowadzenia = DateTime.Now;
            Sprawa.DataPrzyjecia = DateTime.Now;
            Sprawa.Wprowadzajacy = new UserDictionaryItem(UserRepository.CurrentUser);
            Sprawa.Prowadzacy = new UserDictionaryItem(UserRepository.CurrentUser);
            Sprawa.Zlecajacy = new UserDictionaryItem(UserRepository.CurrentUser);
        }

        public SprawaViewModel(HttpContextBase httpContextBase, ClientContext ctx, Sprawa sprawa, FormMode mode)
            : base(ctx)
        {
            this.HttpContextBase = httpContextBase;
            this.FormMode = mode;
            this.Sprawa = sprawa;

            SetEnabled(mode);

            CzynnosciGridViewModel = CustomGridViewModel.GetSprawaCzynnosciGridViewModel(ctx, sprawa.Id, mode);
            KosztyGridViewModel = CustomGridViewModel.GetSprawaKosztyGridViewModel(ctx, sprawa.Id, mode);
            ZadaniaGridViewModel = CustomGraphGridViewModel.GetSprawaZadaniaGridViewModel(sprawa.PlanId, mode);
            //OsobyKontaktoweGridViewModel = CustomGridViewModel.GetKontaktOsobyKontaktoweGridViewModel(ctx, kontakt.Id, mode);
            //DokumentyFinansoweGridViewModel = CustomGridViewModel.GetKontaktDokumentyFinansoweGridViewModel(ctx, kontakt.Id, mode);
            //StawkiPracownikowGridViewModel = CustomGridViewModel.GetKontaktStawkiPracownikowGridViewModel(ctx, kontakt.Id, mode);
            //KontaBankoweGridViewModel = CustomGridViewModel.GetKontaktKontaBankoweGridViewModel(ctx, kontakt.Id, mode);
        }
    }
}

