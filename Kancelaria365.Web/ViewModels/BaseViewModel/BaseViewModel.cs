﻿using Kancelaria365.Data;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kancelaria365.Web
{
    public class BaseViewModel
    {
        public FormMode FormMode { get; set; }

        public BaseViewModel()
        {
        }

        public BaseViewModel(ClientContext ctx)
        {
            ClientContext = ctx;
        }

        public ClientContext ClientContext
        {
            get; set;
        }

        private SprawaRepository sprawaRepository;
        public SprawaRepository SprawaRepository
        {
            get
            {
                if (sprawaRepository == null)
                {
                    sprawaRepository = new SprawaRepository(ClientContext);
                }
                return sprawaRepository;

            }
        }

        private KontaktRepository kontaktRepository;
        public KontaktRepository KontaktRepository
        {
            get
            {
                if (kontaktRepository == null)
                {
                    kontaktRepository = new KontaktRepository(ClientContext);
                }
                return kontaktRepository;

            }
        }


        private KontoRepository kontoRepository;
        public KontoRepository KontoRepository
        {
            get
            {
                if (kontoRepository == null)
                {
                    kontoRepository = new KontoRepository(ClientContext);
                }
                return kontoRepository;

            }
        }

        private CzynnoscRepository czynnoscRepository;
        public CzynnoscRepository CzynnoscRepository
        {
            get
            {
                if (czynnoscRepository == null)
                {
                    czynnoscRepository = new CzynnoscRepository(ClientContext);
                }
                return czynnoscRepository;

            }
        }

        private KosztRepository kosztRepository;
        public KosztRepository KosztRepository
        {
            get
            {
                if (kosztRepository == null)
                {
                    kosztRepository = new KosztRepository(ClientContext);
                }
                return kosztRepository;

            }
        }

        private SlownikRepository slownikRepository;
        public SlownikRepository SlownikRepository
        {
            get
            {
                if (slownikRepository == null)
                {
                    slownikRepository = new SlownikRepository(ClientContext);
                }
                return slownikRepository;

            }
        }

        private KategoriaPracownikaRepository kategoriaPracownikaRepository;
        public KategoriaPracownikaRepository KategoriaPracownikaRepository
        {
            get
            {
                if (kategoriaPracownikaRepository == null)
                {
                    kategoriaPracownikaRepository = new KategoriaPracownikaRepository(ClientContext);
                }
                return kategoriaPracownikaRepository;

            }
        }

        private UserRepository userRepository;
        public UserRepository UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(ClientContext);
                }
                return userRepository;
            }
        }

        public IRepositoryBase<T> GetRepository<T>()
        {
            return RepositoryFactory.Create<T>(ClientContext);
        }

        public Dictionary<string, string> GetFieldsInView(string listTitle, string viewName)
        {
            List list = ClientContext.Web.Lists.GetByTitle(listTitle);
            View view = list.Views.GetByTitle(viewName);
            ClientContext.Load(view.ViewFields);
            ClientContext.Load(list.Fields);
            ClientContext.ExecuteQuery();

            foreach (string internalName in view.ViewFields)
            {
                ClientContext.Load(list.Fields.GetByInternalNameOrTitle(internalName));
            }
            ClientContext.ExecuteQuery();

            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (string internalName in view.ViewFields)
            {
                string name = internalName;

                Field filed = list.Fields.GetByInternalNameOrTitle(internalName);
                string title = list.Fields.GetByInternalNameOrTitle(internalName).Title;

                if (filed.FieldTypeKind == FieldType.User ||
                    filed.FieldTypeKind == FieldType.Lookup)
                {
                    name = name + "[0]";
                }
                dic.Add(name, title);
            }
            return dic;
        }
    }

}
   