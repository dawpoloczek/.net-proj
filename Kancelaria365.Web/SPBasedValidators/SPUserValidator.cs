﻿using Kancelaria365.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.SharePoint.Client;
using System.Linq;
using System.Web;

namespace Kancelaria365.Web.SPBasedValidators
{
    public class SPUserValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                using (var clientContext = SharePointContext.GetContext(HttpContext.Current))
                {

                    UserDictionaryItem item = value as UserDictionaryItem;
                    var rootWeb = clientContext.Site.RootWeb;
                    User ensuredUser = null;
                    if (!string.IsNullOrEmpty(item.AdditionalValue))
                    {
                        ensuredUser = rootWeb.EnsureUser(item.AdditionalValue);
                    }
                    else
                    {
                        ensuredUser = rootWeb.GetUserById(item.Id);
                    }
                    clientContext.Load(ensuredUser);
                    clientContext.ExecuteQuery();

                    //PrincipalInfo pinfo = Utility.ResolvePrincipal(rootWeb, "John Smith", 
                    //    PrincipalType.User, PrincipalSource.All, rootWeb.Users, false); SPUser user = web.Users[pinfo.LoginName];
                }
                return ValidationResult.Success;
            }
            catch (Exception)
            {
                return new ValidationResult("Niepoprawna wartość pola " + validationContext.DisplayName);
            }
        }
    }
}