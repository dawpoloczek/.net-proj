﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.EventReceivers;
using System.Reflection;
using Kancelaria365.Web.Helpers;
using Kancelaria365.Common;
using NLog;

namespace Kancelaria365.Web.Services
{
    public class AppEventReceiver : IRemoteEventService
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        public SPRemoteEventResult ProcessEvent(SPRemoteEventProperties properties)
        {
            SPRemoteEventResult result = new SPRemoteEventResult();
            try
            {
                if (properties.EventType == SPRemoteEventType.AppInstalled)
                {
                    using (ClientContext clientContext = TokenHelper.CreateAppEventClientContext(properties, false))
                    {
                        if (clientContext != null)
                        {
                            AddEventRecivers(clientContext, GlobalNames.Lists.StawkiVAT);
                        }
                    }
                }
                else if (properties.EventType == SPRemoteEventType.AppUninstalling)
                {
                    using (ClientContext clientContext = TokenHelper.CreateAppEventClientContext(properties, false))
                    {
                        RemoveEventReciver(clientContext, GlobalNames.Lists.StawkiVAT);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return result;
        }

        private void RemoveEventReciver(ClientContext clientContext, string listTile,
            string remoteAppUrl = "https://kancelaria365.azurewebsites.net/Services")
        {
            try
            {
                var list = clientContext.Web.Lists.GetByTitle(listTile);
                clientContext.Load(list);
                clientContext.ExecuteQuery();
                EventReceiverDefinitionCollection erdc = list.EventReceivers;
                clientContext.Load(erdc);
                clientContext.ExecuteQuery();
                List<EventReceiverDefinition> toDelete = new List<EventReceiverDefinition>();
                foreach (EventReceiverDefinition erd in erdc)
                {
                    if (erd.ReceiverName == "DictionariesRemoteEventReceiver")
                    {
                        toDelete.Add(erd);
                    }
                }
                //Delete the remote event receiver from the list, when the app gets uninstalled
                foreach (EventReceiverDefinition item in toDelete)
                {
                    try
                    {
                        item.DeleteObject();
                        clientContext.ExecuteQuery();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        private void AddEventRecivers(ClientContext clientContext, string listTile, string remoteAppUrl = "https://kancelaria365.azurewebsites.net/Services")
        {
            try
            {
                var list = clientContext.Web.Lists.GetByTitle(listTile);
                clientContext.Load(list);
                clientContext.ExecuteQuery();
                string remoteUrl = string.Format("{0}/DictionariesRemoteEventReceiver.svc", remoteAppUrl);

                List<EventReceiverType> eventLists = new List<EventReceiverType>();
                eventLists.Add(EventReceiverType.ItemAdded);
                eventLists.Add(EventReceiverType.ItemUpdated);
                eventLists.Add(EventReceiverType.ItemDeleted);

                foreach (var eventType in eventLists)
                {
                    try
                    {
                        EventReceiverDefinitionCreationInformation newEventReceiver = new EventReceiverDefinitionCreationInformation()
                        {
                            EventType = eventType,
                            ReceiverAssembly = Assembly.GetExecutingAssembly().FullName,
                            ReceiverName = "DictionariesRemoteEventReceiver",
                            ReceiverClass = "DictionariesRemoteEventReceiver",
                            ReceiverUrl = remoteUrl,
                            SequenceNumber = 15000
                        };
                        list.EventReceivers.Add(newEventReceiver);
                        clientContext.ExecuteQuery();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void ProcessOneWayEvent(SPRemoteEventProperties properties)
        {
            // This method is not used by app events.
        }
    }
}



