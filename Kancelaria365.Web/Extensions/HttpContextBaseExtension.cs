﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.Mvc;

namespace Kancelaria365.Web.Extensions
{
    public static class HttpContextBaseExtension
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static T GetSessionValue<T>(this HttpContextBase context, string key)
        {
            T value = default(T);
            try
            {

                if (context.Session[key] != null)
                {
                    value = (T)context.Session[key];
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return value;
        }
        public static void SetSessionValue<T>(this HttpContextBase context, string key, T value)
        {
            context.Session[key] = value;
        }
    }
}