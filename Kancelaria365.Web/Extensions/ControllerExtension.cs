﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.Mvc;

namespace Kancelaria365.Web.Extensions
{
    public static class ControllerExtension
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static T GetTempData<T>(this Controller controler, string key)
        {
            T value = default(T);
            try
            {
                if (controler.TempData[key] != null)
                {
                    value = (T)Convert.ChangeType(controler.TempData[key], typeof(T));
                }
            }           
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return value;
        }

        public static void SetTempData<T>(this Controller controler, string key, T value)
        {
         
            controler.TempData[key] = value;
        }

        public static T GetViewData<T>(this Controller controler, string key)
        {
            T value = default(T);
            try
            {
                if (controler.ViewData[key] != null)
                {
                    value = (T)Convert.ChangeType(controler.ViewData[key], typeof(T));
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return value;
        }
        public static void SetViewData<T>(this Controller controler, string key, T value)
        {
            controler.ViewData[key] = value;
        }
    }
}