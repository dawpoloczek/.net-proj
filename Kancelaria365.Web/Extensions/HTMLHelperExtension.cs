﻿using Kancelaria365.Data;
using Kancelaria365.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Kancelaria365.Web
{
    public static class HTMLHelperExtension
    {
        public static MvcHtmlString RequiredLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            var resolvedLabelText = metadata.DisplayName ?? metadata.PropertyName;
            if (!metadata.IsRequired)
            {
                return html.LabelFor(expression, resolvedLabelText, htmlAttributes);
            }

            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            if (attributes == null)
            {
                return html.LabelFor(expression, resolvedLabelText, htmlAttributes);
            }

            const string requiredClass = "required-label";
            if (attributes.ContainsKey("class"))
            {
                var classList = attributes["class"].ToString().Split(' ').ToList();
                classList.Add(requiredClass);
                attributes["class"] = string.Join(" ", classList);
            }
            else
            {
                attributes.Add("class", requiredClass);
            }

            return html.LabelFor(expression, resolvedLabelText, attributes);
        }

        public static MvcHtmlString CustomDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> selectList, Dictionary<string, object> htmlAttributes)
        {
            if (htmlAttributes.ContainsKey("readonly"))
            {
                ModelMetadata model = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

                string value = string.Empty;
                if (selectList.Any(x => x.Selected && x.Text != "--Wybierz--"))
                {
                    SelectListItem selecteditem = selectList.FirstOrDefault(x => x.Selected);
                    value = selecteditem.Text;
                }

                return htmlHelper.TextBox(htmlHelper.IdFor(expression).ToHtmlString(), value, htmlAttributes: htmlAttributes);
            }
            else
            {
                return htmlHelper.DropDownListFor(expression, selectList, htmlAttributes);
            }
        }

        public static MvcHtmlString CustomVatDropDownList<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string nettoFiedlId,
            string bruttoFiedlId,            
            IEnumerable<SelectListItem> selectList, Dictionary<string, object> htmlAttributes)
        {
            if (htmlAttributes.ContainsKey("readonly"))
            {
                ModelMetadata model = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

                string value = string.Empty;
                if (selectList.Any(x => x.Selected && x.Text != "--Wybierz--"))
                {
                    SelectListItem selecteditem = selectList.FirstOrDefault(x => x.Selected);
                    value = selecteditem.Text;
                }

                return htmlHelper.TextBox(htmlHelper.IdFor(expression).ToHtmlString(), value, htmlAttributes: htmlAttributes);
            }
            else
            {
                if (!htmlAttributes.ContainsKey("onchange"))
                {
                    htmlAttributes["onchange"] = "vatFieldUpdated(this, '" + nettoFiedlId + "', '" + bruttoFiedlId + "');";
                }
                return htmlHelper.DropDownListFor(expression, selectList, htmlAttributes);
            }
        }

        public static MvcHtmlString CustomCurrencyInput<TModel, TValue>(
            this HtmlHelper<TModel> html, 
            Expression<Func<TModel, TValue>> expression,
            string vatFieldId,
            string bruttoFieldId,
            Dictionary<string, object> htmlAttributes)
        {
            if (htmlAttributes != null)
            {                
                if (!htmlAttributes.ContainsKey("onchange"))
                {
                    htmlAttributes["onchange"] = "currencyFieldUpdated(this, '"+ vatFieldId + "', '" + bruttoFieldId + "');";
                }
                if (!htmlAttributes.ContainsKey("data-inputmask"))
                {
                    htmlAttributes["data-inputmask"] = "'mask': '9{0,50},9{0,2}'";
                }
            }

            MvcHtmlString inputString = html.TextBoxFor(expression, format: "{0:0.00}", htmlAttributes: htmlAttributes);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(inputString.ToHtmlString());
            sb.AppendLine();
            sb.AppendLine("<script type=\"text/javascript\">");
            sb.AppendLine("$(\"#" + html.IdFor(expression) + "\").inputmask();");
            sb.AppendLine("</script>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString CustomMaskedInput<TModel, TValue>(
            this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression,
            string maskString,
            Dictionary<string, object> htmlAttributes)
                {
                    if (htmlAttributes != null)
                    {
                        if (!htmlAttributes.ContainsKey("data-inputmask"))
                        {
                            htmlAttributes["data-inputmask"] = "'mask': '"+ maskString +"'";
                        }
                    }

                    MvcHtmlString inputString = html.TextBoxFor(expression, htmlAttributes: htmlAttributes);

                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine(inputString.ToHtmlString());
                    sb.AppendLine();
                    sb.AppendLine("<script type=\"text/javascript\">");
                    sb.AppendLine("$(\"#" + html.IdFor(expression) + "\").inputmask();");
                    sb.AppendLine("</script>");
                    return MvcHtmlString.Create(sb.ToString());
                }

        public static MvcHtmlString CustomCheckBox<TModel>(
                this HtmlHelper<TModel> html,
                Expression<Func<TModel, bool>> expression,
                object htmlAttributes,
                FormMode mode)
        {
            MvcHtmlString inputString = html.CheckBoxFor(expression, htmlAttributes);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<div class='checkbox'><label>");
            sb.AppendLine(inputString.ToHtmlString());
            sb.AppendLine("</label></div>");            
            sb.AppendLine();
            sb.AppendLine("<script type=\"text/javascript\">");
            sb.AppendLine("$('#"+html.IdFor(expression).ToString()+"').iCheck({ checkboxClass: 'icheckbox_flat-green'});");
            if(mode == FormMode.Dispaly)
            {
                sb.AppendLine("$('#" + html.IdFor(expression).ToString() + "').iCheck('disable');");
            }
            sb.AppendLine("</script>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString SPNumberInput<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string listTitle, string fieldName, object htmlAttributes)
        {
            BaseController controller = html.ViewContext.Controller as BaseController;
            Dictionary<string, string> fieldProp = controller.GetNumberFieldProperties(listTitle, fieldName);

            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            var resolvedLabelText = metadata.DisplayName ?? metadata.PropertyName;

            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            if (attributes == null)
            {
                return html.TextBoxFor(expression, resolvedLabelText, htmlAttributes);
            }

            const string requiredClass = "bfh-number";
            if (attributes.ContainsKey("class"))
            {
                var classList = attributes["class"].ToString().Split(' ').ToList();
                classList.Add(requiredClass);
                attributes["class"] = string.Join(" ", classList);
            }
            else
            {
                attributes.Add("class", requiredClass);
            }

            if (attributes.ContainsKey("data-min"))
            {
                attributes["data-min"] = fieldProp["MinVal"];
            }
            else
            {
                attributes.Add("data-min", fieldProp["MinVal"]);
            }

            if (attributes.ContainsKey("data-max"))
            {
                attributes["data-max"] = fieldProp["MaxVal"];
            }
            else
            {
                attributes.Add("data-max", fieldProp["MaxVal"]);
            }

            if (attributes.ContainsKey("data-wrap"))
            {
                attributes["data-wrap"] = "true";
            }
            else
            {
                attributes.Add("data-wrap", "true");
            }

            return html.TextBoxFor(expression, resolvedLabelText, attributes);
        }

        public static MvcHtmlString CustomDatePickerFor<TModel, TValue>(
            this HtmlHelper<TModel> html, 
            Expression<Func<TModel, TValue>> expression,
            Dictionary<string, object> htmlAttributes,
            string dateFormatString)
        {
            MvcHtmlString inputString = html.TextBoxFor(expression, format: dateFormatString, htmlAttributes: htmlAttributes);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(inputString.ToHtmlString());

            if (!htmlAttributes.ContainsKey("readonly"))
            {
                sb.AppendLine();
                sb.AppendLine("<script type=\"text/javascript\">");
                sb.AppendLine("DateTimeHelper.createDatePicker('"+ html.IdFor(expression) +"', '"+ dateFormatString + "')");
                sb.AppendLine("</script>");
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static Dictionary<string, object> CreateHtmlAttributes<TModel>(this HtmlHelper<TModel> html, Dictionary<string, object> htmlAttributes, FormMode mode)
        {
            if (mode == FormMode.Dispaly)
            {
                htmlAttributes.Add("readonly", "readonly");
            }
            return htmlAttributes;
        }

        private static dynamic DictionaryToObject(IDictionary<string, object> dict)
        {
            IDictionary<string, object> eo = new ExpandoObject() as IDictionary<string, object>;
            foreach (KeyValuePair<string, object> kvp in dict)
            {
                eo.Add(kvp);
            }
            return eo;
        }
    }
}
