﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Kancelaria365.Common
{
    public static class GlobalNames
    {
        public static class Lists
        {
            public static string KancelariaStrony = "Kancelaria strony";
            public static string KancelariaPliki = "Kancelaria pliki";

            public static string Waluty = "Waluty";
            public static string KategoriePracownikow = "Kategorie pracowników";
            public static string FormyRozliczenia = "Formy rozliczenia";
            public static string KategorieKontaktow = "Kategorie kontaktów";
            public static string NazwyKas = "Nazwy kas";
            public static string DzialyBibliotek = "Działy biblioteki";
            public static string KategorieUczestnikow = "Kategorie uczestników";
            public static string KategorieKosztow = "Kategorie kosztów";
            public static string OpisyKosztow = "Opisy kosztów";
            public static string OpisyKosztowInnych = "Opisy kosztów innych";
            public static string KategorieDokumentow = "Kategorie dokumentów";
            public static string Konta = "Konta";
            public static string Kancelarie = "Kancelarie";
            public static string KategorieCzynnosciTerminowZadan = "Kategorie czynności terminów zadań";
            public static string KategorieSpraw = "Kategorie spraw";
            public static string DefinicjeOdsetek = "Definicje odesetek";
            public static string Sprawy = "Sprawy";
            public static string Czynnosci = "Czynności";
            public static string Kontakty = "Kontakty";
            public static string Koszty = "Koszty";
            public static string Zadania = "Zadania";
            public static string StawkiPracownikow = "Stawki pracowników";
            public static string KontaktyDokumentyFinansowe = "Kontakty dokumenty finansowe";
            public static string OsobyKontaktowe = "Osoby kontaktowe";
            public static string StawkiVAT = "Stawki VAT";

            public static class Views
            {
                public static class Kontakty
                {
                    public static string WszystkieElementy = "Kontakty";
                }
                public static class Sprawy
                {
                    public static string KontaktSprawy = "Kontakt-Sprawy";
                    public static string WszystkieElementy = "Sprawy";
                }
                public static class Czynnosci
                {                    
                    public static string Moje = "Czynnosci-Moje";
                    public static string WszystkieElementy = "Czynnosci";
                    public static string SprawaCzynnosci = "Sprawa-Czynnosci";
                }
                public static class Zadania
                {
                    public static string SprawaZadania = "Sprawa-Zadania";
                }
                public static class Koszty
                {
                    public static string Moje = "Koszty-Moje";
                    public static string WszystkieElementy = "Koszty";
                    public static string SprawaKoszty = "Sprawa-Koszty";
                }
                public static class StawkiPracownikow
                {
                    public static string KontaktyStawkiPracownikow = "Kontakt-Stawki-Pracownikow";
                }
                public static class KontaktyDokumentyFinansowe
                {
                    public static string KontaktyDokumentyFinansoweGrid = "Kontakt-Dokumenty-Finansowe";
                }
                public static class Konta
                {
                    public static string KontaktyKontaBankoweGrid = "Konta";
                }
                public static class OsobyKontaktowe
                {
                    public static string KontaktyOsobyKontaktoweGrid = "Osoby-Kontaktowe";
                }
            }
        }

        public static class FormatStrings
        {
            public const string DateFormatString = "{0:yyyy-MM-dd}";
            // public static string DateFormatString = new CultureInfo("pl-PL").DateTimeFormat.ShortDatePattern;
        }

        public static class Constants
        {
            public static string DefaultListValue = "--Wybierz--";
        }
    }
}