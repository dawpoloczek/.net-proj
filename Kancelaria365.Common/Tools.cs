﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kancelaria365.Common
{
    public class Tools
    {
        public static string GetFormattedDate(DateTime dateTime)
        {
            return string.Format("DateTime({0}, {1}, {2})", dateTime.Year, dateTime.Month, dateTime.Day);
        }
    }
}
